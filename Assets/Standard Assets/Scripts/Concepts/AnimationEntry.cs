using System;
using UnityEngine;

namespace WizardGame
{
	[Serializable]
	public class AnimationEntry
	{
		public string animatorStateName;
		public int layer;
		public Animator animator;

		public void Play (float normalizedTime = float.NegativeInfinity)
		{
			animator.Play(animatorStateName, layer, normalizedTime);
		}

		public bool IsPlaying ()
		{
			return animator.GetCurrentAnimatorStateInfo(layer).IsName(animatorStateName);
		}

		public float GetClipLength ()
		{
			for (int i = 0; i < animator.runtimeAnimatorController.animationClips.Length; i ++)
			{
				AnimationClip animationClip = animator.runtimeAnimatorController.animationClips[i];
				if (animationClip.name == animatorStateName)
					return animationClip.length;
			}
			throw new Exception();
		}
	}
}