﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	[CreateAssetMenu]
	public class AimAtPlayerWithOffset : AimAtPlayer
	{
		// [MakeConfigurable]
		public Vector3 offset;
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return Quaternion.Euler(offset) * base.GetShootDirection(spawner);
		}
	}
}