﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	[CreateAssetMenu]
	public class AimAtPlayerWithRandomOffset : AimAtPlayer
	{
		// [MakeConfigurable]
		public FloatRange randomShootOffsetRange;
		
		public override Vector3 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(randomShootOffsetRange.Get(Random.value));
		}
	}
}