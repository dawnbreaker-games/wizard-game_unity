﻿using UnityEngine;

namespace WizardGame
{
	[CreateAssetMenu]
	public class AimInDirection : BulletPattern
	{
		public Vector3 shootDirection;

		public override Vector3 GetShootDirection (Transform spawner)
		{
			return shootDirection;
		}
	}
}