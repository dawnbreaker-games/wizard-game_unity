﻿using Extensions;
using UnityEngine;
using System.Collections;

namespace WizardGame
{
	[CreateAssetMenu]
	public class AimWhereFacingThenTargetPlayerThenStopAndTurnToTargetThenMoveAgain : AimWhereFacing
	{
		public float targetTime;
		public float turnRate;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.AddEvent ((object obj) => { Turn ((Bullet) obj); }, targetTime);
			}
			return output;
		}

		void Turn (Bullet bullet)
		{
			Redirect (bullet, Vector2.zero);
			Vector2 targetDirection = Player.instance.trs.position - bullet.trs.position;
			bullet.previousFacing = bullet.trs.forward;
			bullet.AddEvent ((object obj) => { Turn ((Bullet) obj, targetDirection); });
		}

		void Turn (Bullet bullet, Vector2 targetDirection)
		{
			float rotateAmount = turnRate * Time.deltaTime;
			bullet.previousFacing = bullet.previousFacing.RotateTo(targetDirection, rotateAmount);
			bullet.trs.forward = bullet.trs.forward.RotateTo(bullet.previousFacing, rotateAmount);
			if (Vector2.Angle(bullet.previousFacing, targetDirection) <= rotateAmount)
				Redirect (bullet, targetDirection.normalized);
			else
				bullet.AddEvent ((object obj) => { Turn ((Bullet) obj, targetDirection); });
		}
	}
}