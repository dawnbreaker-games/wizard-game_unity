using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	[CreateAssetMenu]
	public class FollowClosestEnemy : AimWhereFacing
	{
		public LayerMask whatIsEnemy;
		public float rotateRate;
		public float followRange;

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.AddEvent ((object obj) => { FollowRoutine ((Bullet) obj); });
			}
			return output;
		}
		
		public override Bullet[] Shoot (Vector3 spawnPos, Vector3 direction, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawnPos, direction, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				bullet.AddEvent ((object obj) => { FollowRoutine ((Bullet) obj); });
			}
			return output;
		}

		void FollowRoutine (Bullet bullet)
		{
			if (bullet == null || !bullet.gameObject.activeInHierarchy)
				return;
			Collider[] hits = Physics.OverlapSphere(bullet.trs.position, followRange, whatIsEnemy);
			if (hits.Length > 0)
			{
				Vector3 closestHitPosition = new Vector3();
				float closestHitDistanceSqr = Mathf.Infinity;
				for (int i = 0; i < hits.Length; i ++)
				{
					Vector3 hitPosition = hits[i].bounds.center;
					float hitDistanceSqr = (hitPosition - bullet.trs.position).sqrMagnitude;
					if (hitDistanceSqr < closestHitDistanceSqr)
					{
						closestHitDistanceSqr = hitDistanceSqr;
						closestHitPosition = hitPosition;
					}
				}
				Vector3 newVelocity = Vector3.RotateTowards(bullet.velocity, closestHitPosition - bullet.trs.position, rotateRate * Mathf.Deg2Rad * Time.deltaTime, 0);
				Redirect (bullet, newVelocity.normalized);
			}
			bullet.events.Clear();
			bullet.AddEvent ((object obj) => { FollowRoutine ((Bullet) obj); });
		}
	}
}