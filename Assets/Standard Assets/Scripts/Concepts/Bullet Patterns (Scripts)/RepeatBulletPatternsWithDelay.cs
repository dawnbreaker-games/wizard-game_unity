﻿﻿using System;
using UnityEngine;
using System.Collections;

namespace WizardGame
{
	[CreateAssetMenu]
	public class RepeatBulletPatternsWithDelay : BulletPattern
	{
		// [MakeConfigurable]
		public uint repeatCount;
		public BulletPatternEntry[] bulletPatternEntries;

		public override void Init (Transform spawner)
		{
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntry.bulletPattern.Init (spawner);
			}
		}

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = new Bullet[0];
			BulletPatternEntry bulletPatternEntry = bulletPatternEntries[0];
			if (bulletPatternEntry.bulletPattern != null)
				output = bulletPatternEntry.bulletPattern.Shoot(spawner, bulletPrefab);
			if (bulletPatternEntries.Length > 1)
				EventManager.AddEvent ((object obj) => { Shoot (spawner, bulletPrefab, 0, 1); }, bulletPatternEntry.delayNextBulletPattern);
			else if (repeatCount > 1)
				EventManager.AddEvent ((object obj) => { Shoot (spawner, bulletPrefab, 1, 0); }, bulletPatternEntry.delayNextBulletPattern);
			return output;
		}
		
		public override Bullet[] Shoot (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab)
		{
			Bullet[] output = new Bullet[0];
			BulletPatternEntry bulletPatternEntry = bulletPatternEntries[0];
			if (bulletPatternEntry.bulletPattern != null)
				output = bulletPatternEntry.bulletPattern.Shoot(spawnPosition, direction, bulletPrefab);
			if (bulletPatternEntries.Length > 1)
				EventManager.AddEvent ((object obj) => { Shoot (spawnPosition, direction, bulletPrefab, 0, 1); }, bulletPatternEntry.delayNextBulletPattern);
			else if (repeatCount > 1)
				EventManager.AddEvent ((object obj) => { Shoot (spawnPosition, direction, bulletPrefab, 1, 0); }, bulletPatternEntry.delayNextBulletPattern);
			return output;
		}

		Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, int currentRepeat, int bulletPatternEntryIndex)
		{
			Bullet[] output = new Bullet[0];
			BulletPatternEntry bulletPatternEntry = bulletPatternEntries[bulletPatternEntryIndex];
			if (bulletPatternEntry.bulletPattern != null)
				output = bulletPatternEntry.bulletPattern.Shoot(spawner, bulletPrefab);
			if (bulletPatternEntries.Length > bulletPatternEntryIndex + 1)
				EventManager.AddEvent ((object obj) => { Shoot (spawner, bulletPrefab, currentRepeat, bulletPatternEntryIndex + 1); }, bulletPatternEntry.delayNextBulletPattern);
			else if (repeatCount > currentRepeat)
				EventManager.AddEvent ((object obj) => { Shoot (spawner, bulletPrefab, currentRepeat + 1, 0); }, bulletPatternEntry.delayNextBulletPattern);
			return output;
		}

		Bullet[] Shoot (Vector3 spawnPosition, Vector3 direction, Bullet bulletPrefab, int currentRepeat, int bulletPatternEntryIndex)
		{
			Bullet[] output = new Bullet[0];
			BulletPatternEntry bulletPatternEntry = bulletPatternEntries[bulletPatternEntryIndex];
			if (bulletPatternEntry.bulletPattern != null)
				output = bulletPatternEntry.bulletPattern.Shoot(spawnPosition, direction, bulletPrefab);
			if (bulletPatternEntries.Length > bulletPatternEntryIndex + 1)
				EventManager.AddEvent ((object obj) => { Shoot (spawnPosition, direction, bulletPrefab, currentRepeat, bulletPatternEntryIndex + 1); }, bulletPatternEntry.delayNextBulletPattern);
			else if (repeatCount > currentRepeat)
				EventManager.AddEvent ((object obj) => { Shoot (spawnPosition, direction, bulletPrefab, currentRepeat + 1, 0); }, bulletPatternEntry.delayNextBulletPattern);
			return output;
		}

		[Serializable]
		public class BulletPatternEntry
		{
			public BulletPattern bulletPattern;
			public float delayNextBulletPattern;
		}
	}
}
