using System;
using UnityEngine;
using System.Collections.Generic;

namespace WizardGame
{
	[CreateAssetMenu]
	public class ShootBulletPatternEveryXTimes : BulletPattern
	{
		public bool shootAtFirstTime;
        public uint xTimes;
        public BulletPattern bulletPattern;
		Dictionary<Transform, uint> timesRemainingDict = new Dictionary<Transform, uint>();

		public override void Init (Transform spawner)
		{
			if (shootAtFirstTime)
				timesRemainingDict[spawner] = 1;
			else
				timesRemainingDict[spawner] = xTimes;
		}

		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			timesRemainingDict[spawner] --;
			if (timesRemainingDict[spawner] == 0)
			{
				timesRemainingDict[spawner] = xTimes;
				return bulletPattern.Shoot(spawner, bulletPrefab);
			}
			return new Bullet[0];
		}
	}
}
