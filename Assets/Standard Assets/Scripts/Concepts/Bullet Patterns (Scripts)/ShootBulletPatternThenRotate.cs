using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	[CreateAssetMenu]
	public class ShootBulletPatternThenRotate : BulletPattern
	{
		public BulletPattern bulletPattern;
		public Vector3 rotation;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = bulletPattern.Shoot(spawner, bulletPrefab);
			if (setSpawnerAimingOnShoot)
				spawner.Rotate(rotation);
			return output;
		}
	}
}