using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	[CreateAssetMenu]
	public class ShootWhereFacingWithOffsetThenRedirectToOffset : AimWhereFacingWithOffset
	{
		public float redirectTime;
		public float redirectOffset;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				RedirectAfterDelay (bullet, bullet.trs.forward.Rotate(redirectOffset), redirectTime);
			}
			return output;
		}
	}
}