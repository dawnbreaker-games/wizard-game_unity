namespace WizardGame
{
	public static class LocalUserInfo
	{
		public static string username;
		public static bool isPublic;
		public static int bestScore;
		public static uint wins;
		public static uint winsInCurrentSeason;
		public static uint losses;
		public static uint lossesInCurrentSeason;
		public static uint kills;
		public static uint killsInCurrentSeason;
		public static uint deaths;
		public static uint deathsInCurrentSeason;
		public static float skill;
		public static float skillInCurrentSeason;
		public static uint lastMonthPlayed;
	}
}