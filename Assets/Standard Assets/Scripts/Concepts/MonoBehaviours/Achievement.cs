using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace WizardGame
{
	public class Achievement : MonoBehaviour
	{
		public Level levelIAmFoundOn;
		public string displayName;
		public GameObject incompleteIndicatorGo;
		public Level unlockLevelOnComplete;
		public Level[] levels = new Level[0];
		[HideInInspector]
		public bool complete;
#if UNITY_EDITOR
		public LineRenderer completeIndicatorLineRenderer;
		public LineRenderer incompleteIndicatorLineRenderer;
		public float lineRendererSepeartion;
#endif
		public static Achievement[] instances = new Achievement[0];
		public static uint completeCount;

#if UNITY_EDITOR
		void OnValidate ()
		{
			Transform trs = GetComponent<Transform>();
			if (completeIndicatorLineRenderer == null)
				completeIndicatorLineRenderer = trs.Find("Complete Indicator").GetComponent<LineRenderer>();
			if (incompleteIndicatorLineRenderer == null)
				incompleteIndicatorLineRenderer = trs.Find("Incomplete Indicator").GetComponent<LineRenderer>();
			if (incompleteIndicatorGo == null)
				incompleteIndicatorGo = completeIndicatorLineRenderer.gameObject;
			levelIAmFoundOn = GetComponentInParent<Level>();
			if (levelIAmFoundOn != null)
			{
				Vector3 offset = ((Vector2) (unlockLevelOnComplete.trs.position - levelIAmFoundOn.trs.position)).normalized.Rotate90() * lineRendererSepeartion / 2;
				completeIndicatorLineRenderer.SetPositions(new Vector3[] { levelIAmFoundOn.trs.position + offset, unlockLevelOnComplete.trs.position + offset });
				incompleteIndicatorLineRenderer.SetPositions(new Vector3[] { levelIAmFoundOn.trs.position + offset, unlockLevelOnComplete.trs.position + offset });
			}
		}
#endif

		public void Init ()
		{
#if !UNITY_WEBGL
			if (SaveAndLoadManager.saveData.completeAchievementsNames.Contains(name))
				Complete ();
#endif
		}

		public bool ShouldBeComplete ()
		{
			return (levelIAmFoundOn == null || levelIAmFoundOn.unlocked) && GetProgress() >= GetMaxProgress();
		}
		
		public virtual void Complete ()
		{
			if (complete)
				return;
			incompleteIndicatorGo.SetActive(false);
#if !UNITY_WEBGL
			if (!SaveAndLoadManager.saveData.completeAchievementsNames.Contains(name))
				SaveAndLoadManager.saveData.completeAchievementsNames = SaveAndLoadManager.saveData.completeAchievementsNames.Add(name);
#endif
			unlockLevelOnComplete.unlocked = true;
			unlockLevelOnComplete.lockedIndicatorGo.SetActive(false);
			complete = true;
			completeCount ++;
			print(name + " complete");
		}

		public virtual uint GetProgress ()
		{
			throw new NotImplementedException();
		}

		public virtual uint GetMaxProgress ()
		{
			throw new NotImplementedException();
		}
	}
}