using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace WizardGame
{
	public class ExplorationAchievement : Achievement
	{
		public uint exploreCount;

		public override uint GetProgress ()
		{
			uint exploreCount = 0;
			for (int i = 0; i < levels.Length; i ++)
			{
				Level level = levels[i];
				if (level.unlocked)
					exploreCount ++;
			}
			return exploreCount;
		}

		public override uint GetMaxProgress ()
		{
			return exploreCount;
		}
	}
}