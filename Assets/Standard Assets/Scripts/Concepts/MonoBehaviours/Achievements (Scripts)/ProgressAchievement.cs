namespace WizardGame
{
	public class ProgressAchievement : Achievement
	{
		public uint completeAchievementCount;

		public override uint GetProgress ()
		{
			uint completeAchievementCount = 0;
			for (int i = 0; i < levels.Length; i ++)
			{
				Level level = levels[i];
				Achievement[] achievements = level.GetComponentsInChildren<Achievement>();
				for (int i2 = 0; i2 < achievements.Length; i2 ++)
				{
					Achievement achievement = achievements[i2];
					if (achievement.complete)
						completeAchievementCount ++;
				}
			}
			return completeAchievementCount;
		}

		public override uint GetMaxProgress ()
		{
			return completeAchievementCount;
		}
	}
}