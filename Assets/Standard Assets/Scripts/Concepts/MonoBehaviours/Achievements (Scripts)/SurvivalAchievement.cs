using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace WizardGame
{
	public class SurvivalAchievement : Achievement
	{
		public uint time;
		public uint levelsThatNeedToReachTime;

		public override uint GetProgress ()
		{
			if (levelsThatNeedToReachTime == 0)
			{
				float totalTime = 0;
				for (int i = 0; i < levels.Length; i ++)
				{
					Level level = levels[i];
					totalTime += level.BestTimeReached;
				}
				return (uint) totalTime;
			}
			else
			{
				uint levelsThatReachTime = 0;
				for (int i = 0; i < levels.Length; i ++)
				{
					Level level = levels[i];
					if (level.BestTimeReached >= time)
						levelsThatReachTime ++;
				}
				return levelsThatReachTime;
			}
		}

		public override uint GetMaxProgress ()
		{
			if (levelsThatNeedToReachTime > 0)
				return levelsThatNeedToReachTime;
			else
				return time;
		}
	}
}