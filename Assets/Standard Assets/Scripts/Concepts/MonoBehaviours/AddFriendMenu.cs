using Extensions;
using UnityEngine;
using PlayerIOClient;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class AddFriendMenu : SingletonMonoBehaviour<AddFriendMenu>
	{
		public Button sendButton;
		public static string usernameToAdd;

		public void SetUsernameToAdd (string username)
		{
			usernameToAdd = username;
			sendButton.interactable = !string.IsNullOrEmpty(username);
		}

		public void AddUser ()
		{
			if (!FriendsMenu.friendsUsernames.Contains(usernameToAdd))
			{
				if (!AddFriendRequestsMenu.usernamesOfPendingInvites.Contains(usernameToAdd))
					NetworkManager.client.BigDB.Load("PlayerObjects", usernameToAdd, OnLoadDBObjectSuccess, NetworkManager.DisplayError);
				else
					GameManager.instance.DisplayNotification (usernameToAdd + " already sent you a friend request and is awaiting your response! Press the friends tab to accept or reject the request.");
			}
			else
				GameManager.instance.DisplayNotification (usernameToAdd + " is already your friend!");
		}

		void OnLoadDBObjectSuccess (DatabaseObject dbObj)
		{
			if (dbObj != null)
				NetworkManager.client.GameRequests.Send("addfriend", null, new string[] { usernameToAdd }, OnSendGameRequestSuccess, NetworkManager.DisplayError);
			else
				GameManager.instance.DisplayNotification (usernameToAdd + " is not an existing user.");
		}

		void OnSendGameRequestSuccess ()
		{
			GameManager.instance.DisplayNotification ("Invite sent");
			gameObject.SetActive(false);
		}

		void OnApplicationQuit ()
		{
			usernameToAdd = null;
		}
	}
}