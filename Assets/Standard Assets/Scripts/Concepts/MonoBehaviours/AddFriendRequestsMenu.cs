using Extensions;
using UnityEngine;
using PlayerIOClient;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class AddFriendRequestsMenu : SingletonMonoBehaviour<AddFriendRequestsMenu>
	{
		public AddFriendRequestInfoIndicator addFriendRequestInfoIndicatorPrefab;
		public Transform addFriendRequestInfoIndicatorsParent;
		public static List<string> usernamesOfPendingInvites = new List<string>();
		public static List<string> rejectedUsernames = new List<string>();

		public void AcceptAll ()
		{
			AddFriendRequestInfoIndicator[] addFriendRequestInfoIndicators = addFriendRequestInfoIndicatorsParent.GetComponentsInChildren<AddFriendRequestInfoIndicator>();
			for (int i = 0; i < addFriendRequestInfoIndicators.Length; i ++)
			{
				AddFriendRequestInfoIndicator addFriendRequestInfoIndicator = addFriendRequestInfoIndicators[i];
				addFriendRequestInfoIndicator.Accept ();
			}
		}

		void OnApplicationQuit ()
		{
			usernamesOfPendingInvites.Clear();
			rejectedUsernames.Clear();
		}
	}
}