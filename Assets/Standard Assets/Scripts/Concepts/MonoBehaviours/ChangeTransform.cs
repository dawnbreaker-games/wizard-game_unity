using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class ChangeTransform : MonoBehaviour
	{
		public Transform trs;
		public BoxCollider positionInsideCollider;
		public Collider collider;
        public FloatRange rotateAmountRange;
		public Transform[] possibleOrientations = new Transform[0];
		public Vector3 minPositionChange;
		public Vector3 maxPositionChange;
		public Vector3 minSizeChange;
		public Vector3 maxSizeChange;
		public PositionMode positionMode;
		public RotateMode rotateMode;
		public SizeMode sizeMode;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (collider == null)
				collider = GetComponent<Collider>();
		}
#endif

		public void Do ()
		{
			if (rotateMode == RotateMode.Transforms)
				trs.rotation = possibleOrientations[Random.Range(0, possibleOrientations.Length)].rotation;
			else if (rotateMode == RotateMode.Random)
				trs.Rotate(Random.onUnitSphere * rotateAmountRange.Get(Random.value));
            if (positionMode == PositionMode.BoxColliderBounds)
				trs.position = positionInsideCollider.bounds.Shrink(collider.bounds).RandomPoint();
			else if (positionMode == PositionMode.Transforms)
				trs.position = possibleOrientations[Random.Range(0, possibleOrientations.Length)].position;
			else if (positionMode == PositionMode.RandomInsideBox)
				trs.position += new Vector3(Random.Range(minSizeChange.x, maxSizeChange.x), Random.Range(minSizeChange.y, maxSizeChange.y), Random.Range(minSizeChange.z, maxSizeChange.z));
			if (sizeMode == SizeMode.Random)
				trs.localScale += new Vector3(Random.Range(minSizeChange.x, maxSizeChange.x), Random.Range(minSizeChange.y, maxSizeChange.y), Random.Range(minSizeChange.z, maxSizeChange.z));
			else if (sizeMode == SizeMode.Transforms)
				trs.localScale = possibleOrientations[Random.Range(0, possibleOrientations.Length)].localScale;
		}

		public enum PositionMode
		{
            DontChange,
			BoxColliderBounds,
			RandomInsideBox,
			RandomInsideSectionOfSphere,
			Transforms
		}

		public enum RotateMode
		{
			DontChange,
            Random,
			Transforms
		}

		public enum SizeMode
		{
			DontChange,
            Random,
			Transforms
		}
	}
}