using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class FriendsMenu : SingletonMonoBehaviour<FriendsMenu>
	{
		public FriendInfoIndicator friendInfoIndicatorPrefab;
		public Transform friendInfoIndicatorsParent;
		public uint maxFriendInfoIndicatorsPerPage;
		public _Text currentPageText;
		public static List<string> friendsUsernames = new List<string>();
		static uint currentPage;

		void Load ()
		{
			currentPage = 0;
			currentPageText.Text = "Page 1 / " + Mathf.Clamp(friendsUsernames.Count / maxFriendInfoIndicatorsPerPage, 1, uint.MaxValue);
			for (int i = 0; i < FriendsMenu.friendsUsernames.Count; i ++)
			{
				string friendUsername = FriendsMenu.friendsUsernames[i];
				FriendInfoIndicator friendInfoIndicator = Instantiate(friendInfoIndicatorPrefab, friendInfoIndicatorsParent);
				friendInfoIndicator.Init (friendUsername);
			}
		}

		void Unload ()
		{
			for (int i = 0; i < friendInfoIndicatorsParent.childCount; i ++)
			{
				Transform child = friendInfoIndicatorsParent.GetChild(i);
				Destroy(child.gameObject);
			}
		}

		public void Reload ()
		{
			Unload ();
			Load ();
		}

		public void NextPage ()
		{
			if (currentPage < friendsUsernames.Count / maxFriendInfoIndicatorsPerPage)
				currentPage ++;
			else
				currentPage = 0;
			currentPageText.Text = "Page " + (currentPage + 1) + " / " + Mathf.Clamp(friendsUsernames.Count / maxFriendInfoIndicatorsPerPage, 1, uint.MaxValue);
			Reload ();
		}

		public void PreviousPage ()
		{
			if (currentPage > 0)
				currentPage --;
			else
				currentPage = (uint) (friendsUsernames.Count / maxFriendInfoIndicatorsPerPage);
			currentPageText.Text = "Page " + (currentPage + 1) + " / " + Mathf.Clamp(friendsUsernames.Count / maxFriendInfoIndicatorsPerPage, 1, uint.MaxValue);
			Reload ();
		}
	}
}