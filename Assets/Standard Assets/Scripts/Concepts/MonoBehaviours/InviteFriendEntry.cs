namespace WizardGame
{
	public class InviteFriendEntry : UserInfoIndicator
	{
		public void ToggleInviteUser ()
		{
			int indexOfUsername = InviteFriendsMenu.usernamesToInvite.IndexOf(username);
			if (indexOfUsername != -1)
				InviteFriendsMenu.usernamesToInvite.RemoveAt(indexOfUsername);
			else
				InviteFriendsMenu.usernamesToInvite.Add(username);
			InviteFriendsMenu.instance.sendButton.interactable = InviteFriendsMenu.usernamesToInvite.Count > 0;
		}
	}
}