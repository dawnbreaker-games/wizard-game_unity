using Extensions;
using UnityEngine;
using PlayerIOClient;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class InviteFriendsMenu : SingletonMonoBehaviour<InviteFriendsMenu>
	{
		public Button sendButton;
		public InviteFriendEntry inviteFriendEntryPrefab;
		public Transform inviteFriendEntriesParent;
		public InviteRequest inviteRequestPrefab;
		public Transform inviteRequestsParentIfInGame;
		public Transform inviteRequestsParentIfNotInGame;
		public PartyMemberInfoIndicator partyMemberInfoIndicatorPrefab;
		public Transform partyMemberInfoIndicatorsParent;
		public static uint otherPartyMemberCount;
		public static List<string> usernamesToInvite = new List<string>();
		public static List<string> localPendingInvites = new List<string>();
		public static List<string> localRejectedInvites = new List<string>();

		public void InviteUsers ()
		{
			NetworkManager.client.GameRequests.Send("invitefriend", null, usernamesToInvite.ToArray(), OnSendGameRequestSuccess, NetworkManager.DisplayError);
		}

		void OnSendGameRequestSuccess ()
		{
			GameManager.instance.DisplayNotification ("Invite sent!");
			gameObject.SetActive(false);
		}

		void OnApplicationQuit ()
		{
			usernamesToInvite.Clear();
			localPendingInvites.Clear();
			localRejectedInvites.Clear();
		}
	}
}