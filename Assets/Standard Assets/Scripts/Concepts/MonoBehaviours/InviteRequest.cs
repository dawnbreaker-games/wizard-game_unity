using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace WizardGame
{
	public class InviteRequest : MonoBehaviour
	{
		public CanvasGroup canvasGroup;
		public _Text text;
		string gameModeName;
		string inviterUsername;
		string[] otherPartyMembersUsernames = new string[0];
		const string REPLACE_STRING = "_";

		public void Init (string gameModeName, string inviterUsername, string[] otherPartyMembersUsernames)
		{
			this.gameModeName = gameModeName;
			this.inviterUsername = inviterUsername;
			this.otherPartyMembersUsernames = otherPartyMembersUsernames;
			InviteFriendsMenu.localPendingInvites.Add(inviterUsername);
			text.Text = text.Text.Replace(REPLACE_STRING, inviterUsername);
		}

		public void Reject ()
		{
			InviteFriendsMenu.localRejectedInvites.Add(inviterUsername);
			Destroy(gameObject);
		}

		public void Accept ()
		{
			canvasGroup.interactable = false;
			if (!string.IsNullOrEmpty(gameModeName))
			{
				MainMenu.isPrivate = true;
				MainMenu.MatchId = inviterUsername;
				_SceneManager.instance.NextScene ();
			}
			else
			{
				AddPartyMember (inviterUsername);
				for (int i = 0; i < otherPartyMembersUsernames.Length; i ++)
					AddPartyMember (otherPartyMembersUsernames[i]);
				NetworkManager.client.GameRequests.Send("invitefriend", new Dictionary<string, string>() { { "", "" } }, new string[] { inviterUsername }, null, NetworkManager.DisplayError);
				Destroy(gameObject);
			}
		}

		void OnDestroy ()
		{
			InviteFriendsMenu.localPendingInvites.Remove(inviterUsername);
		}

		public static void AddPartyMember (string inviterUsername)
		{
			PartyMemberInfoIndicator partyMemberInfoIndicator = Instantiate(InviteFriendsMenu.instance.partyMemberInfoIndicatorPrefab, InviteFriendsMenu.instance.partyMemberInfoIndicatorsParent);
			partyMemberInfoIndicator.Init (inviterUsername);
			InviteFriendsMenu.otherPartyMemberCount ++;
		}
	}
}