using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class Item : MonoBehaviour
	{
		public Player owner;
		public Transform trs;
		public uint cost;
		public _Text pickupText;
		public Collider pickupCollider;
		public Transform bottomTrs;
		[HideInInspector]
		public bool isInShop;
		public string description;
		public byte maxCount;
		public Item[] combinedWith = new Item[0];
		PickupUpdater pickupUpdater;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (combinedWith.Length == 0)
				combinedWith = GetComponentsInChildren<Item>().Remove(this);
		}
#endif

		public virtual void OnGain (Player player)
		{
			for (int i = 0; i < combinedWith.Length; i ++)
			{
				Item item = combinedWith[i];
				if (item.gameObject.activeSelf)
					item.OnGain (player);
			}
			owner = player;
			trs.SetParent(owner.itemsParent);
			gameObject.SetActive(false);
		}

		public virtual void OnTriggerEnter (Collider other)
		{
			pickupUpdater = new PickupUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(pickupUpdater);
		}

		public virtual void OnTriggerExit (Collider other)
		{
			GameManager.updatables = GameManager.updatables.Remove(pickupUpdater);
		}
		
		public virtual void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(pickupUpdater);
		}

		public override string ToString ()
		{
			string output = description;
			for (int i = 0; i < combinedWith.Length; i ++)
			{
				Item item = combinedWith[i];
				if (string.IsNullOrEmpty(description) && i == 0)
					output += item.ToString(); 
				else
					output += "\n" + item.ToString(); 
			}
			return output;
		}

		class PickupUpdater : IUpdatable
		{
			Item item;

			public PickupUpdater (Item item)
			{
				this.item = item;
			}

			public void DoUpdate ()
			{
				RaycastHit hit;
				if (item == null)
					return;
				if (Physics.Raycast(new Ray(Player.instance.headTrs.position, Player.instance.headTrs.forward), out hit, Mathf.Infinity, LayerMask.GetMask(LayerMask.LayerToName(item.pickupCollider.gameObject.layer))) && hit.collider == item.pickupCollider)
				{
					if (item.isInShop)
						item.pickupText.Text = "Press E to buy";
					else
						item.pickupText.Text = "Press E to pickup";
					item.pickupText.gameObject.SetActive(true);
					if (InputManager.InteractInput && (!item.isInShop || Player.instance.money >= item.cost))
					{
						if (item.isInShop)
							Player.instance.AddMoney ((int) -item.cost);
						item.pickupText.gameObject.SetActive(false);
						item.OnGain (Player.instance);
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
				else
					item.pickupText.gameObject.SetActive(false);
			}
		}
	}
}