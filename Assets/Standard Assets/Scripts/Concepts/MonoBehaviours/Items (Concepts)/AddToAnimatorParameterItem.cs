using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class AddToAnimatorParameterItem : Item
	{
		public string parameterName;
		public float amount;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			Animator animator = owner.animator;
			animator.SetFloat(parameterName, animator.GetFloat(parameterName) + amount);
		}
	}
}