using System;
using Extensions;

namespace WizardGame
{
	public class AddToBulletDamageMultiplierItem : Item
	{
		public float amount;
		public static float multiplier = 1;
		bool owned;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			float previousMultiplier = multiplier;
			multiplier += amount;
			for (int i = 0; i < Player.instance.bulletPatternEntriesSortedList.Values.Count; i ++)
			{
				BulletPatternEntry bulletPatternEntry = Player.instance.bulletPatternEntriesSortedList.Values[i];
				bulletPatternEntry.bulletPrefab.damage /= previousMultiplier;
				bulletPatternEntry.bulletPrefab.damage *= multiplier;
			}
			owned = true;
		}

		void OnDestroy ()
		{
			if (!owned)
				return;
			float previousMultiplier = multiplier;
			multiplier -= amount;
			for (int i = 0; i < Player.instance.bulletPatternEntriesSortedList.Values.Count; i ++)
			{
				BulletPatternEntry bulletPatternEntry = Player.instance.bulletPatternEntriesSortedList.Values[i];
				bulletPatternEntry.bulletPrefab.damage /= previousMultiplier;
				bulletPatternEntry.bulletPrefab.damage *= multiplier;
			}
		}
	}
}