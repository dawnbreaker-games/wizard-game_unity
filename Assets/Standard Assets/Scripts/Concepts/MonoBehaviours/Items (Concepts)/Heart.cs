using Extensions;

namespace WizardGame
{
	public class Heart : Item
	{
		public override void OnGain (Player player)
		{
			base.OnGain (player);
			owner.TakeDamage (-1, null);
			owner.items = owner.items.Remove(this);
			Destroy(gameObject);
		}
	}
}