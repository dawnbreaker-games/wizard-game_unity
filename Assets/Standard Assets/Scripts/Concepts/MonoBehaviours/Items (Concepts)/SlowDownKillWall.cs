using System;
using Extensions;

namespace WizardGame
{
	public class SlowDownKillWall : Item
	{
		public float amount;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			KillWall.instance.moveSpeed -= amount;
			Destroy(gameObject);
		}
	}
}