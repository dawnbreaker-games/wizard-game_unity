using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class AntigravityItem : UseableItem, IUpdatable
	{
		public float maxCharge;
		public Transform chargeIndicatorTrs;
		public float chargeRate;
		float charge;
		float previousCharge;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			charge = maxCharge;
		}

		public override void Use ()
		{
			if (charge < 1)
				return;
			charge --;
			base.Use ();
			Level.instance.trs.rotation = Quaternion.FromToRotation(Physics.gravity, owner.headTrs.forward);
			chargeIndicatorTrs.localScale = new Vector2(charge / maxCharge, 1);
			GameManager.updatables = GameManager.updatables.Remove(this);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			previousCharge = charge;
			charge = Mathf.Clamp(charge + chargeRate * Time.deltaTime, 0, maxCharge);
			if ((int) charge > (int) previousCharge)
			{
				chargeIndicatorTrs.localScale = new Vector2(charge / maxCharge, 1);
				if (charge == maxCharge)
					GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}
	}
}