using UnityEngine;

namespace WizardGame
{
	public class BombItem : UseableItem
	{
		public float range;
		public uint maxBullets;
		
		public override void Use ()
		{
			base.Use ();
			if (Bullet.instances.Count <= maxBullets)
			{
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					if ((Player.instance.trs.position - bullet.trs.position).sqrMagnitude <= range * range)
					{
						ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
						i --;
					}
				}
			}
			else
			{
				Bullet.instances.Sort(CompareBullets);
				for (int i = 0; i < maxBullets; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					if ((Player.instance.trs.position - bullet.trs.position).sqrMagnitude <= range * range)
						ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
					else
						return;
				}
			}
		}

		int CompareBullets (Bullet bullet, Bullet bullet2)
		{
			float distanceSqr = (Player.instance.trs.position - bullet.trs.position).sqrMagnitude;
			float distanceSqr2 = (Player.instance.trs.position - bullet2.trs.position).sqrMagnitude;
			return (int) Mathf.Sign(distanceSqr2 - distanceSqr);
		}
	}
}