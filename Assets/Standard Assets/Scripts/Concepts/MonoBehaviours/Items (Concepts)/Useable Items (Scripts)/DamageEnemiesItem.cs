namespace WizardGame
{
	public class DamageEnemiesItem : UseableItem
	{
		public float damage;

		public override void Use ()
		{
			base.Use ();
			for (int i = 0; i < Enemy.instances.Count; i ++)
			{
				Enemy enemy = Enemy.instances[i];
				enemy.TakeDamage (damage, owner);
			}
		}
	}
}