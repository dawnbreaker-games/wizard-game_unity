using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class MagicBoots : UseableItem, IUpdatable
	{
		public float addToMoveSpeed;
		public TrailRenderer trailRenderer;
		public float dissolveRate;
		bool isTrailDissolving;

		public override void Use ()
		{
			base.Use ();
			isTrailDissolving = false;
			trailRenderer.widthCurve = AnimationCurve.Linear(0, Player.instance.radius * 2, 1, 0);
			trailRenderer.gameObject.SetActive(true);
			Player.instance.moveSpeed += addToMoveSpeed;
			Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, LayerMask.NameToLayer("Enemy Bullet"), true);
			Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, LayerMask.NameToLayer("Hazard"), true);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public void DoUpdate ()
		{
			if (!isTrailDissolving)
			{
				float timeOvershoot = Time.time - lastUseTime - maxUseTime;
				if (timeOvershoot >= 0 || useAction.ReadValue<float>() == 0)
				{
					lastUseTime = Time.time - timeOvershoot;
					isTrailDissolving = true;
					Player.instance.moveSpeed -= addToMoveSpeed;
					Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, LayerMask.NameToLayer("Enemy Bullet"), false);
					Physics2D.IgnoreLayerCollision(Player.instance.gameObject.layer, LayerMask.NameToLayer("Hazard"), false);
				}
			}
			else
			{
				float startWidth = trailRenderer.widthCurve.keys[0].value - dissolveRate * Time.deltaTime;
				trailRenderer.widthCurve = AnimationCurve.Linear(0, startWidth, 1, 0);
				if (startWidth <= 0)
				{
					trailRenderer.gameObject.SetActive(false);
					trailRenderer.Clear();
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			trailRenderer.gameObject.SetActive(false);
			trailRenderer.Clear();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}
	}
}