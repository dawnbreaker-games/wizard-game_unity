using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class MagicMirror : UseableItem, IUpdatable
	{
		public Transform mirrorPositionIndicatorTrs;

		void OnEnable ()
		{
			mirrorPositionIndicatorTrs.SetParent(null);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		void OnDisable ()
		{
			if (mirrorPositionIndicatorTrs != null)
				mirrorPositionIndicatorTrs.gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			mirrorPositionIndicatorTrs.position = Level.instance.trs.position + (Level.instance.trs.position - Player.instance.trs.position);
			mirrorPositionIndicatorTrs.up = -Player.instance.trs.forward;
		}

		public override void Use ()
		{
            base.Use ();
			Player.instance.trs.position = Level.instance.trs.position + (Level.instance.trs.position - Player.instance.trs.position);
		}
	}
}