using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class PortalItem : UseableItem, IUpdatable
	{
		public Transform targetPositionIndicatorTrs;
		public Transform destinationIndicatorTrs;
		public FloatRange destinationDistanceRange;
		public float durationToChangeDestinationDistance;
		float destinationDistance;
		float destinationDistanceChangeSpeed;
		float destinationDistanceRangeSize;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			gameObject.SetActive(true);
			destinationDistanceRangeSize = destinationDistanceRange.max - destinationDistanceRange.min;
			destinationDistance = destinationDistanceRange.min;
			destinationDistanceChangeSpeed = destinationDistanceRangeSize / durationToChangeDestinationDistance;
		}

		public override void OnBeganUsing ()
		{
			base.OnBeganUsing ();
			destinationDistance = destinationDistanceRange.min;
			targetPositionIndicatorTrs.localPosition = Vector3.forward * destinationDistance;
			targetPositionIndicatorTrs.gameObject.SetActive(true);
			SetDestinationIndicatorPosition ();
			destinationIndicatorTrs.gameObject.SetActive(true);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void OnStopUsing ()
		{
			base.OnStopUsing ();
			GameManager.updatables = GameManager.updatables.Remove(this);
			if (destinationIndicatorTrs != null)
			{
				destinationIndicatorTrs.gameObject.SetActive(false);
				Player.instance.trs.position = destinationIndicatorTrs.position;
			}
			if (targetPositionIndicatorTrs != null)
				targetPositionIndicatorTrs.gameObject.SetActive(false);
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public void DoUpdate ()
		{
			if (destinationDistance < destinationDistanceRange.max)
			{
				destinationDistance += destinationDistanceChangeSpeed * Time.deltaTime;
				targetPositionIndicatorTrs.localPosition = Vector3.forward * destinationDistance;
			}
			else if (destinationDistance > destinationDistanceRange.max)
			{
				destinationDistance = destinationDistanceRange.max;
				targetPositionIndicatorTrs.localPosition = Vector3.forward * destinationDistance;
			}
			SetDestinationIndicatorPosition ();
		}

		void SetDestinationIndicatorPosition ()
		{
			RaycastHit hit;
			if (Player.instance.rigid.SweepTest(destinationIndicatorTrs.forward, out hit, destinationDistance))
				destinationIndicatorTrs.localPosition = Vector3.forward * hit.distance;
			else
				destinationIndicatorTrs.position = targetPositionIndicatorTrs.position;
		}
	}
}