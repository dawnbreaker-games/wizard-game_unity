using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class ShieldItem : UseableItem, IUpdatable
	{
		public float maxCharge;
		public float chargeRate;
		public Transform chargeIndicator;
		public static bool isUsing;
		public const float DIVIDE_DAMAGE = 2;
		float charge;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			charge = maxCharge;
		}

		public override void OnBeganUsing ()
		{
			base.OnBeganUsing ();
			GameManager.updatables = GameManager.updatables.Remove(this);
			isUsing = true;
			if (NetworkManager.connection != null)
				NetworkManager.connection.Send("Player Start Using Shield");
		}

		public override void OnStopUsing ()
		{
			base.OnStopUsing ();
			GameManager.updatables = GameManager.updatables.Add(this);
			isUsing = false;
			if (NetworkManager.connection != null)
				NetworkManager.connection.Send("Player Stop Using Shield");
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			GameManager.updatables = GameManager.updatables.Remove(this);
		}

		public override void Use ()
		{
			base.Use ();
			charge -= Time.deltaTime;
			chargeIndicator.localScale = new Vector2(Mathf.Clamp01(charge / maxCharge), 1);
			if (charge <= 0)
				OnStopUsing ();
		}

		public void DoUpdate ()
		{
            charge = Mathf.Clamp(charge + chargeRate * Time.deltaTime, 0, maxCharge);
			chargeIndicator.localScale = new Vector2(Mathf.Clamp01(charge / maxCharge), 1);
		}
	}
}