namespace WizardGame
{
	public class UseableWeapon : UseableItem
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;
		public bool autoShoot;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			if (pickupCollider != null)
				pickupCollider.gameObject.SetActive(false);
			gameObject.SetActive(true);
			owner.bulletPatternEntriesSortedList.Add(bulletPatternEntry.name, bulletPatternEntry);
		}

		public override void Use ()
		{
            base.Use ();
			if (autoShoot)
	            bulletPatternEntry.Shoot ();
			if (animationEntry.animator != null)
				animationEntry.Play ();
		}
	}
}