using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;

namespace WizardGame
{
	public class UseableItem : Item
	{
		public InputAction useAction;
		public float cooldown;
		[HideInInspector]
		public float cooldownRemaining;
		public float maxUseTime;
		public Transform cooldownIndicator;
		protected float lastUseTime;
		UseUpdater useUpdater;
		CooldownUpdater cooldownUpdater;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			useAction.Enable();
			if (cooldownIndicator != null && cooldown > 0)
				cooldownIndicator.parent.gameObject.SetActive(true);
		}

		public void TryToUse (InputAction.CallbackContext context = default(InputAction.CallbackContext))
		{
			if (!GameManager.paused && useUpdater == null && Time.time - lastUseTime >= cooldown)
				Use ();
		}

		public virtual void Use ()
		{
			lastUseTime = Time.time;
			if (cooldownIndicator != null)
				cooldownIndicator.localScale = new Vector2(0, 1);
			if (maxUseTime > 0)
			{
				if (useUpdater == null)
					OnBeganUsing ();
			}
			else
			{
				cooldownUpdater = new CooldownUpdater(this);
				GameManager.updatables = GameManager.updatables.Add(cooldownUpdater);
			}
		}

		public virtual void OnBeganUsing ()
		{
			useUpdater = new UseUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(useUpdater);	
		}

		public virtual void OnStopUsing ()
		{
			float timeOvershoot = Time.time - lastUseTime - maxUseTime;
			if (timeOvershoot > 0)
				lastUseTime = Time.time - timeOvershoot;
			GameManager.updatables = GameManager.updatables.Remove(useUpdater);
			useUpdater = null;
			cooldownUpdater = new CooldownUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(cooldownUpdater);
		}

		public override void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(useUpdater);
			GameManager.updatables = GameManager.updatables.Remove(cooldownUpdater);
			useAction.Disable();
		}

		public override string ToString ()
		{
			string output = base.ToString() + " ";
			if (maxUseTime > 0)
			{
				output += "Max use time: " + maxUseTime;
				if (maxUseTime < Mathf.Infinity)
					output += "s. ";
				else
					output += " ";
			}
			output += "Cooldown: " + cooldown + "s.\n";
			return output;
		}

		class UseUpdater : IUpdatable
		{
			UseableItem useableItem;
			float usedTime;

			public UseUpdater (UseableItem useableItem)
			{
				this.useableItem = useableItem;
			}

			public void DoUpdate ()
			{
				if (useableItem.useAction.ReadValue<float>() == 1)
				{
					useableItem.Use ();
					usedTime += Time.deltaTime;
					if (usedTime >= useableItem.maxUseTime)
						useableItem.OnStopUsing ();
				}
				else
					useableItem.OnStopUsing ();
			}
		}

		class CooldownUpdater : IUpdatable
		{
			UseableItem useableItem;

			public CooldownUpdater (UseableItem useableItem)
			{
				this.useableItem = useableItem;
			}

			public void DoUpdate ()
			{
				if (useableItem == null || useableItem.cooldownIndicator == null)
				{
					GameManager.updatables = GameManager.updatables.Remove(this);
					return;
				}
				useableItem.cooldownIndicator.localScale = new Vector2((Time.time - useableItem.lastUseTime) / useableItem.cooldown, 1);
				if (Time.time - useableItem.lastUseTime >= useableItem.cooldown)
				{
					useableItem.cooldownIndicator.localScale = Vector2.one;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}