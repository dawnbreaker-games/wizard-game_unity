using UnityEngine;

namespace WizardGame
{
	public class Weapon : Item
	{
		public BulletPatternEntry bulletPatternEntry;
		public AnimationEntry animationEntry;

		public override void OnGain (Player player)
		{
			base.OnGain (player);
			bulletPatternEntry.spawner = owner.bulletSpawnersParent;
			animationEntry.animator = owner.animator;
			owner.bulletPatternEntriesSortedList[bulletPatternEntry.name] = bulletPatternEntry;
			animationEntry.Play ();
		}

		void OnDestroy ()
		{
			if (owner == null)
				return;
			owner.bulletPatternEntriesSortedList.Remove(bulletPatternEntry.name);
			animationEntry.animator.Play("None", animationEntry.layer);
		}
	}
}