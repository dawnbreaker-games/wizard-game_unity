using UnityEngine;
using UnityEngine.UI;

namespace WizardGame
{
	public class LeaderboardEntry : MonoBehaviour
	{
		public _Text rankText;
		public _Text valueText;
		public _Text usernameText;
		public GameObject[] valueTypeIndicators = new GameObject[0];
		public Button addFriendButton;
		public Button removeFriendButton;
		public Button inviteFriendButton;

		void OnEnable ()
		{
			if (FriendsMenu.friendsUsernames.Contains(usernameText.Text))
			{
				addFriendButton.gameObject.SetActive(false);
				removeFriendButton.gameObject.SetActive(true);
				inviteFriendButton.gameObject.SetActive(true);
			}
			else if (AddFriendRequestsMenu.usernamesOfPendingInvites.Contains(usernameText.Text))
				addFriendButton.interactable = false;
		}

		public void AddFriend ()
		{
			addFriendButton.interactable = false;
			AddFriendMenu.usernameToAdd = usernameText.Text;
			AddFriendMenu.instance.AddUser ();
		}

		public void RemoveFriend ()
		{
			
		}

		public void InviteFriend ()
		{
			
		}

		public enum ValueType
		{
			Skill,
			Wins,
            Losses,
            WinLossRatio,
            Kills,
            Deaths,
            KillDeathRatio
		}
	}
}