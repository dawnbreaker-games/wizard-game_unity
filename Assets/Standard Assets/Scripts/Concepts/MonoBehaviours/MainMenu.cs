using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace WizardGame
{
	public class MainMenu : SingletonMonoBehaviour<MainMenu>
	{
		public TMP_InputField minPlayersInputField;
		public TMP_InputField maxPlayersInputField;
		public Toggle rankedToggle;
		public Toggle showOnlyCurrentSeasonToggle;
		public Toggle showAllLeaderboardEntriesToggle;
		public Toggle showOnlyYouAndFriendsLeaderboardEntriesToggle;
		public TMP_InputField usernameInputField;
		public TMP_InputField passwordInputField;
		public Toggle rememberLoginInfoToggle;
		public TMP_InputField matchIdInputField;
		public Selectable[] registerAndLoginSelectables = new Selectable[0];
		public Button[] submitButtons = new Button[0];
		public GameObject[] deactivateGosOnLogin = new GameObject[0];
		public GameObject activateGoOnLogin;
		public LeaderboardEntry leaderboardEntryPrefab;
		public Transform leaderboardEntriesParent;
		public uint maxLeaderboardEntriesPerPage;
		public _Text leaderboardPageText;
		public static bool isPrivate;
		public static uint MinPlayers
		{
			get
			{
				return (uint) SaveAndLoadManager.GetInt("Min players", 1);
			}
			set
			{
				PlayerPrefs.SetInt("Min players", (int) value);
			}
		}
		public static uint MaxPlayers
		{
			get
			{
				return (uint) SaveAndLoadManager.GetInt("Max players", 4);
			}
			set
			{
				PlayerPrefs.SetInt("Max players", (int) value);
			}
		}
		public static bool Ranked
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Ranked", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Ranked", value);
			}
		}
		public static bool IsPrivate
		{
			get
			{
				return isPrivate;
			}
			set
			{
				isPrivate = value;
			}
		}
		public static string MatchId
		{
			get
			{
				return PlayerPrefs.GetString("Match id");
			}
			set
			{
				PlayerPrefs.SetString("Match id", value);
			}
		}
		public static string RememberedUsername
		{
			get
			{
				return PlayerPrefs.GetString("Username");
			}
			set
			{
				PlayerPrefs.SetString("Username", value);
			}
		}
		public static string RememberedPassword
		{
			get
			{
				return PlayerPrefs.GetString("Password");
			}
			set
			{
				PlayerPrefs.SetString("Password", value);
			}
		}
		public static bool RememberLoginInfo
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Remember login info", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Remember login info", value);
			}
		}
		public static bool _ShowAllLeaderboardEntries
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Show all leaderboard entries", true);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Show all leaderboard entries", value);
			}
		}
		public static bool _ShowOnlyYouAndFriendLeaderboardEntries
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Show only you and friend leaderboard entries", true);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Show only you and friend leaderboard entries", value);
			}
		}
		static string password;
		static LeaderboardEntry.ValueType leaderboardValueType;
		static string[] usernames = new string[0];
		static DatabaseObject dbObj;
		static ResetLeaderboardUpdater resetLeaderboardUpdater = new ResetLeaderboardUpdater();
		static bool leaderboardIsOpen;
		static bool showCurrentSeasonLeaderboardInfo;
		static bool showAllLeaderboardEntries;
		static bool showOnlyYouAndFriendLeaderboardEntries;
		static uint leaderboardPage;

		public override void Awake ()
		{
			base.Awake ();
			minPlayersInputField.text = "" + MinPlayers;
			maxPlayersInputField.text = "" + MaxPlayers;
			rankedToggle.isOn = Ranked;
			showAllLeaderboardEntriesToggle.isOn = _ShowAllLeaderboardEntries;
			showOnlyCurrentSeasonToggle.isOn = _ShowOnlyYouAndFriendLeaderboardEntries;
			if (RememberLoginInfo)
			{
				usernameInputField.text = RememberedUsername;
				passwordInputField.text = RememberedPassword;
			}
			rememberLoginInfoToggle.isOn = RememberLoginInfo;
			matchIdInputField.text = MatchId;
		}

		public void SetMinPlayers (TMP_InputField inputField)
		{
			string text = GetInputFieldText(inputField);
			MinPlayers = uint.Parse(text);
			if (MinPlayers == 0)
			{
				MinPlayers = 1;
				inputField.text = "" + MinPlayers;
			}
			else if (MinPlayers > MaxPlayers)
			{
				MinPlayers = MaxPlayers;
				inputField.text = "" + MinPlayers;
			}
			if (MinPlayers != 2)
				rankedToggle.isOn = false;
		}

		public void SetMaxPlayers (TMP_InputField inputField)
		{
			string text = GetInputFieldText(inputField);
			MaxPlayers = uint.Parse(text);
			if (MaxPlayers == 0)
			{
				MaxPlayers = 1;
				inputField.text = "" + MaxPlayers;
			}
			else if (MaxPlayers < MinPlayers)
			{
				MaxPlayers = MinPlayers;
				inputField.text = "" + MaxPlayers;
			}
			if (MaxPlayers != 2)
				rankedToggle.isOn = false;
		}

		public void SetRanked (bool ranked)
		{
			Ranked = ranked;
			if (ranked)
			{
				minPlayersInputField.text = "2";
				maxPlayersInputField.text = "2";
			}
		}

		public void SetUsername (string username)
		{
			if (RememberLoginInfo)
				RememberedUsername = username;
			LocalUserInfo.username = username;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetPassword (string password)
		{
			if (RememberLoginInfo)
				RememberedPassword = password;
			MainMenu.password = password;
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = !string.IsNullOrEmpty(LocalUserInfo.username) && !string.IsNullOrEmpty(password);
			}
		}

		public void SetRememberLoginInfo (bool remember)
		{
			RememberLoginInfo = remember;
			if (remember)
			{
				RememberedUsername = LocalUserInfo.username;
				RememberedPassword = password;
			}
		}

		public void Register ()
		{
			rankedToggle.interactable = false;
			rankedToggle.isOn = false;
			if (string.IsNullOrEmpty(LocalUserInfo.username))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty username!");
				return;
			}
			else if (string.IsNullOrEmpty(password))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty password!");
				return;
			}
			Logout ();
			NetworkManager.Connect (OnConnectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnConnectSuccess_Register (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			NetworkManager.client.BigDB.LoadOrCreate("Usernames", "usernames", OnLoadUsernamesDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnLoadUsernamesDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			DatabaseArray usernamesDBArray = dbObj.GetArray("usernames");
			if (!usernamesDBArray.Contains(LocalUserInfo.username))
				usernamesDBArray.Add(LocalUserInfo.username);
			else
			{
				GameManager.instance.DisplayNotification ("An account with that username already exists!");
				return;
			}
			MainMenu.dbObj = dbObj;
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnSaveDBObjectSuccess_Register ()
		{
			DatabaseObject dbObj2 = new DatabaseObject();
			dbObj2.Set("password", password);
			dbObj2.Set("skill", (float) 0);
			dbObj2.Set("skillInCurrentSeason", (float) 0);
			dbObj2.Set("wins", (uint) 0);
			dbObj2.Set("winsInCurrentSeason", (uint) 0);
			dbObj2.Set("losses", (uint) 0);
			dbObj2.Set("lossesInCurrentSeason", (uint) 0);
			dbObj2.Set("kills", (uint) 0);
			dbObj2.Set("killsInCurrentSeason", (uint) 0);
			dbObj2.Set("deaths", (uint) 0);
			dbObj2.Set("deathsInCurrentSeason", (uint) 0);
			dbObj2.Set("lastMonthPlayed", (uint) DateTime.UtcNow.Month);
			dbObj2.Set("friends", new DatabaseArray());
			dbObj2.Set("otherPartyMembers", new DatabaseArray());
			NetworkManager.client.BigDB.CreateObject("PlayerObjects", LocalUserInfo.username, dbObj2, OnCreatePlayerDBObjectSuccess_Register, OnRegisterOrLoginFail);
		}

		void OnSaveDBObjectFail_Register (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			dbObj.Save(OnSaveDBObjectSuccess_Register, OnSaveDBObjectFail_Register);
		}

		void OnCreatePlayerDBObjectSuccess_Register (DatabaseObject dbObj)
		{
			for (int i = 0; i < deactivateGosOnLogin.Length; i ++)
			{
				GameObject go = deactivateGosOnLogin[i];
				go.SetActive(false);
			}
			activateGoOnLogin.SetActive(true);
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
			rankedToggle.interactable = true;
			GameManager.updatables = GameManager.updatables.Add(resetLeaderboardUpdater);
		}

		void OnRegisterOrLoginFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
		}

		public void Login ()
		{
			rankedToggle.interactable = false;
			rankedToggle.isOn = false;
			if (string.IsNullOrEmpty(LocalUserInfo.username))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty username!");
				return;
			}
			else if (string.IsNullOrEmpty(password))
			{
				GameManager.instance.DisplayNotification ("You can't have an empty password!");
				return;
			}
			Logout ();
			NetworkManager.Connect (OnConnectSuccess_Login, OnRegisterOrLoginFail);
		}

		public void OnConnectSuccess_Login (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			NetworkManager.client.BigDB.Load("PlayerObjects", LocalUserInfo.username, OnLoadPlayerDBObjectSuccess_Login, OnRegisterOrLoginFail);
		}

		void OnLoadPlayerDBObjectSuccess_Login (DatabaseObject dbObj)
		{
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = true;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = true;
			}
			if (dbObj.GetString("password") != password)
			{
				GameManager.instance.DisplayNotification ("Incorrect login info.");
				return;
			}
			LocalUserInfo.skill = dbObj.GetFloat("skill");
			LocalUserInfo.skillInCurrentSeason = dbObj.GetFloat("skillInCurrentSeason");
			LocalUserInfo.wins = dbObj.GetUInt("wins");
			LocalUserInfo.winsInCurrentSeason = dbObj.GetUInt("winsInCurrentSeason");
			LocalUserInfo.losses = dbObj.GetUInt("losses");
			LocalUserInfo.lossesInCurrentSeason = dbObj.GetUInt("lossesInCurrentSeason");
			LocalUserInfo.kills = dbObj.GetUInt("kills");
			LocalUserInfo.killsInCurrentSeason = dbObj.GetUInt("killsInCurrentSeason");
			LocalUserInfo.deaths = dbObj.GetUInt("deaths");
			LocalUserInfo.deathsInCurrentSeason = dbObj.GetUInt("deathsInCurrentSeason");
			LocalUserInfo.lastMonthPlayed = dbObj.GetUInt("lastMonthPlayed");
			DatabaseArray friendsDBArray = dbObj.GetArray("friends");
			FriendsMenu.friendsUsernames.Clear();
			for (int i = 0; i < friendsDBArray.Count; i ++)
			{
				string friend = friendsDBArray.GetString(i);
				FriendsMenu.friendsUsernames.Add(friend);
			}
			for (int i = 0; i < deactivateGosOnLogin.Length; i ++)
			{
				GameObject go = deactivateGosOnLogin[i];
				go.SetActive(false);
			}
			activateGoOnLogin.SetActive(true);
			rankedToggle.interactable = true;
			GameManager.updatables = GameManager.updatables.Add(resetLeaderboardUpdater);
		}

		void UnloadLeaderboard ()
		{
			leaderboardIsOpen = false;
			if (leaderboardEntriesParent == null)
				return;
			for (int i = 0; i < leaderboardEntriesParent.childCount; i ++)
			{
				Transform child = leaderboardEntriesParent.GetChild(i);
				Destroy(child.gameObject);
			}
		}

		void LoadLeaderboard ()
		{
			leaderboardIsOpen = true;
			leaderboardPage = 0;
			if (leaderboardPageText != null)
				leaderboardPageText.Text = "Page 1 / " + Mathf.Clamp(usernames.Length / maxLeaderboardEntriesPerPage, 1, uint.MaxValue);
			NetworkManager.client.BigDB.Load("Usernames", "usernames", OnLoadDBObjectSuccess_LoadLeaderboard, OnLoadDBObjectFail_LoadLeaderboard);
		}

		public void ReloadLeaderboard ()
		{
			if (NetworkManager.client == null)
			{
				NetworkManager.Connect (OnConnectSuccess_ReloadLeaderboard, NetworkManager.DisplayError);
				return;
			}
			UnloadLeaderboard ();
			LoadLeaderboard ();
		}

		void OnConnectSuccess_ReloadLeaderboard (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			ReloadLeaderboard ();
		}

		public void SetLeaderboardValueType (int valueTypeIndex)
		{
			leaderboardValueType = (LeaderboardEntry.ValueType) Enum.ToObject(typeof(LeaderboardEntry.ValueType), valueTypeIndex);
			ReloadLeaderboard ();
		}

		void OnLoadDBObjectSuccess_LoadLeaderboard (DatabaseObject dbObj)
		{
			DatabaseArray dbArray = dbObj.GetArray("usernames");
			if (dbArray.Count == 0)
				return;
			usernames = new string[dbArray.Count];
			for (int i = 0; i < dbArray.Count; i ++)
				usernames[i] = dbArray.GetString(i);
			NetworkManager.client.BigDB.LoadKeys("PlayerObjects", usernames, OnLoadDBObjectsSuccess, OnLoadDBObjectsFail);
		}
		
		void OnLoadDBObjectsSuccess (DatabaseObject[] dbObjs)
		{
			SortedDictionary<float, List<string>> leaderboardValuesDict = new SortedDictionary<float, List<string>>(new ValueComparer());
			for (int i = 0; i < dbObjs.Length; i ++)
			{
				DatabaseObject dbObj = dbObjs[i];
				uint lastMonthPlayed = dbObj.GetUInt("lastMonthPlayed");
				if ((uint) ((float) lastMonthPlayed / 3) % 4 == (uint) ((float) LocalUserInfo.lastMonthPlayed / 3) % 4) // Checks if the user has played in the same season as the local user
				{
					float value = 0;
					if (showCurrentSeasonLeaderboardInfo)
					{
						if (leaderboardValueType == LeaderboardEntry.ValueType.Skill)
							value = dbObj.GetFloat("skillInCurrentSeason");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Wins)
							value = dbObj.GetUInt("winsInCurrentSeason");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Losses)
							value = dbObj.GetUInt("lossesInCurrentSeason");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.WinLossRatio)
							value = (float) dbObj.GetUInt("winsInCurrentSeason") / dbObj.GetUInt("lossesInCurrentSeason");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Kills)
							value = dbObj.GetUInt("killsInCurrentSeason");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Deaths)
							value = dbObj.GetUInt("deathsInCurrentSeason");
						else// if (leaderboardValueType == LeaderboardEntry.ValueType.KillDeathRatio)
							value = (float) dbObj.GetUInt("killsInCurrentSeason") / dbObj.GetUInt("deathsInCurrentSeason");
					}
					else
					{
						if (leaderboardValueType == LeaderboardEntry.ValueType.Skill)
							value = dbObj.GetFloat("skill");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Wins)
							value = dbObj.GetUInt("wins");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Losses)
							value = dbObj.GetUInt("losses");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.WinLossRatio)
							value = (float) dbObj.GetUInt("wins") / dbObj.GetUInt("losses");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Kills)
							value = dbObj.GetUInt("kills");
						else if (leaderboardValueType == LeaderboardEntry.ValueType.Deaths)
							value = dbObj.GetUInt("deaths");
						else// if (leaderboardValueType == LeaderboardEntry.ValueType.KillDeathRatio)
							value = (float) dbObj.GetUInt("kills") / dbObj.GetUInt("deaths");
					}
					List<string> usernamesWithValue = new List<string>();
					if (leaderboardValuesDict.TryGetValue(value, out usernamesWithValue))
						usernamesWithValue.Add(usernames[i]);
					else
						usernamesWithValue = new List<string>(new string[] { usernames[i] });
					leaderboardValuesDict[value] = usernamesWithValue;
				}
			}
			uint rank = 1;
			uint entryIndex = 0;
			foreach (KeyValuePair<float, List<string>> keyValuePair in leaderboardValuesDict)
			{
				for (int i = 0; i < keyValuePair.Value.Count; i ++)
				{
					if (entryIndex >= leaderboardPage * maxLeaderboardEntriesPerPage)
					{
						string username = keyValuePair.Value[i];
						if (showAllLeaderboardEntries || (showOnlyYouAndFriendLeaderboardEntries && FriendsMenu.friendsUsernames.Contains(username)) || username == LocalUserInfo.username)
						{
							LeaderboardEntry leaderboardEntry = Instantiate(leaderboardEntryPrefab, leaderboardEntriesParent);
							leaderboardEntry.rankText.Text = "" + rank;
							leaderboardEntry.usernameText.Text = username;
							leaderboardEntry.valueText.Text = "" + keyValuePair.Key.ToString("F0");
							leaderboardEntry.valueTypeIndicators[leaderboardValueType.GetHashCode()].SetActive(true);
						}
					}
					entryIndex ++;
					if (entryIndex > leaderboardPage * maxLeaderboardEntriesPerPage + maxLeaderboardEntriesPerPage)
						return;
				}
				rank ++;
				if (rank > maxLeaderboardEntriesPerPage)
					return;
			}
		}

		void OnLoadDBObjectsFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.client.BigDB.LoadKeys("PlayerObjects", usernames, OnLoadDBObjectsSuccess, OnLoadDBObjectsFail);
		}

		void OnLoadDBObjectFail_LoadLeaderboard (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.client.BigDB.Load("Usernames", "usernames", OnLoadDBObjectSuccess_LoadLeaderboard, OnLoadDBObjectFail_LoadLeaderboard);
		}

		static void ResetLeaderboard ()
		{
			if (NetworkManager.client == null)
			{
				NetworkManager.Connect (OnConnectSuccess_ResetLeaderboard, NetworkManager.DisplayError);
				return;
			}
			NetworkManager.client.BigDB.LoadMyPlayerObject(OnLoadDBObjectSuccess_ResetLeaderboard, OnLoadDBObjectFail_ResetLeaderboard);
		}

		static void OnConnectSuccess_ResetLeaderboard (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			ResetLeaderboard ();
		}

		static void OnLoadDBObjectSuccess_ResetLeaderboard (DatabaseObject dbObj)
		{
			MainMenu.dbObj = dbObj;
			dbObj.Set("skillInCurrentSeason", (float) 0);
			LocalUserInfo.skillInCurrentSeason = 0;
			dbObj.Set("winsInCurrentSeason", (uint) 0);
			LocalUserInfo.winsInCurrentSeason = 0;
			dbObj.Set("lossesInCurrentSeason", (uint) 0);
			LocalUserInfo.lossesInCurrentSeason = 0;
			dbObj.Set("killsInCurrentSeason", (uint) 0);
			LocalUserInfo.killsInCurrentSeason = 0;
			dbObj.Set("deathsInCurrentSeason", (uint) 0);
			LocalUserInfo.deathsInCurrentSeason = 0;
			LocalUserInfo.lastMonthPlayed = (uint) DateTime.UtcNow.Month;
			dbObj.Set("lastMonthPlayed", LocalUserInfo.lastMonthPlayed);
			dbObj.Save(OnSaveDBObjectSuccess_ResetLeaderboard, OnSaveDBObjectFail_ResetLeaderboard);
		}

		static void OnLoadDBObjectFail_ResetLeaderboard (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			NetworkManager.client.BigDB.LoadMyPlayerObject(OnLoadDBObjectSuccess_ResetLeaderboard, OnLoadDBObjectFail_ResetLeaderboard);
		}

		static void OnSaveDBObjectSuccess_ResetLeaderboard ()
		{
			if (instance == null)
				return;
			bool previouslyOpen = leaderboardIsOpen;
			instance.UnloadLeaderboard ();
			if (previouslyOpen)
				instance.LoadLeaderboard ();
		}

		static void OnSaveDBObjectFail_ResetLeaderboard (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			dbObj.Save(OnSaveDBObjectSuccess_ResetLeaderboard, OnSaveDBObjectFail_ResetLeaderboard);
		}

		public void ShowCurrentSeasonLeaderboardInfo (bool showCurrentSeasonLeaderboardInfo)
		{
			MainMenu.showCurrentSeasonLeaderboardInfo = showCurrentSeasonLeaderboardInfo;
			ReloadLeaderboard ();
		}

		public void ShowAllLeaderboardEntries (bool showAllLeaderboardEntries)
		{
			MainMenu.showAllLeaderboardEntries = showAllLeaderboardEntries;
			if (showAllLeaderboardEntries)
				showOnlyYouAndFriendsLeaderboardEntriesToggle.SetIsOnWithoutNotify(false);
			ReloadLeaderboard ();
		}

		public void ShowOnlyYouAndFriendLeaderboardEntries (bool showOnlyYouAndFriendLeaderboardEntries)
		{
			MainMenu.showOnlyYouAndFriendLeaderboardEntries = showOnlyYouAndFriendLeaderboardEntries;
			if (showOnlyYouAndFriendLeaderboardEntries)
				showAllLeaderboardEntriesToggle.SetIsOnWithoutNotify(false);
			ReloadLeaderboard ();
		}

		public void NextLeaderboardPage ()
		{
			if (leaderboardPage < usernames.Length / maxLeaderboardEntriesPerPage)
				leaderboardPage ++;
			else
				leaderboardPage = 0;
			leaderboardPageText.Text = "Page " + (leaderboardPage + 1) + " / " + Mathf.Clamp(usernames.Length / maxLeaderboardEntriesPerPage, 1, uint.MaxValue);
			ReloadLeaderboard ();
		}

		public void PreviousLeaderboardPage ()
		{
			if (leaderboardPage > 0)
				leaderboardPage --;
			else
				leaderboardPage = (uint) (usernames.Length / maxLeaderboardEntriesPerPage);
			leaderboardPageText.Text = "Page " + (leaderboardPage + 1) + " / " + Mathf.Clamp(usernames.Length / maxLeaderboardEntriesPerPage, 1, uint.MaxValue);
			ReloadLeaderboard ();
		}

		void Logout ()
		{
			for (int i = 0; i < submitButtons.Length; i ++)
			{
				Button submitButton = submitButtons[i];
				submitButton.interactable = false;
			}
			for (int i = 0; i < registerAndLoginSelectables.Length; i ++)
			{
				Selectable selectable = registerAndLoginSelectables[i];
				selectable.interactable = false;
			}
			FriendsMenu.friendsUsernames.Clear();
			GameManager.updatables = GameManager.updatables.Remove(resetLeaderboardUpdater);
			NetworkManager.Disconnect ();
		}

		string GetInputFieldText (TMP_InputField inputField)
		{
			string output = inputField.text;
			if (string.IsNullOrEmpty(output))
				output = ((TMP_Text) inputField.placeholder).text;
			return output;
		}

		void OnApplicationQuit ()
		{
			GameManager.updatables = GameManager.updatables.Remove(resetLeaderboardUpdater);
		}

		class ValueComparer : IComparer<float>
		{
			public int Compare (float value, float value2)
			{
				int multiplier = 1;
				if (leaderboardValueType == LeaderboardEntry.ValueType.Losses || leaderboardValueType == LeaderboardEntry.ValueType.Deaths)
					multiplier *= -1;
				if (value > value2)
					return -multiplier;
				else if (value < value2)
					return multiplier;
				else
					return 0;
			}
		}

		public class ResetLeaderboardUpdater : IUpdatable
		{
			public void DoUpdate ()
			{
				uint month = (uint) DateTime.UtcNow.Month;
				if (month != LocalUserInfo.lastMonthPlayed)
				{
					LocalUserInfo.lastMonthPlayed = month;
					if (month % 3 == 0)
						ResetLeaderboard ();
				}
			}
		}
	}
}