using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace WizardGame
{
	public class Match : SingletonMonoBehaviour<Match>
	{
		public Player playerPrefab;
		public Transform[] playerSpawnTransforms = new Transform[0];
		public float reviveDelay;
		public float makeVulnerableDelay;
		public uint minTimeWithNoDeathToEndRound;
		public float multiplyAnimatorSpeedsAtRoundEnd;
		public _Text waitingForMorePlayersText;
		public _Text localPlayerHpText;
		public Canvas canvas;
		public static Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();
		public static Dictionary<uint, Team> playerTeamsDict = new Dictionary<uint, Team>();
		public static Player localPlayer;
		public static UIntRange playerCountRange;
		public static bool ranked;
		public static bool isPublic;
		public static float animatorSpeed;
		public static uint localPlayerId;
		static ReviveUpdater reviveUpdater;
		static int lastSpawnedAtSpawnPointIndexForLocalPlayer;
		static byte maxScore;
		static bool hasEnded;
		const string PRIVATE_GAME_INDICATOR = "Private: ";
		const float MAX_SKILL_IN_CURRENT_SEASON_DIFFERENCE = .7f;

		public override void Awake ()
		{
			base.Awake ();
			playersDict.Clear();
			playerTeamsDict.Clear();
			hasEnded = false;
			animatorSpeed = playerPrefab.animator.speed;
			NetworkManager.Disconnect ();
			NetworkManager.Connect (OnConnectSuccess, OnConnectFail);
		}

		void MakeDefaultMatch ()
		{
			playerCountRange = new UIntRange(2, 2);
			isPublic = true;
			ranked = true;
			maxScore = 20;
			MakeMatch ();
		}
		
		void MakeCustomMatch ()
		{
			playerCountRange = new UIntRange(MainMenu.MinPlayers, MainMenu.MaxPlayers);
			maxScore = 20;
			MakeMatch ();
		}

		void MakeMatch ()
		{
			if (isPublic)
				NetworkManager.client.Multiplayer.CreateJoinRoom(LocalUserInfo.username, name, true, GetMakeRoomData(), null, OnMakeOrJoinMatchSucess, NetworkManager.DisplayError);
			else
				NetworkManager.client.Multiplayer.CreateJoinRoom(PRIVATE_GAME_INDICATOR + MainMenu.MatchId, name, false, GetMakeRoomData(), null, OnMakeOrJoinMatchSucess, NetworkManager.DisplayError);
		}
		
		void ListMatches ()
		{
			isPublic = !MainMenu.isPrivate;
			ranked = isPublic && MainMenu.Ranked;
			NetworkManager.client.Multiplayer.ListRooms(name, new Dictionary<string, string>() { { "ranked", ranked.ToString() } }, 0, 0, OnListMatchesSuccess, NetworkManager.DisplayError);
		}

		void OnListMatchesSuccess (RoomInfo[] roomInfos)
		{
			for (int i = 0; i < roomInfos.Length; i ++)
			{
				RoomInfo roomInfo = roomInfos[i];
				if (roomInfo.OnlineUsers < uint.Parse(roomInfo.RoomData["maxPlayers"]) && (isPublic || roomInfo.Id == PRIVATE_GAME_INDICATOR + MainMenu.MatchId) && (!ranked || Mathf.Abs(LocalUserInfo.skillInCurrentSeason - float.Parse(roomInfo.RoomData["skillInCurrentSeason"])) <= MAX_SKILL_IN_CURRENT_SEASON_DIFFERENCE))
				{
					JoinMatch (roomInfo, OnMakeOrJoinMatchSucess, NetworkManager.DisplayError);
					return;
				}
			}
			MakeCustomMatch ();
		}

		void OnConnectSuccess (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.GameRequests.Refresh(NetworkManager.OnRefreshGameRequestsSuccess);
			NetworkManager.instance.enabled = true;
			client.Multiplayer.UseSecureConnections = true;
			ListMatches ();
		}

		void OnConnectFail (PlayerIOError error)
		{
			GameManager.instance.DisplayNotification ("Error: " + error.ToString());
			NetworkManager.Connect (OnConnectSuccess, OnConnectFail);
		}

		Dictionary<string, string> GetMakeRoomData ()
		{
			Dictionary<string, string> output = new Dictionary<string, string>();
			output.Add("makeVulnerableDelay" , "" + makeVulnerableDelay);
			output.Add("minPlayers" , "" + playerCountRange.min);
			output.Add("maxPlayers" , "" + playerCountRange.max);
			output.Add("ranked" , ranked.ToString());
			output.Add("spawnPointCount" , "" + playerSpawnTransforms.Length);
			output.Add("minTimeWithNoDeathToEndRound", "" + minTimeWithNoDeathToEndRound);
			output.Add("maxScore", "" + maxScore);
			if (ranked)
				output.Add("skillInCurrentSeason", "" + LocalUserInfo.skillInCurrentSeason);
			return output;
		}

		Dictionary<string, string> GetJoinRoomData ()
		{
			return null;
		}

		void JoinMatch (RoomInfo roomInfo, Callback<Connection> onSuccess, Callback<PlayerIOError> onFail)
		{
			makeVulnerableDelay = float.Parse(roomInfo.RoomData["makeVulnerableDelay"]);
			playerCountRange = new UIntRange();
			playerCountRange.min = uint.Parse(roomInfo.RoomData["minPlayers"]);
			playerCountRange.max = uint.Parse(roomInfo.RoomData["maxPlayers"]);
			ranked = bool.Parse(roomInfo.RoomData["ranked"]);
			maxScore = byte.Parse(roomInfo.RoomData["maxScore"]);
			NetworkManager.client.Multiplayer.JoinRoom(roomInfo.Id, GetJoinRoomData(), onSuccess, onFail);
		}

		void OnMakeOrJoinMatchSucess (Connection connection)
		{
			NetworkManager.connection = connection;
			connection.OnMessage += OnMessage;
			connection.OnDisconnect += OnDisconnect;
			VoiceChat.instance.Init ();
		}

		void OnMessage (object sender, Message message)
		{
			switch (message.Type)
			{
				case "Spawn Player":
					OnSpawnPlayerMessage (message);
					break;
				case "Remove Player":
					OnRemovePlayerMessage (message);
					break;
				case "Move Player":
					OnMovePlayerMessage (message);
					break;
				case "Rotate Player":
					OnRotatePlayerMessage (message);
					break;
				case "Player Shoot":
					OnPlayerShootMessage (message);
					break;
				case "Player Start Using Shield":
					OnPlayerStartUsingShield (message);
					break;
				case "Player Stop Using Shield":
					OnPlayerStopUsingShield (message);
					break;
				case "Kill Player":
					OnKillPlayerMessage (message);
					break;
				case "Revive Player":
					OnRevivePlayerMessage (message);
					break;
				case "Make Player Vulnerable":
					OnMakePlayerVulnerableMessage (message);
					break;
				case "End Round":
					OnEndRoundMessage (message);
					break;
			}
		}

		void OnDisconnect (object sender, string reason)
		{
			if (!hasEnded)
				End ();
		}

		void OnSpawnPlayerMessage (Message message)
		{
			Transform spawnPoint = playerSpawnTransforms[message.GetUInt(4)];
			Player player = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
			Color teamColor = new Color(message.GetFloat(1), message.GetFloat(2), message.GetFloat(3));
			uint playerId = message.GetUInt(0);
			playerTeamsDict.Add(playerId, new Team(teamColor));
			if (message.Count == 6)
			{
				localPlayer = player;
				localPlayerId = playerId;
			}
			player.Init (playerId);
 			Scoreboard.instance.AddEntry (player.owner);
			if (playersDict.Count >= playerCountRange.min)
			{
				waitingForMorePlayersText.gameObject.SetActive(false);
				if (localPlayer != null)
					localPlayer.enabled = true;
			}
			else
				UpdateWaitingForMorePlayersText ();
		}

		void OnRemovePlayerMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
				Destroy(player.gameObject);
			if (Scoreboard.instance != null)
				Scoreboard.instance.RemoveEntry (playerTeamsDict[playerId]);
			playersDict.Remove(playerId);
			playerTeamsDict.Remove(playerId);
			if (playersDict.Count < playerCountRange.min)
				UpdateWaitingForMorePlayersText ();
		}

		void OnMovePlayerMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
			{
				Vector3 position = new Vector3(message.GetFloat(1), message.GetFloat(2), message.GetFloat(3));
				player.trs.localPosition = position;
			}
		}

		void OnRotatePlayerMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
			{
				Vector3 rotation = new Vector3(message.GetFloat(1), message.GetFloat(2), message.GetFloat(3));
				player.trs.localEulerAngles = Vector3.up * rotation.y;
				player.headTrs.localEulerAngles = rotation;
			}
		}

		void OnPlayerShootMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
			{
				Bullet[] bullets = player.ShootBulletPatternEntry("Shoot Where Facing");
				for (int i = 0; i < bullets.Length; i ++)
				{
					Bullet bullet = bullets[i];
					Physics.IgnoreCollision(player.characterController, bullet.collider, true);
					bullet.collider.gameObject.layer = LayerMask.NameToLayer("Enemy Bullet");
				}
			}
		}

		void OnPlayerStartUsingShield (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
				player.shieldIndicatorGo.SetActive(true);
		}

		void OnPlayerStopUsingShield (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
				player.shieldIndicatorGo.SetActive(false);
		}

		void OnKillPlayerMessage (Message message)
		{
			uint killerId = message.GetUInt(0);
			Player killer = playersDict[killerId];
			uint victimId = message.GetUInt(1);
			Player victim = playersDict[victimId];
			OnPlayerDied (victim, killer);
		}

		void OnRevivePlayerMessage (Message message)
		{
			RevivePlayer (message.GetUInt(0), playerSpawnTransforms[message.GetUInt(1)]);
		}

		void OnMakePlayerVulnerableMessage (Message message)
		{
			uint playerId = message.GetUInt(0);
			Player player = playersDict[playerId];
			if (player != null)
				MakePlayerVulnerable (player);
		}
		
		void OnEndRoundMessage (Message message)
		{
			animatorSpeed *= multiplyAnimatorSpeedsAtRoundEnd;
			foreach (KeyValuePair<uint, Player> keyValuePair in playersDict)
				keyValuePair.Value.animator.speed = animatorSpeed;
		}

		public void OnPlayerDied (Player victim, Player killer)
		{
			Destroy(victim.gameObject);
			if (killer != victim)
			{
				killer.owner.score ++;
				Scoreboard.instance.UpdateEntry (killer.owner);
				if (ranked && localPlayer == killer)
				{
					LocalUserInfo.kills ++;
					LocalUserInfo.killsInCurrentSeason ++;
				}
				else if (localPlayer == victim)
				{
					LocalUserInfo.deaths ++;
					LocalUserInfo.deathsInCurrentSeason ++;
					NetworkManager.connection.Send("Kill Player", killer.id);
					reviveUpdater = new ReviveUpdater(this, victim.id);
					GameManager.updatables = GameManager.updatables.Add(reviveUpdater);
				}
				if (!hasEnded && killer.owner.score >= maxScore)
				{
					if (ranked)
					{
						if (localPlayer == killer)
						{
							float skillChange = 1f - ((float) victim.owner.score / maxScore);
							LocalUserInfo.skill += skillChange;
							LocalUserInfo.skillInCurrentSeason ++;
							LocalUserInfo.wins ++;
							LocalUserInfo.winsInCurrentSeason ++;
						}
						else
						{
							float skillChange = 1f - ((float) localPlayer.owner.score / maxScore);
							LocalUserInfo.skill -= skillChange;
							LocalUserInfo.skillInCurrentSeason -= skillChange;
							LocalUserInfo.losses ++;
							LocalUserInfo.lossesInCurrentSeason ++;
						}
					}
					End ();
				}
			}
			else
			{
				victim.owner.score --;
				Scoreboard.instance.UpdateEntry (victim.owner);
				if (localPlayer == victim)
				{
					LocalUserInfo.deaths ++;
					LocalUserInfo.deathsInCurrentSeason ++;
					NetworkManager.connection.Send("Kill Player", killer.id);
					reviveUpdater = new ReviveUpdater(this, victim.id);
					GameManager.updatables = GameManager.updatables.Add(reviveUpdater);
				}
			}
		}

		Player RevivePlayer (uint id, Transform spawnPoint)
		{
			Player player = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
			player.Init (id);
			return player;
		}

		void ReviveLocalPlayer (uint id)
		{
			lastSpawnedAtSpawnPointIndexForLocalPlayer = Random.Range(0, playerSpawnTransforms.Length);
			Player player = RevivePlayer(id, playerSpawnTransforms[lastSpawnedAtSpawnPointIndexForLocalPlayer]);
			player.enabled = true;
		}

		void MakePlayerVulnerable (Player player)
		{
			player.invulnerable = false;
			player.invulnerableIndicator.SetActive(false);
		}

		void End ()
		{
			hasEnded = true;
			_SceneManager.instance.LoadScene (0);
		}

		void UpdateWaitingForMorePlayersText ()
		{
			uint playersRemaining = playerCountRange.min - (uint) playersDict.Count;
			string waitingForMorePlayersString = "Waiting for " + playersRemaining + " more players";
			if (playersRemaining < 2)
				waitingForMorePlayersString = waitingForMorePlayersString.Remove(waitingForMorePlayersString.Length - 1);
			waitingForMorePlayersText.Text = waitingForMorePlayersString;
		}

		public class Team : Team<Player>
		{
			public Color color;
			public int score;

			public Team ()
			{
			}

			public Team (Color color, int score = 0, string name = "", Player representative = default(Player), params Player[] representatives) : base(color, representative, representatives)
			{
				this.color = color;
				this.score = score;
			}
		}

		class ReviveUpdater : IUpdatable
		{
			Match match;
			uint playerId;
			float timer;

			public ReviveUpdater (Match match, uint playerId)
			{
				this.match = match;
				this.playerId = playerId;
				timer = match.reviveDelay;
			}

			public void DoUpdate ()
			{
				timer -= Time.deltaTime;
				if (timer <= 0)
				{
					match.ReviveLocalPlayer (playerId);
					if (localPlayerId == playerId)
						NetworkManager.connection.Send("Revive Player", lastSpawnedAtSpawnPointIndexForLocalPlayer);
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}