using UnityEngine;

namespace WizardGame
{
	public class MoveByMovementsOfOtherObjects : MonoBehaviour
	{
		public Transform trs;
		public Transform[] moveWith = new Transform[0];
		Vector3[] previousPositions = new Vector3[0];

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
		}
#endif

		void Awake ()
		{
			previousPositions = new Vector3[moveWith.Length];
			for (int i = 0; i < moveWith.Length; i ++)
				previousPositions[i] = moveWith[i].position;
		}

		public void Do ()
		{
			for (int i = 0; i < moveWith.Length; i ++)
			{
				Transform moveWithTrs = moveWith[i];
				trs.position += moveWithTrs.position - previousPositions[i];
				previousPositions[i] = moveWithTrs.position;
			}
		}
	}
}