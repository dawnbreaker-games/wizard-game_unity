using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class Scoreboard : SingletonMonoBehaviour<Scoreboard>
	{
		public ScoreboardEntry scoreboardEntryPrefab;
		public Transform scoreboardEntriesParent;
		static Dictionary<Match.Team, ScoreboardEntry> scoreboardEntriesDict = new Dictionary<Match.Team, ScoreboardEntry>();

		public void AddEntry (Match.Team team)
		{
			ScoreboardEntry scoreboardEntry = Instantiate(scoreboardEntryPrefab, scoreboardEntriesParent);
			scoreboardEntry.Init (team);
			scoreboardEntriesDict.Add(team, scoreboardEntry);
		}

		public void UpdateEntry (Match.Team team)
		{
			ScoreboardEntry scoreboardEntry = scoreboardEntriesDict[team];
			scoreboardEntry.scoreText.Text = "" + team.score;
			foreach (KeyValuePair<Match.Team, ScoreboardEntry> keyValuePair in scoreboardEntriesDict)
			{
				if (keyValuePair.Value.trs == null)
					return;
				if (team.score >= keyValuePair.Key.score)
					scoreboardEntry.trs.SetSiblingIndex(keyValuePair.Value.trs.GetSiblingIndex() - 1);
				else
					scoreboardEntry.trs.SetSiblingIndex(keyValuePair.Value.trs.GetSiblingIndex() + 1);
			}
		}

		public void RemoveEntry (Match.Team team)
		{
			ScoreboardEntry scoreboardEntry;
			if (scoreboardEntriesDict.TryGetValue(team, out scoreboardEntry))
			{
				Destroy(scoreboardEntry.gameObject);
				scoreboardEntriesDict.Remove(team);
			}
		}
	}
}