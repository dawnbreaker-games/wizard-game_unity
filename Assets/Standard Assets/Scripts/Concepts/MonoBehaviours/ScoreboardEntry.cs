using UnityEngine;
using UnityEngine.UI;

namespace WizardGame
{
	public class ScoreboardEntry : MonoBehaviour
	{
		public Transform trs;
		public _Text scoreText;
		public Image teamColorIndicator;

		public void Init (Match.Team team)
		{
			Material material = new Material(teamColorIndicator.material);
			material.SetColor("_tint", team.color);
			teamColorIndicator.material = material;
		}
	}
}