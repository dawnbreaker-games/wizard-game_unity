using UnityEngine;

namespace WizardGame
{
	public class Game : SingletonMonoBehaviour<Game>
	{
		public Transform[] spawnPoints = new Transform[0];
		public Player playerPrefab;

		public Player SpawnPlayer (uint playerId, int spawnPointIndex)
		{
			Transform spawnPoint = spawnPoints[spawnPointIndex];
			Player player = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
			player.id = playerId;
			return player;
		}
	}
}