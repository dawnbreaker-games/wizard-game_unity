using UnityEngine;

namespace WizardGame
{
	public class Roguelike : Game
	{
		public _Text scoreText;
		public _Text bestScoreText;
		public _Text killWallDistanceText;
		public int BestScore
		{
			get
			{
				return PlayerPrefs.GetInt(name + " best score");
			}
			set
			{
				PlayerPrefs.SetInt(name + " best score", value);
			}
		}
		public new static Roguelike instance;
		public new static Roguelike Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Roguelike>(true);
				return instance;
			}
        }
		int score;

		public override void Awake ()
		{
			base.Awake ();
			bestScoreText.Text = "" + BestScore;
		}

        public void DoUpdate ()
        {
			score = Mathf.Max(score, (int) Player.instance.trs.position.z);
			scoreText.Text = "" + score;
			if (score > BestScore)
			{
				BestScore = score;
				bestScoreText.Text = "" + score;
			}
			killWallDistanceText.Text = "" + (int) (Player.instance.characterController.bounds.min.z - KillWall.instance.trs.position.z);
        }
	}
}