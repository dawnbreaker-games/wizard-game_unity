using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace WizardGame
{
	public class ProceduralLevel : SingletonMonoBehaviour<ProceduralLevel>
	{
		public LevelPieceGroup levelPiecesGroup;
		public int alternateMaterialCount;
		[HideInInspector]
		public int alternateAreaIndex;
		public AudioSource musicAudioSource;
		public AudioClip[] alternateAreaMusics = new AudioClip[0];
		public List<ProceduralLevelPiece> madeLevelPieces = new List<ProceduralLevelPiece>();
		public List<Bounds> levelPiecesBounds = new List<Bounds>();
		public ProceduralLevelPiece furthestLevelPiece;
		public AnimationCurve difficultyOverDistanceCurve;
		public AnimationCurve itemsCostOverDistanceCurve;
		public Enemy[] enemyPrefabs = new Enemy[0];
		public Item[] itemPrefabs = new Item[0];
		public LayerMask whatIsGround;
		public static float targetDifficulty;
		public static float currentDifficulty;
		public static float targetItemsCost;
		public static float currentItemsCost;
		float furthestPointZ;
		ProceduralLevelPiece furthestLevelPiecePrefab;
		int furthestDeactivatedLevelPieceIndex;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			alternateAreaIndex = Random.Range(0, alternateMaterialCount);
			if (musicAudioSource != null)
				musicAudioSource.clip = alternateAreaMusics[alternateAreaIndex];
			furthestLevelPiecePrefab = levelPiecesGroup.startingLevelPieces[Random.Range(0, levelPiecesGroup.startingLevelPieces.Length)];
			furthestLevelPiece = Instantiate(furthestLevelPiecePrefab);
			targetDifficulty = 0;
			currentDifficulty = 0;
			targetItemsCost = 0;
			currentItemsCost = 0;
			furthestLevelPiece.Init (this);
			madeLevelPieces.Add(furthestLevelPiece);
			levelPiecesBounds.Add(furthestLevelPiece.bounds);
			furthestPointZ = furthestLevelPiece.bounds.max.z;
			targetDifficulty = difficultyOverDistanceCurve.Evaluate(furthestPointZ);
			targetItemsCost = itemsCostOverDistanceCurve.Evaluate(furthestPointZ);
			DoUpdate ();
		}

		public void DoUpdate ()
		{
			while (furthestPointZ <= Player.Instance.headTrs.position.z + CameraScript.Instance.camera.farClipPlane)
				MakeNextLevelPieces ();
		}

		ProceduralLevelPiece[] MakeNextLevelPieces ()
		{
			return MakeNextLevelPieces(furthestLevelPiece);
		}

		ProceduralLevelPiece[] MakeNextLevelPieces (ProceduralLevelPiece levelPiece)
		{
			ProceduralLevelPiece[] output = new ProceduralLevelPiece[levelPiece.proceduralLevelPieceScaffolds.Length];
			for (int i = 0; i < levelPiece.proceduralLevelPieceScaffolds.Length; i ++)
			{
				ProceduralLevelPieceScaffold scaffold = levelPiece.proceduralLevelPieceScaffolds[i];
				int levelPieceIndex = Random.Range(0, levelPiecesGroup.nonStartingLevelPieces.Length);
				output[i] = MakeNextLevelPiece(levelPiecesGroup.nonStartingLevelPieces[levelPieceIndex], scaffold);
			}
			return output;
		}

		ProceduralLevelPiece MakeNextLevelPiece (ProceduralLevelPiece levelPiecePrefab, ProceduralLevelPieceScaffold scaffold)
		{
			Vector3 offset = -levelPiecePrefab.bounds.center;
			Bounds scaffoldBounds = scaffold.boundsCollider.bounds;
			Bounds bounds = levelPiecePrefab.bounds.AnchorToPoint(scaffoldBounds, scaffold.anchorPoint);
			ProceduralLevelPiece levelPiece = Instantiate(levelPiecePrefab, offset + bounds.center, Quaternion.identity);
			levelPiece.Init (this);
			madeLevelPieces.Add(levelPiece);
			levelPiecesBounds.Add(levelPiece.bounds);
			furthestLevelPiece = levelPiece;
			furthestLevelPiecePrefab = levelPiecePrefab;
			furthestPointZ = levelPiece.bounds.max.z;
			targetDifficulty = difficultyOverDistanceCurve.Evaluate(furthestPointZ);
			targetItemsCost = itemsCostOverDistanceCurve.Evaluate(furthestPointZ);
			return levelPiece;
		}

		public void DeactivatePastLevelPieces ()
		{
		}

		public void ActivatePastLevelPieces ()
		{
		}

		[Serializable]
		public struct LevelPieceGroup
		{
			public ProceduralLevelPiece[] nonStartingLevelPieces;
			public ProceduralLevelPiece[] startingLevelPieces;
		}
	}
}