﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class Hazard : Spawnable
	{
		public float damage;
		public float radius;
		public Collider collider;
		public static List<Hazard> instances = new List<Hazard>();

		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances.Add(this);
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances.Remove(this);
		}
		
		public virtual void OnCollisionEnter (Collision coll)
		{
			IDestructable destructable = coll.collider.GetComponentInParent<IDestructable>();
			if (destructable != null)
				destructable.TakeDamage (damage, null);
		}
		
		public virtual void OnTriggerEnter (Collider other)
		{
			if (other.isTrigger)
				return;
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null)
				destructable.TakeDamage (damage, null);
		}
	}
}