using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace WizardGame
{
	public class ProceduralLevelPiece : MonoBehaviour
	{
		public Transform trs;
		public ProceduralLevel level;
		public ProceduralLevelPiece levelPieceOfMyScaffold;
		public ProceduralLevelPieceScaffold[] proceduralLevelPieceScaffolds = new ProceduralLevelPieceScaffold[0];
		public Collider[] colliders = new Collider[0];
		public List<ProceduralLevelPiece> neighbors = new List<ProceduralLevelPiece>();
		public ProceduralLevelPiece[] children = new ProceduralLevelPiece[0];
		public SerializableDictionary<Renderer, MaterialGroup> alternateMaterialGroupsDict = new SerializableDictionary<Renderer, MaterialGroup>();
		public ChangeTransform[] changeTransforms = new ChangeTransform[0];
		public ToggleGameObjectsBasedOnChance[] toggleGameObjects = new ToggleGameObjectsBasedOnChance[0];
		public MoveByMovementsOfOtherObjects[] moveByMovementsOfOtherObjects = new MoveByMovementsOfOtherObjects[0];
		public float multiplyDifficulty;
		public float addToDifficulty;
		public float multiplyItemsCost;
		public float addToItemsCost;
		[HideInInspector]
		public Bounds bounds;
		public Collider[] enemySpawnBoundsColliders = new Collider[0];
		public Collider[] itemSpawnBoundsColliders = new Collider[0];
#if UNITY_EDITOR
		public bool setBounds;
		public bool _changeTransforms;
		public bool _toggleGameObjects;

		void OnValidate ()
		{
			if (!Application.isPlaying)
			{
				proceduralLevelPieceScaffolds = GetComponentsInChildren<ProceduralLevelPieceScaffold>();
				children = GetComponentsInChildren<ProceduralLevelPiece>().Remove(this);
				if (colliders.Length == 0)
					colliders = GetComponentsInChildren<Collider>();
				if (setBounds)
				{
					setBounds = false;
					SetBounds ();
				}
				if (_changeTransforms)
				{
					_changeTransforms = false;
					ChangeTransforms ();
				}
				if (_toggleGameObjects)
				{
					_toggleGameObjects = false;
					ToggleGameObjects ();
				}
				return;
			}
		}
#endif

		public virtual void Init (ProceduralLevel level)
		{
			this.level = level;
			alternateMaterialGroupsDict.Init ();
			foreach (KeyValuePair<Renderer, MaterialGroup> keyValuePair in alternateMaterialGroupsDict)
				keyValuePair.Key.sharedMaterial = keyValuePair.Value.materials[level.alternateAreaIndex];
			ToggleGameObjects ();
			ChangeTransforms ();
			MoveByMovementsOfOtherObjects ();
			SetBounds ();
			SpawnEnemies ();
			SpawnItems ();
		}

		public void ChangeTransforms ()
		{
			for (int i = 0; i < changeTransforms.Length; i ++)
			{
				ChangeTransform changeTransform = changeTransforms[i];
				changeTransform.Do ();
			}
		}

		public void ToggleGameObjects ()
		{
			for (int i = 0; i < toggleGameObjects.Length; i ++)
			{
				ToggleGameObjectsBasedOnChance toggleGameObject = toggleGameObjects[i];
				toggleGameObject.Do ();
			}
		}

		public void MoveByMovementsOfOtherObjects ()
		{
			for (int i = 0; i < moveByMovementsOfOtherObjects.Length; i ++)
			{
				MoveByMovementsOfOtherObjects _moveByMovementsOfOtherObjects = moveByMovementsOfOtherObjects[i];
				_moveByMovementsOfOtherObjects.Do ();
			}
		}

		void SetBounds ()
		{
			Bounds[] collidersBounds = new Bounds[colliders.Length];
			for (int i = 0; i < colliders.Length; i ++)
			{
				Collider collider = colliders[i];
				collidersBounds[i] = collider.bounds;
			}
			bounds = BoundsExtensions.Combine(collidersBounds);
		}

		void SpawnEnemies ()
		{
			if (enemySpawnBoundsColliders.Length == 0)
				return;
			float difficulty = ProceduralLevel.targetDifficulty * multiplyDifficulty + addToDifficulty;
			difficulty += ProceduralLevel.targetDifficulty - ProceduralLevel.currentDifficulty;
			List<Enemy> enemyPrefabsRemaining = new List<Enemy>(level.enemyPrefabs);
			while (enemyPrefabsRemaining.Count > 0)
			{
				int enemyPrefabIndex = Random.Range(0, enemyPrefabsRemaining.Count);
				Enemy enemyPrefab = enemyPrefabsRemaining[enemyPrefabIndex];
				if (difficulty >= enemyPrefab.difficulty)
				{
					difficulty -= enemyPrefab.difficulty;
					ProceduralLevel.currentDifficulty += enemyPrefab.difficulty;
					SpawnEnemy (enemyPrefab);
				}
				else
					enemyPrefabsRemaining.RemoveAt(enemyPrefabIndex);
			}
		}

		Enemy SpawnEnemy (Enemy enemyPrefab)
		{
			List<Collider> enemySpawnBoundsCollidersRemaining = new List<Collider>(enemySpawnBoundsColliders);
			Collider enemySpawnBoundsCollider = null;
			while (true)
			{
				int enemySpawnBoundsColliderIndex = Random.Range(0, enemySpawnBoundsCollidersRemaining.Count);
				enemySpawnBoundsCollider = enemySpawnBoundsCollidersRemaining[enemySpawnBoundsColliderIndex];
				if (enemySpawnBoundsCollider.gameObject.activeInHierarchy)
					break;
				else
				{
					enemySpawnBoundsCollidersRemaining.RemoveAt(enemySpawnBoundsColliderIndex);
					if (enemySpawnBoundsCollidersRemaining.Count == 0)
						return null;
				}
			}
			Vector3 spawnPosition = enemySpawnBoundsCollider.bounds.RandomPoint();
			Quaternion spawnRotation = Quaternion.identity;
			return Instantiate(enemyPrefab, spawnPosition, spawnRotation);
		}

		void SpawnItems ()
		{
			if (itemSpawnBoundsColliders.Length == 0)
				return;
			float itemsCost = ProceduralLevel.targetItemsCost * multiplyItemsCost + addToItemsCost;
			itemsCost += ProceduralLevel.targetItemsCost - ProceduralLevel.currentItemsCost;
			List<Item> itemPrefabsRemaining = new List<Item>(level.itemPrefabs);
			while (itemPrefabsRemaining.Count > 0)
			{
				int itemPrefabIndex = Random.Range(0, itemPrefabsRemaining.Count);
				Item itemPrefab = itemPrefabsRemaining[itemPrefabIndex];
				if (itemsCost >= itemPrefab.cost)
				{
					itemsCost -= itemPrefab.cost;
					ProceduralLevel.currentItemsCost += itemPrefab.cost;
					SpawnItem (itemPrefab);
				}
				else
					itemPrefabsRemaining.RemoveAt(itemPrefabIndex);
			}
		}

		Item SpawnItem (Item itemPrefab)
		{
			List<Collider> itemSpawnBoundsCollidersRemaining = new List<Collider>(itemSpawnBoundsColliders);
			Collider enemySpawnBoundsCollider = null;
			while (true)
			{
				int enemySpawnBoundsColliderIndex = Random.Range(0, itemSpawnBoundsCollidersRemaining.Count);
				enemySpawnBoundsCollider = itemSpawnBoundsCollidersRemaining[enemySpawnBoundsColliderIndex];
				if (enemySpawnBoundsCollider.gameObject.activeInHierarchy)
					break;
				else
				{
					itemSpawnBoundsCollidersRemaining.RemoveAt(enemySpawnBoundsColliderIndex);
					if (itemSpawnBoundsCollidersRemaining.Count == 0)
						return null;
				}
			}
			Vector3 spawnPosition = enemySpawnBoundsCollider.bounds.RandomPoint();
			Quaternion spawnRotation = Quaternion.identity;
			Item item = Instantiate(itemPrefab, spawnPosition, spawnRotation);
			RaycastHit hit;
			if (Physics.Raycast(new Ray(item.bottomTrs.position, Vector3.down), out hit, Mathf.Infinity, ProceduralLevel.instance.whatIsGround))
				item.trs.position += Vector3.down * hit.distance;
			return item;
		}

		[Serializable]
		public struct MaterialGroup
		{
			public Material[] materials;
		}
	}
}