using System;
using UnityEngine;

namespace WizardGame
{
	[Serializable]
	public class Team<T>
	{
		public T representative;
		public T[] representatives;
		public Color color;
		public Material material;
		public Team<T> opponent;
		public Team<T>[] opponents = new Team<T>[0];

		public Team ()
		{
		}

		public Team (Color color, T representative = default(T), params T[] representatives)
		{
			this.color = color;
			this.representatives = representatives;
		}
	}
}