using WizardGame;
using UnityEngine;

public class TextFader : MonoBehaviour
{
	public _Text text;
	public float alphaWhenCollidersInside;
	uint collidersInside;

#if UNITY_EDITOR
	void OnValidate ()
	{
		if (text == null)
			text = GetComponent<_Text>();
	}
#endif

	void OnTriggerEnter2D (Collider2D other)
	{
		collidersInside ++;
		if (collidersInside == 1)
			text.SetAlpha (alphaWhenCollidersInside);
	}

	void OnTriggerExit2D (Collider2D other)
	{
		collidersInside --;
		if (collidersInside == 0)
			text.SetAlpha (1);
	}
}