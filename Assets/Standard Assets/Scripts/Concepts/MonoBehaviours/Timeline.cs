using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class Timeline : MonoBehaviour
	{
		[HideInInspector]
		public float timeOfFirstPoint = Mathf.Infinity;
		[HideInInspector]
		public float timeOfLastPoint;
		public List<Point> points = new List<Point>();

		public Point InsertPointAtTime (float time, Bullet[] bullets, Explosion[] explosions)
		{
			Bullet.Snapshot[] bulletSnapshots = new Bullet.Snapshot[bullets.Length];
			for (int i = 0; i < bulletSnapshots.Length; i ++)
			{
				Bullet bullet = bullets[i];
				bulletSnapshots[i] = new Bullet.Snapshot(bullet);
			}
			Explosion.Snapshot[] explosionSnapshots = new Explosion.Snapshot[explosions.Length];
			for (int i = 0; i < explosionSnapshots.Length; i ++)
			{
				Explosion explosion = explosions[i];
				explosionSnapshots[i] = new Explosion.Snapshot(explosion);
			}
			return InsertPointAtTime(time, bulletSnapshots, explosionSnapshots);
		}

		public Point InsertPointAtTime (float time, Bullet.Snapshot[] bulletSnapshots = null, Explosion.Snapshot[] explosionSnapshots = null)
		{
			Point output = new Point(bulletSnapshots, explosionSnapshots);
			int pointIndex = GetPointIndexAtTime(time);
			if (pointIndex == points.Count)
			{
				points.Add(output);
				timeOfLastPoint = time;
				return output;
			}
			points.Insert(pointIndex, output);
			timeOfFirstPoint = Mathf.Min(time, timeOfFirstPoint);
			timeOfLastPoint = Mathf.Max(time, timeOfLastPoint);
			return output;
		}

		public void ApplyAtTime (float time)
		{
			Point point = points[GetPointIndexAtTime(time)];
			point.Apply ();
		}

		public void ApplyAtTimeTo (float time, Bullet[] bullets, Explosion[] explosions)
		{
			Point point = points[GetPointIndexAtTime(time)];
			point.ApplyTo (bullets, explosions);
		}

		public Point GetPointAtTime (float time)
		{
			return points[GetPointIndexAtTime(time)];
		}

		public int GetPointIndexAtTime (float time)
		{
			int pointCount = points.Count;
			if (time > timeOfLastPoint)
				return pointCount;
			int pointIndex = (int) ((float) pointCount * Mathf.InverseLerp(timeOfFirstPoint, timeOfLastPoint, time));
			pointIndex = Mathf.Clamp(pointIndex, 0, pointCount - 1);
			Point point = points[pointIndex];
			int changePointIndex = MathfExtensions.Sign(time - point.time);
			if (changePointIndex == 0)
				return pointIndex;
			for (; ; pointIndex += changePointIndex)
			{
				if (pointIndex < 0 || pointIndex >= points.Count)
					return Mathf.Clamp(pointIndex, 0, pointCount - 1);
				point = points[pointIndex];
				int newChangePointIndex = MathfExtensions.Sign(time - point.time);
				if (newChangePointIndex != changePointIndex)
				{
					if (newChangePointIndex == 0)
						return pointIndex;
					break;
				}
			}
			pointIndex = Mathf.Clamp(pointIndex - changePointIndex, 0, pointCount - 1);
			return pointIndex;
		}

		public struct Point
		{
			public float time;
			public Bullet.Snapshot[] bulletSnapshots;
			public Explosion.Snapshot[] explosionSnapshots;

			public Point (float time, Bullet.Snapshot[] bulletSnapshots, Explosion.Snapshot[] explosionSnapshots)
			{
				this.time = time;
				if (bulletSnapshots != null)
					this.bulletSnapshots = bulletSnapshots;
				else
				{
					this.bulletSnapshots = new Bullet.Snapshot[Bullet.instances.Count];
					for (int i = 0; i < this.bulletSnapshots.Length; i ++)
					{
						Bullet bullet = Bullet.instances[i];
						this.bulletSnapshots[i] = new Bullet.Snapshot(bullet);
					}
				}
				if (explosionSnapshots != null)
					this.explosionSnapshots = explosionSnapshots;
				else
				{
					this.explosionSnapshots = new Explosion.Snapshot[Explosion.instances.Count];
					for (int i = 0; i < this.explosionSnapshots.Length; i ++)
					{
						Explosion explosion = Explosion.instances[i];
						this.explosionSnapshots[i] = new Explosion.Snapshot(explosion);
					}
				}
			}

			public Point (Bullet.Snapshot[] bulletSnapshots, Explosion.Snapshot[] explosionSnapshots) : this (Level.currentTime, bulletSnapshots, explosionSnapshots)
			{
			}

			public void Apply ()
			{
				ApplyTo (Bullet.instances.ToArray(), Explosion.instances.ToArray());
			}

			public void ApplyTo (Bullet[] bullets, Explosion[] explosions)
			{
				EventManager.events.Clear();
				EventManager.currentEventIndex = 0;
				EventManager.previousEventCount = 0;
				List<Bullet> bullets2 = new List<Bullet>(bullets);
				for (int i = 0; i < bulletSnapshots.Length; i ++)
				{
					Bullet.Snapshot bulletSnapshot = bulletSnapshots[i];
					bullets2.Remove(bulletSnapshot.Apply());
				}
				for (int i = 0; i < bullets2.Count; i ++)
				{
					Bullet bullet = bullets2[i];
					if (bullet != null)
						ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
				}
				List<Explosion> explosions2 = new List<Explosion>(explosions);
				for (int i = 0; i < explosionSnapshots.Length; i ++)
				{
					Explosion.Snapshot explosionSnapshot = explosionSnapshots[i];
					explosions2.Remove(explosionSnapshot.Apply());
				}
				for (int i = 0; i < explosions2.Count; i ++)
				{
					Explosion explosion = explosions2[i];
					if (explosion != null)
						ObjectPool.instance.Despawn (explosion.prefabIndex, explosion.gameObject, explosion.trs);
				}
			}
		}
	}
}