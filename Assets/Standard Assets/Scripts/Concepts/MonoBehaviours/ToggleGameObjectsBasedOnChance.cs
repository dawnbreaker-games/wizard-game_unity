using UnityEngine;

namespace WizardGame
{
	public class ToggleGameObjectsBasedOnChance : MonoBehaviour
	{
		public GameObject[] gos = new GameObject[0];
		[Range(0, 1)]
		public float chance;

		public void Do ()
		{
			if (Random.value < chance)
			{
				for (int i = 0; i < gos.Length; i ++)
				{
					GameObject go = gos[i];
					go.SetActive(!go.activeSelf);
				}
			}
		}
	}
}