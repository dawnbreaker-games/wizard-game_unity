using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class EnemyGroup : UpdateWhileEnabled
	{
		public Transform trs;
		public Enemy[] enemies = new Enemy[0];
		public float stopRange;
		public LayerMask whatIPatrolOn;
#if UNITY_EDITOR
		public bool useCustomYPosition;
		public float yPosition;
#endif
		Vector3 destination;
		Vector3 toDestination;
		float patrolRange;
		float stopRangeSqr;

		void Awake ()
		{
			float patrolArea = 0;
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				patrolArea += new Circle2D(enemy.patrol.patrolRange).Area - new Circle2D(enemy.patrol.trs.lossyScale.x * enemy.patrol.controller.radius).Area;
			}
			patrolRange = (float) Mathf.Sqrt(patrolArea / Mathf.PI);
			stopRangeSqr = stopRange * stopRange;
		}

		public override void OnEnable ()
		{
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				enemy.patrol.enabled = false;
			}
			SetDestination ();
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = enemies[i];
				toDestination = (destination - enemy.patrol.trs.position).SetY(0);
				enemy.patrol.move = Vector3.ClampMagnitude(toDestination, 1);
				enemy.patrol.move *= enemy.patrol.moveSpeed;
				enemy.patrol.trs.forward = enemy.patrol.move;
				enemy.patrol.HandleGravity ();
				if (enemy.patrol.controller != null && enemy.patrol.controller.enabled)
					enemy.patrol.controller.Move(enemy.patrol.move * Time.deltaTime);
				else
					enemy.patrol.rigid.velocity = enemy.patrol.move;
				if (toDestination.sqrMagnitude <= stopRangeSqr || enemy.patrol.controller.collisionFlags.ToString().Contains("Sides"))
					SetDestination ();
			}
		}

		void SetDestination ()
		{
			while (true)
			{
				destination = trs.position + (Random.insideUnitCircle * patrolRange).XYToXZ();
				if (Physics.Raycast(destination.SetY(trs.position.y), Vector3.down, Mathf.Infinity, whatIPatrolOn))
					return;
			}
		}
	}
}