using UnityEngine;

namespace WizardGame
{
	public class Boss : Enemy
	{
		public static Boss instance;
		public static Boss Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Boss>();
				return instance;
			}
		}

		public override void TakeDamage (float amount, Player player)
		{
			float previousHp = hp;
			base.TakeDamage (amount, player);
			BossLevel bossLevel = (BossLevel) Level.instance;
			bossLevel.totalDamage += previousHp - hp;
			bossLevel.currentTimeText.Text = "Damage: " + string.Format("{0:0.#}", bossLevel.totalDamage);
			if (bossLevel.totalDamage >= bossLevel.totalMaxHp)
				bossLevel.End ();
		}
	}
}