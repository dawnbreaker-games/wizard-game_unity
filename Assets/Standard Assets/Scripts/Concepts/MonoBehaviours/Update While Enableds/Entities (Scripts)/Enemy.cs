using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace WizardGame
{
	public class Enemy : Entity
	{
		public FloatRange targetRangeFromPlayer;
		public Transform bulletSpawnersParent;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public float difficulty;
		public Patrol patrol;
		public AnimationEntry moveAnimationEntry;
		public AnimationEntry attackAnimationEntry;
		public LayerMask whatBlocksVision;
		public SphereCollider awakenRangeSphereCollider;
		public Transform eyesTrs;
		public float visionDegrees;
		public EnemyGroup enemyGroup;
		public LookAtPlayer lookAtPlayer;
		public float interestRange;
		public uint moneyReward;
		public static List<Enemy> instances = new List<Enemy>();
		float interestRangeSqr;
		float awakenRange;
		float awakenRangeSqr;
		bool awakened;

#if UNITY_EDITOR
		void OnValidate ()
		{
			enemyGroup = GetComponentInParent<EnemyGroup>();
		}
#endif

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instances.Add(this);
		}

		public override void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnDisable ();
			instances.Remove(this);
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (bulletSpawnersParent != null)
				Destroy(bulletSpawnersParent.gameObject);
		}

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			bulletSpawnersParent.SetParent(null);
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
			awakenRange = awakenRangeSphereCollider.bounds.extents.x;
			awakenRangeSqr = awakenRange * awakenRange;
			interestRangeSqr = interestRange * interestRange;
		}

		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleAttacking ();
			base.DoUpdate ();
			if ((Player.instance.trs.position - trs.position).sqrMagnitude > interestRangeSqr)
				LoseInterest ();
		}

		public override void HandleMoving ()
		{
			Vector3 toPlayer = Player.instance.trs.position - trs.position;
			float toPlayerDistanceSqr = toPlayer.sqrMagnitude;
			Vector3 velocity = Vector3.zero;
			if (toPlayerDistanceSqr > targetRangeFromPlayer.max * targetRangeFromPlayer.max)
				velocity = toPlayer.normalized * moveSpeed;
			else if (toPlayerDistanceSqr < targetRangeFromPlayer.min * targetRangeFromPlayer.min)
				velocity = -toPlayer.normalized * moveSpeed;
			rigid.velocity = velocity;
			moveAnimationEntry.Play ();
		}

		public override void Death (Player player)
		{
			base.Death (player);
			Player.instance.AddMoney ((int) moneyReward);
		}

		void HandleAttacking ()
		{
			bulletSpawnersParent.position = trs.position;
			attackAnimationEntry.Play ();
		}
		
		void OnTriggerStay (Collider other)
		{
			RaycastHit hit = new RaycastHit();
			if (!awakened && (Player.instance.trs.position - trs.position).sqrMagnitude <= awakenRangeSqr && Vector3.Angle(eyesTrs.forward, Player.instance.trs.position - eyesTrs.position) <= visionDegrees && Physics.Raycast(eyesTrs.position, Player.instance.trs.position - eyesTrs.position, out hit, awakenRange, whatBlocksVision) && hit.rigidbody == Player.instance.rigid)
			{
				if (enemyGroup != null)
				{
					enemyGroup.enabled = false;
					for (int i = 0; i < enemyGroup.enemies.Length; i ++)
					{
						Enemy enemy = enemyGroup.enemies[i];
						if (!enemy.awakened)
							enemy.Awaken ();
					}
				}
				else
					Awaken ();
			}
		}

		void Awaken ()
		{
			awakened = true;
			if (lookAtPlayer != null)
				lookAtPlayer.enabled = true;
			if (patrol != null)
				patrol.enabled = false;
			awakenRangeSphereCollider.enabled = false;
			enabled = true;
		}

		void LoseInterest ()
		{
			awakened = false;
			if (lookAtPlayer != null)
				lookAtPlayer.enabled = false;
			if (enemyGroup != null)
			{
				for (int i = 0; i < enemyGroup.enemies.Length; i ++)
				{
					Enemy enemy = enemyGroup.enemies[i];
					if (enemy.awakened)
						LoseInterest ();
				}
				enemyGroup.enabled = true;
			}
			else if (patrol != null)
				patrol.enabled = true;
			awakenRangeSphereCollider.enabled = true;
			enabled = false;
		}

	}
}