using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace WizardGame
{
	[ExecuteInEditMode]
	public class Entity : UpdateWhileEnabled, IDestructable
	{
		[HideInInspector]
		public float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public Transform trs;
		public Rigidbody rigid;
		public float moveSpeed;
		public float radius;
		// public AnimationEntry[] animationEntries = new AnimationEntry[0];
		// public Dictionary<string, AnimationEntry> animationEntriesDict = new Dictionary<string, AnimationEntry>();
		// public AudioClip[] deathAudioClips = new AudioClip[0];
		[HideInInspector]
		public bool dead;

		public virtual void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				return;
			}
#endif
			hp = maxHp;
			// for (int i = 0; i < animationEntries.Length; i ++)
			// {
			// 	AnimationEntry animationEntry = animationEntries[i];
			// 	animationEntriesDict.Add(animationEntry.animatorStateName, animationEntry);
			// }
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandleRotating ();
			HandleMoving ();
		}

		public virtual void HandleRotating ()
		{
		}
		
		public virtual void HandleMoving ()
		{
		}

		public virtual void TakeDamage (float amount, Player player)
		{
			if (dead || maxHp < 0 || amount == 0)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			if (hp == 0)
			{
				dead = true;
				Death (player);
			}
		}

		public virtual void Death (Player player)
		{
			Destroy(gameObject);
			// PlayAnimationEntry ("Death");
			// AudioManager.instance.MakeSoundEffect (deathAudioClips[Random.Range(0, deathAudioClips.Length)], trs.position);
		}

		// public void PlayAnimationEntry (string name)
		// {
		// 	animationEntriesDict[name].Play ();
		// }
	}
}