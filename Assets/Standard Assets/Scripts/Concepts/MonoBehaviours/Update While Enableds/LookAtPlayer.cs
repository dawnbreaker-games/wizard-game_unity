﻿using UnityEngine;

namespace WizardGame
{
	public class LookAtPlayer : UpdateWhileEnabled
	{
		public Transform trs;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
		}
#endif

		public override void DoUpdate ()
		{
			trs.LookAt(Player.instance.trs);
		}
	}
}