﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class Patrol : UpdateWhileEnabled
	{
		public bool isFlying;
		public float patrolRange;
		public float stopRange;
		public LayerMask whatIPatrolOn;
		public Transform trs;
		public CharacterController controller;
		public Rigidbody rigid;
		public float moveSpeed;
		float yVel;
		[HideInInspector]
		public Vector3 move;
		public AnimationEntry moveAnimationEntry;
		Vector3 initPosition;
		Vector3 destination;
		float stopRangeSqr;
		Vector3 toDestination;

		void Awake ()
		{
			initPosition = trs.position;
			stopRangeSqr = stopRange * stopRange;
		}

		public override void OnEnable ()
		{
			SetDestination ();
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			toDestination = destination - trs.position;
			if (!isFlying)
				toDestination = toDestination.SetY(0);
			move = Vector3.ClampMagnitude(toDestination, 1);
			move *= moveSpeed;
			trs.forward = move;
			HandleGravity ();
			if (controller != null && controller.enabled)
				controller.Move(move * Time.deltaTime);
			else
				rigid.velocity = move;
			moveAnimationEntry.Play ();
			if (toDestination.sqrMagnitude <= stopRangeSqr || (controller != null && controller.collisionFlags.ToString().Contains("Sides")))
				SetDestination ();
		}

		void SetDestination ()
		{
			if (isFlying)
			{
				destination = initPosition + Random.onUnitSphere * Random.value * patrolRange;
				return;
			}
			while (true)
			{
				destination = initPosition + (Random.insideUnitCircle * patrolRange).XYToXZ();
				RaycastHit hit;
				if (Physics.Raycast(destination.SetY(trs.position.y + patrolRange), Vector3.down, out hit, Mathf.Infinity, whatIPatrolOn))
					return;
			}
		}

		public void HandleGravity ()
		{
			if (controller != null && controller.enabled && !controller.isGrounded)
			{
				yVel += Physics.gravity.y * Time.deltaTime;
				move += Vector3.up * yVel;
			}
			else
				yVel = 0;
		}
	}
}