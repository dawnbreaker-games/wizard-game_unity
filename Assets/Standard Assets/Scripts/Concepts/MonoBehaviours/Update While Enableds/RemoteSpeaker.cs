using UnityEngine;
using UnityEngine.UI;
using FrostweepGames.VoicePro;

namespace WizardGame
{
	public class RemoteSpeaker : UpdateWhileEnabled
	{
		public Speaker speaker;
		public _Text speakerNameText;
		public GameObject isTalkingIndicator;

		public void Init ()
		{
			speakerNameText.Text = speaker.Name;
		}
		
		public override void DoUpdate ()
		{
			isTalkingIndicator.SetActive(speaker.Playing);
		}

		public void Mute (bool mute)
		{
			speaker.IsMute = mute;
		}

		public void MuteForAllUsers (bool mute)
		{
			AdminTools.SetSpeakerMuteStatus(speaker, mute);
		}
	}
}