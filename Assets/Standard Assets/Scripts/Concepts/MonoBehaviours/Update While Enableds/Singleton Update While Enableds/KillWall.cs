using UnityEngine;

namespace WizardGame
{
	public class KillWall : SingletonUpdateWhileEnabled<KillWall>
	{
		public Transform trs;
		public float moveSpeed;
		public float addToMoveSpeedRate;

		public override void DoUpdate ()
		{
			moveSpeed += addToMoveSpeedRate * Time.deltaTime;
			trs.position += Vector3.forward * moveSpeed * Time.deltaTime;
			if (trs.position.z >= Player.instance.characterController.bounds.min.z && !Player.instance.dead)
				Player.instance.Death (null);
		}
	}
}