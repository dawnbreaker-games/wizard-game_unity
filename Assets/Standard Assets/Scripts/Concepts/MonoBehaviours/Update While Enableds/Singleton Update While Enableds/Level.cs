using System;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Threading.Tasks;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace WizardGame
{
	public class Level : SingletonUpdateWhileEnabled<Level>
	{
		public string displayName;
		public Transform trs;
		public float innerSize;
		public float size;
		public bool unlocked;
		public EnemySpawnEntry[] enemySpawnEntries = new EnemySpawnEntry[0];
		public AnimationCurve difficultyOverTimeCurve;
		public FloatRange spawnAtTimeIntervalRange;
		public GameObject moveToBeginTextGo;
		public _Text currentTimeText;
		public _Text bestTimeReachedText;
		public Transform playerSpawnPoint;
		public GameObject lockedIndicatorGo;
		public GameObject untriedIndicatorGo;
		public Type type;
		public GameObject fireTrailEmitterGo;
		public float windSpeed;
		public float rewindDelay;
		public float rewindDuration;
		public float branchInterval;
		public float divideObjectsAlphaInNextBranch;
		public Timeline timeline;
		public float divideDifficulty;
		public float addToDifficulty;
		[HideInInspector]
		public float rewindDurationRemaining;
		public float BestTimeReached
		{
			get
			{
				float output = 0;
				SaveAndLoadManager.saveData.bestLevelTimesDict.TryGetValue(name, out output);
				return output;
			}
			set
			{
				SaveAndLoadManager.saveData.bestLevelTimesDict[name] = value;
			}
		}
		public static List<List<Bullet.Snapshot>> bulletSnapshots = new List<List<Bullet.Snapshot>>();
		public static Level[] instances = new Level[0];
		public static float currentTime;
		static Bullet[] bulletsInBranch = new Bullet[0];
		static Explosion[] explosionsInBranch = new Explosion[0];
		static Bullet[] bulletsInNextBranch = new Bullet[0];
		static Explosion[] explosionsInNextBranch = new Explosion[0];
		float rewindDelayRemaining;
		float timeUntilBranch;
		float spawnAtTime;
		float difficulty;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (type.HasFlag(Type.Teleport))
			{
				Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
				for (int i = 0; i < colliders.Length; i ++)
				{
					Collider2D collider = colliders[i];
					if (collider.name != "Spawn Zone" && collider is not CompositeCollider2D)
						collider.usedByComposite = true;
				}
				CompositeCollider2D compositeCollider = gameObject.GetComponent<CompositeCollider2D>();
				if (compositeCollider == null)
					compositeCollider = gameObject.AddComponent<CompositeCollider2D>();
				compositeCollider.isTrigger = true;
				Rigidbody2D rigid = GetComponent<Rigidbody2D>();
				rigid.bodyType = RigidbodyType2D.Static;
			}
			else
			{
				Collider2D[] colliders = GetComponentsInChildren<Collider2D>();
				for (int i = 0; i < colliders.Length; i ++)
				{
					Collider2D collider = colliders[i];
					if (collider.name != "Spawn Zone" && collider is not CompositeCollider2D)
						collider.usedByComposite = false;
				}
				GameManager.DestroyOnNextEditorUpdate (GetComponent<CompositeCollider2D>());
				GameManager.DestroyOnNextEditorUpdate (GetComponent<Rigidbody2D>());
			}
		}
#endif

		public virtual void Begin ()
		{
			OnBegin ();
			for (int i = 0; i < enemySpawnEntries.Length; i ++)
			{
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[i];
				for (int i2 = 0; i2 < enemySpawnEntry.spawnZones.Length; i2 ++)
					enemySpawnEntry.spawnZones[i2] = new Zone2D(enemySpawnEntry.spawnZones[i2].collider);
				enemySpawnEntries[i] = enemySpawnEntry;
			}
			do
			{
				spawnAtTime += spawnAtTimeIntervalRange.Get(Random.value);
				difficulty += difficultyOverTimeCurve.Evaluate(spawnAtTime);
			} while (SpawnEnemies().Length == 0);
		}

		public void OnBegin ()
		{
			timeUntilBranch = branchInterval;
			bulletsInBranch = new Bullet[0];
			explosionsInBranch = new Explosion[0];
			bulletsInNextBranch = new Bullet[0];
			explosionsInNextBranch = new Explosion[0];
			moveToBeginTextGo.SetActive(false);
			bestTimeReachedText.gameObject.SetActive(false);
			untriedIndicatorGo.SetActive(false);
			if (!SaveAndLoadManager.saveData.triedPlayers.Contains(Player.Instance.name))
				SaveAndLoadManager.saveData.triedPlayers = SaveAndLoadManager.saveData.triedPlayers.Add(Player.instance.name);
			Player.instance.enabled = true;
			Player.instance.animator.enabled = true;
			Player.instance.untriedIndicatorGo.SetActive(false);
			if (type.HasFlag(Type.Fire))
				fireTrailEmitterGo.SetActive(true);
			enabled = true;
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			if (Boss.Instance == null)
			{
				while (GameManager.TimeSinceLevelLoad >= spawnAtTime)
				{
					spawnAtTime += spawnAtTimeIntervalRange.Get(Random.value);
					difficulty += difficultyOverTimeCurve.Evaluate(GameManager.TimeSinceLevelLoad);
					SpawnEnemies ();
				}
			}
			if (type.HasFlag(Type.TimeRewind))
			{
				if (rewindDelayRemaining > 0)
				{
					rewindDelayRemaining -= Time.deltaTime;
					currentTime += Time.deltaTime;
					timeline.InsertPointAtTime (currentTime);
					if (rewindDelayRemaining <= 0)
						rewindDurationRemaining = rewindDuration - rewindDelayRemaining;
				}
				else
				{
					rewindDurationRemaining -= Time.deltaTime;
					currentTime -= Time.deltaTime;
					for (int i = timeline.points.Count - 1; i >= 0; i --)
					{
						Timeline.Point timelinePoint = timeline.points[i];
						if (timelinePoint.time > currentTime)
							timeline.points.RemoveAt(i);
						else
							break;
					}
					Timeline.Point lastPoint = timeline.points[timeline.points.Count - 1];
					lastPoint.Apply ();
					timeline.timeOfLastPoint = lastPoint.time;
					if (rewindDurationRemaining <= 0)
						rewindDelayRemaining = rewindDelay - rewindDurationRemaining;
				}
			}
			else
				currentTime += Time.deltaTime;
			if (type.HasFlag(Type.Wind))
			{
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					bullet.extraVelocity = ((Vector2) (bullet.trs.position - trs.position)).Rotate270().normalized * windSpeed;
					bullet.rigid.velocity = bullet.velocity + bullet.extraVelocity;
					bullet.trs.forward = bullet.rigid.velocity;
				}
			}
			if (type.HasFlag(Type.Branch))
			{
				timeUntilBranch -= Time.deltaTime;
				List<Bullet> bullets = new List<Bullet>(Bullet.instances);
				bullets.RemoveEach(bulletsInBranch);
				List<Explosion> explosions = new List<Explosion>(Explosion.instances);
				explosions.RemoveEach(explosionsInBranch);
				timeline.InsertPointAtTime (currentTime, bullets.ToArray(), explosions.ToArray());
				if (timeUntilBranch <= 0)
				{
					for (int i = 0; i < bulletsInBranch.Length; i ++)
					{
						Bullet bullet = bulletsInBranch[i];
						if (bullet != null)
						{
							BombBullet bombBullet = bullet as BombBullet;
							if (bombBullet != null)
								bombBullet.explodeOnDisable = false;
							ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
						}
					}
					for (int i = 0; i < bulletsInNextBranch.Length; i ++)
					{
						Bullet bullet = bulletsInNextBranch[i];
						if (bullet != null)
							ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
					}
					for (int i = 0; i < explosionsInBranch.Length; i ++)
					{
						Explosion explosion = explosionsInBranch[i];
						if (explosion != null)
							ObjectPool.instance.Despawn (explosion.prefabIndex, explosion.gameObject, explosion.trs);
					}
					for (int i = 0; i < explosionsInNextBranch.Length; i ++)
					{
						Explosion explosion = explosionsInNextBranch[i];
						if (explosion != null)
							ObjectPool.instance.Despawn (explosion.prefabIndex, explosion.gameObject, explosion.trs);
					}
					ShowNextBranchIndicator ();
					MakeBranch ();
					int removeCount = timeline.GetPointIndexAtTime(currentTime + timeUntilBranch) + 1;
					removeCount = Mathf.Clamp(removeCount, 0, timeline.points.Count - 1);
					timeline.points.RemoveRange(0, removeCount);
					timeline.timeOfFirstPoint = timeline.points[0].time;
					timeUntilBranch += branchInterval;
				}
			}
			if (Boss.instance == null)
				currentTimeText.Text = string.Format("{0:0.#}", currentTime);
		}

		void ShowNextBranchIndicator ()
		{
			Timeline.Point nextBranchPoint = timeline.GetPointAtTime(currentTime + timeUntilBranch);
			bulletsInNextBranch = new Bullet[nextBranchPoint.bulletSnapshots.Length];
			for (int i = 0; i < bulletsInNextBranch.Length; i ++)
			{
				Bullet.Snapshot bulletSnapshot = nextBranchPoint.bulletSnapshots[i];
				bulletSnapshot.bullet = null;
				Bullet bullet = bulletSnapshot.Apply();
				BombBullet bombBullet = bullet as BombBullet;
				if (bombBullet != null)
					bombBullet.explodeOnDisable = false;
				bullet.enabled = false;
				// bullet.rigid.isKinematic = false;
				// bullet.spriteRenderer.color = bullet.spriteRenderer.color.DivideAlpha(divideObjectsAlphaInNextBranch);
				bulletsInNextBranch[i] = bullet;
			}
			explosionsInNextBranch = new Explosion[nextBranchPoint.explosionSnapshots.Length];
			for (int i = 0; i < explosionsInNextBranch.Length; i ++)
			{
				Explosion.Snapshot explosionSnapshot = nextBranchPoint.explosionSnapshots[i];
				explosionSnapshot.explosion = null;
				Explosion explosion = explosionSnapshot.Apply();
				explosion.enabled = false;
				explosion.collider.enabled = false;
				// explosion.spriteRenderer.color = explosion.spriteRenderer.color.DivideAlpha(divideObjectsAlphaInNextBranch);
				explosionsInNextBranch[i] = explosion;
			}
		}

		void MakeBranch ()
		{
			Timeline.Point branchPoint = timeline.GetPointAtTime(currentTime - branchInterval + timeUntilBranch);
			bulletsInBranch = new Bullet[branchPoint.bulletSnapshots.Length];
			for (int i = 0; i < bulletsInBranch.Length; i ++)
			{
				Bullet.Snapshot bulletSnapshot = branchPoint.bulletSnapshots[i];
				bulletSnapshot.bullet = null;
				bulletsInBranch[i] = bulletSnapshot.Apply();
			}
			explosionsInBranch = new Explosion[branchPoint.explosionSnapshots.Length];
			for (int i = 0; i < explosionsInBranch.Length; i ++)
			{
				Explosion.Snapshot explosionSnapshot = branchPoint.explosionSnapshots[i];
				explosionSnapshot.explosion = null;
				explosionsInBranch[i] = explosionSnapshot.Apply();
			}
		}

		Enemy[] SpawnEnemies ()
		{
			List<Enemy> output = new List<Enemy>();
			List<EnemySpawnEntry> remainingEnemySpawnEntries = new List<EnemySpawnEntry>(enemySpawnEntries);
			while (remainingEnemySpawnEntries.Count > 0)
			{
				int randomIndex = Random.Range(0, remainingEnemySpawnEntries.Count);
				EnemySpawnEntry enemySpawnEntry = remainingEnemySpawnEntries[randomIndex];
				float difficulty = enemySpawnEntry.enemyPrefab.difficulty / divideDifficulty / GameManager.instance.divideDifficulty;
				difficulty += addToDifficulty;
				if (this.difficulty - difficulty < 0)
					remainingEnemySpawnEntries.RemoveAt(randomIndex);
				else
				{
					output.Add(enemySpawnEntry.SpawnEnemy());
					this.difficulty -= difficulty;
				}
			}
			return output.ToArray();
		}

		public virtual void End ()
		{
			if (currentTime > BestTimeReached)
			{
				if (rewindDurationRemaining > 0)
					BestTimeReached = MathfExtensions.RoundToInterval(currentTime, rewindDelay, MathfExtensions.RoundingMethod.RoundUpIfNotInteger);
				else
					BestTimeReached = currentTime;
				Achievement.instances = FindObjectsOfType<Achievement>();
				for (int i = 0; i < Achievement.instances.Length; i ++)
				{
					Achievement achievement = Achievement.instances[i];
					if (!achievement.complete && achievement.ShouldBeComplete())
						achievement.Complete ();
				}
			}
			if (NetworkManager.client == null)
			{
				if (!string.IsNullOrEmpty(LocalUserInfo.username))
					NetworkManager.Connect (OnConnectSuccess, null);
			}
			else
				NetworkManager.client.BigDB.Load("PlayerObjects", LocalUserInfo.username, OnLoadDBObjectSuccess);
			currentTime = 0;
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			_SceneManager.instance.RestartScene ();
		}

		static void OnConnectSuccess (Client client)
		{
			NetworkManager.client = client;
			NetworkManager.client.BigDB.Load("PlayerObjects", LocalUserInfo.username, OnLoadDBObjectSuccess);
		}

		static void OnLoadDBObjectSuccess (DatabaseObject dbObj)
		{
			DatabaseArray dbArray = dbObj.GetArray("times");
			dbArray.Set(Level.instances.IndexOf(Level.instance), Level.instance.BestTimeReached);
			float totalTime = 0;
			for (int i = 0; i < dbArray.Count; i ++)
			{
				if (dbArray.Contains(i))
					totalTime += dbArray.GetFloat(i, 0);
			}
			dbObj.Set("totalTime", totalTime);
			dbObj.Set("tasksDone", Achievement.completeCount);
			dbObj.Save();
		}

		public Vector2 GetPositionToTeleportTo (Vector2 fromPosition, float radius)
		{
			if (!type.HasFlag(Type.Teleport))
				return fromPosition;
			else if (fromPosition.x >= trs.position.x + innerSize / 2 - radius)
				return fromPosition + Vector2.left * (innerSize - radius * 2);
			else if (fromPosition.x <= trs.position.x - innerSize / 2 + radius)
				return fromPosition + Vector2.right * (innerSize - radius * 2);
			else if (fromPosition.y >= trs.position.y + innerSize / 2 - radius)
				return fromPosition + Vector2.down * (innerSize - radius * 2);
			else if (fromPosition.y <= trs.position.y - innerSize / 2 + radius)
				return fromPosition + Vector2.up * (innerSize - radius * 2);
			else
				return fromPosition;
		}

		public Vector2 GetSmallestVectorToPoint (Vector2 fromPosition, Vector2 toPosition, float radius)
		{
			if (!type.HasFlag(Type.Teleport))
				return toPosition - fromPosition;
			else
			{
				float x = trs.position.x + innerSize / 2 - radius;
				Vector2 vectorThroughRightWall = GetVectorToPoint(fromPosition, toPosition, x, true, radius);
				x = trs.position.x - innerSize / 2 + radius;
				Vector2 vectorThroughLeftWall = GetVectorToPoint(fromPosition, toPosition, x, true, radius);
				float y = trs.position.y + innerSize / 2 - radius;
				Vector2 vectorThroughUpWall = GetVectorToPoint(fromPosition, toPosition, y, false, radius);
				y = trs.position.y - innerSize / 2 + radius;
				Vector2 vectorThroughDownWall = GetVectorToPoint(fromPosition, toPosition, y, false, radius);
				float vectorThroughRightWallMagnitude = vectorThroughRightWall.magnitude;
				float vectorThroughLeftWallMagnitude = vectorThroughLeftWall.magnitude;
				float vectorThroughUpWallMagnitude = vectorThroughUpWall.magnitude;
				float vectorThroughDownWallMagnitude = vectorThroughDownWall.magnitude;
				Vector2 toToPosition = toPosition - fromPosition;
				float minDistance = Mathf.Min(toToPosition.magnitude, vectorThroughRightWallMagnitude, vectorThroughLeftWallMagnitude, vectorThroughUpWallMagnitude, vectorThroughDownWallMagnitude);
				if (minDistance == vectorThroughRightWallMagnitude)
					return vectorThroughRightWall;
				if (minDistance == vectorThroughLeftWallMagnitude)
					return vectorThroughLeftWall;
				if (minDistance == vectorThroughUpWallMagnitude)
					return vectorThroughUpWall;
				if (minDistance == vectorThroughDownWallMagnitude)
					return vectorThroughDownWall;
				else
					return toToPosition;
			}
		}

		Vector2 GetVectorToPoint (Vector2 fromPosition, Vector2 toPosition, float hitPointComponent, bool isHitPointComponentX, float radius)
		{
			float fromPositionComponent;
			float toPositionComponent;
			float fromPositionOtherComponent;
			float toPositionOtherComponent;
			float oppositeSideOfHitPointComponent;
			if (isHitPointComponentX)
			{
				fromPositionComponent = fromPosition.x;
				toPositionComponent = toPosition.x;
				fromPositionOtherComponent = fromPosition.y;
				toPositionOtherComponent = toPosition.y;
				if (hitPointComponent == trs.position.x - innerSize / 2 + radius)
					oppositeSideOfHitPointComponent = trs.position.x + innerSize / 2 - radius;
				else
					oppositeSideOfHitPointComponent = trs.position.x - innerSize / 2 + radius;
			}
			else
			{
				fromPositionComponent = fromPosition.y;
				toPositionComponent = toPosition.y;
				fromPositionOtherComponent = fromPosition.x;
				toPositionOtherComponent = toPosition.x;
				if (hitPointComponent == trs.position.y - innerSize / 2 + radius)
					oppositeSideOfHitPointComponent = trs.position.y + innerSize / 2 - radius;
				else
					oppositeSideOfHitPointComponent = trs.position.y - innerSize / 2 + radius;
			}
			float toToPositionComponent = Mathf.Abs(hitPointComponent - fromPositionComponent) + Mathf.Abs(toPositionComponent - oppositeSideOfHitPointComponent);
			toToPositionComponent *= Mathf.Sign(hitPointComponent - fromPositionComponent);
			float toToPositionOtherComponent = toPositionOtherComponent - fromPositionOtherComponent;
			float hypotenuseLength = MathfExtensions.GetHypotenuse(toToPositionComponent, toToPositionOtherComponent);
			float angleToToPosition;
			if (isHitPointComponentX)
				angleToToPosition = Mathf.Atan2(toToPositionOtherComponent, toToPositionComponent) * Mathf.Rad2Deg;
			else
				angleToToPosition = Mathf.Atan2(toToPositionComponent, toToPositionOtherComponent) * Mathf.Rad2Deg;
			return VectorExtensions.FromFacingAngle(angleToToPosition) * hypotenuseLength;
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			Transform trs = other.GetComponent<Transform>().parent;
			float radius;
			Entity entity = trs.GetComponent<Entity>();
			if (entity != null)
				radius = entity.radius;
			else
			{
				Bullet bullet = trs.GetComponent<Bullet>();
				radius = bullet.radius;
			}
			trs.position = GetPositionToTeleportTo(trs.position, radius);
		}

		void OnTriggerStay2D (Collider2D other)
		{
			OnTriggerEnter2D (other);
		}

		[Serializable]
		public struct EnemySpawnEntry
		{
			public Enemy enemyPrefab;
			public Zone2D[] spawnZones;
			public float minSpawnDistanceToPlayer;

			public Enemy SpawnEnemy ()
			{
				Vector3 spawnPosition;
				do
				{
					Zone2D spawnZone = spawnZones[Random.Range(0, spawnZones.Length)];
					spawnPosition = spawnZone.GetRandomPoint();
				} while ((Player.Instance.trs.position - spawnPosition).sqrMagnitude < minSpawnDistanceToPlayer * minSpawnDistanceToPlayer);
				Enemy enemy = ObjectPool.instance.SpawnComponent<Enemy>(enemyPrefab, spawnPosition);
				return enemy;
			}
		}

		[Flags]
		public enum Type
		{
			Fire = 2,
			Wind = 4,
			Teleport = 8,
			TimeRewind = 16,
			Branch = 32
		}
	}
}