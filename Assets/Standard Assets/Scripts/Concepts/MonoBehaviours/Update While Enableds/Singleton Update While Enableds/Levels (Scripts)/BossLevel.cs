using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace WizardGame
{
	public class BossLevel : Level
	{
		[HideInInspector]
		public float totalMaxHp;
		[HideInInspector]
		public float totalDamage;

		public override void Begin ()
		{
			OnBegin ();
			for (int i = 0; i < enemySpawnEntries.Length; i ++)
			{
				EnemySpawnEntry enemySpawnEntry = enemySpawnEntries[i];
				for (int i2 = 0; i2 < enemySpawnEntry.spawnZones.Length; i2 ++)
					enemySpawnEntry.spawnZones[i2] = new Zone2D(enemySpawnEntry.spawnZones[i2].collider);
				enemySpawnEntries[i] = enemySpawnEntry;
				Enemy boss = enemySpawnEntry.SpawnEnemy();
				totalMaxHp += boss.maxHp;
			}
			currentTimeText.Text = "Damage: 0";
		}

		public override void End ()
		{
			if (totalDamage > BestTimeReached)
				BestTimeReached = totalDamage;
			Achievement.instances = FindObjectsOfType<Achievement>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (!achievement.complete && achievement.ShouldBeComplete())
					achievement.Complete ();
			}
#if !UNITY_WEBGL
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
#endif
			_SceneManager.instance.RestartScene ();
		}
	}
}