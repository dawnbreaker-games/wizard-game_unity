using UnityEngine;

namespace WizardGame
{
	public class TutorialLevel : Level
	{
		public GameObject activateOnEnd;

		public override void End ()
		{
			for (int i = 0; i < Bullet.instances.Count; i ++)
			{
				Bullet bullet = Bullet.instances[i];
				ObjectPool.instance.Despawn (bullet.prefabIndex, bullet.gameObject, bullet.trs);
				i --;
			}
			if (InputManager.UsingGamepad)
				GameManager.instance.DisplayNotification ("You got hit by a bullet. Try again. Remember to hold the left trigger to use this character's ability. His ability makes him temporarily slightly faster and invulnerable until you release the left trigger (or a second passes).");
			else
				GameManager.instance.DisplayNotification ("You got hit by a bullet. Try again. Remember to hold the right mouse button to use this character's ability. His ability makes him temporarily slightly faster and invulnerable until you release the right mouse button (or a second passes).");
			activateOnEnd.SetActive(true);
		}
	}
}