using UnityEngine;

namespace WizardGame
{
	public class Tutorial : SingletonUpdateWhileEnabled<Tutorial>
	{
		public GameObject[] activateOnFinish = new GameObject[0];

		public virtual void Finish ()
		{
			gameObject.SetActive(false);
			for (int i = 0; i < activateOnFinish.Length; i ++)
			{
				GameObject go = activateOnFinish[i];
				go.SetActive(true);
			}
		}
	}
}