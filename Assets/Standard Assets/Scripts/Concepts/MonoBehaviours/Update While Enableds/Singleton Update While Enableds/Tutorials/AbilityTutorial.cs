using UnityEngine;

namespace WizardGame
{
	public class AbilityTutorial : Tutorial
	{
		public override void DoUpdate ()
		{
			if (Bullet.instances.Count > 0)
			{
				for (int i = 0; i < Bullet.instances.Count; i ++)
				{
					Bullet bullet = Bullet.instances[i];
					if (bullet.gameObject.layer == LayerMask.NameToLayer("Enemy Bullet") && Player.instance.trs.position.y > bullet.trs.position.y)
					{
						Finish ();
						return;
					}
				}
			}
		}
	}
}