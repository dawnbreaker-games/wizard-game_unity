﻿using Extensions;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using FrostweepGames.VoicePro;
using System.Collections.Generic;
using FrostweepGames.Plugins.Native;

namespace WizardGame
{
	public class VoiceChat : SingletonUpdateWhileEnabled<VoiceChat>
	{
		public Toggle muteLocalToggle;
		public Toggle muteOthersToggle;
		public Toggle reliableTransmissionToggle;
		public Transform remoteSpeakersParent;
		public RemoteSpeaker remoteSpeakerPrefab;
		public Recorder recorder;
		public Listener listener;
		static bool ReliableTransmission
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Reliable transmission", true);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Reliable transmission", value);
			}
		}
		static bool _MuteLocal
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Mute local", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Mute local", value);
			}
		}
		static bool _MuteOthers
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Mute others", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Mute others", value);
			}
		}
		List<RemoteSpeaker> remoteSpeakers = new List<RemoteSpeaker>();

		public void Init ()
		{
			if (muteLocalToggle != null)
				muteLocalToggle.isOn = _MuteLocal;
			if (muteOthersToggle != null)
				muteOthersToggle.isOn = _MuteOthers;
			if (reliableTransmissionToggle != null)
				reliableTransmissionToggle.isOn = ReliableTransmission;
			GeneralConfig.Config.reliableTransmission = ReliableTransmission;
			RefreshMicrophones ();
			listener.SpeakersUpdatedEvent += OnSpeakersUpdated;
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			listener.SpeakersUpdatedEvent -= OnSpeakersUpdated;
		}

		public override void DoUpdate ()
		{
			if (NetworkRouter.Instance.ReadyToTransmit && recorder.StartRecord())
				enabled = false;
		}

		void OnSpeakersUpdated (List<Speaker> speakers)
		{
			if (remoteSpeakers.Count > 0)
			{
				for (int i = 0; i < remoteSpeakers.Count; i++)
				{
					RemoteSpeaker remoteSpeaker = remoteSpeakers[i];
					if (!speakers.Contains(remoteSpeaker.speaker))
					{
						Destroy(remoteSpeaker.gameObject);
						remoteSpeakers.RemoveAt(i);
						i --;
					}
				}
			}
			for (int i = 0; i < speakers.Count; i ++)
			{
				Speaker speaker = speakers[i];
				if (remoteSpeakers.Find(remoteSpeaker => remoteSpeaker.speaker == speaker) == null)
				{
					RemoteSpeaker remoteSpeaker = Instantiate(remoteSpeakerPrefab, remoteSpeakersParent);
					remoteSpeaker.speaker = speaker;
					remoteSpeaker.Init ();
					remoteSpeakers.Add(remoteSpeaker);
				}
			}
		}

		void RefreshMicrophones ()
		{
			recorder.RefreshMicrophones();
			if (CustomMicrophone.HasConnectedMicrophoneDevices())
				recorder.SetMicrophone(CustomMicrophone.devices[0]);
		}

		public void MuteLocal (bool mute)
		{
			_MuteLocal = mute;
			if (!mute)
			{
				if ((!NetworkRouter.Instance.ReadyToTransmit || !recorder.StartRecord()) && muteLocalToggle != null)
					muteLocalToggle.isOn = true;
			}
			else
				recorder.StopRecord();
		}

		public void MuteOthers (bool mute)
		{
			_MuteOthers = mute;
			listener.SetMuteStatus(mute);
		}

		public void SetReliableTransmission (bool reliableTransmission)
		{
			ReliableTransmission = reliableTransmission;
			GeneralConfig.Config.reliableTransmission = reliableTransmission;
		}

		public void UseMicrophone (int index)
		{
			if (CustomMicrophone.HasConnectedMicrophoneDevices())
				recorder.SetMicrophone(CustomMicrophone.devices[index]);
		}
	}
}