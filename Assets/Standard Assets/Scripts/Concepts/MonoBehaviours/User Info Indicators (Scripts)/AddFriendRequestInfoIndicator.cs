using UnityEngine;
using UnityEngine.UI;
using PlayerIOClient;
using System.Collections.Generic;

namespace WizardGame
{
	public class AddFriendRequestInfoIndicator : UserInfoIndicator
	{
		public Button acceptButton;

		public override void Init (string username)
		{
			base.Init (username);
			AddFriendRequestsMenu.usernamesOfPendingInvites.Add(username);
		}

		public void Reject ()
		{
			AddFriendRequestsMenu.rejectedUsernames.Add(username);
			Destroy(gameObject);
		}

		public void Accept ()
		{
			acceptButton.interactable = false;
			NetworkManager.client.BigDB.LoadMyPlayerObject(OnLoadDBObjectSuccess_Local, NetworkManager.DisplayError);
			NetworkManager.client.BigDB.Load("PlayerObjects", username, OnLoadDBObjectSuccess_Friend, NetworkManager.DisplayError);
			NetworkManager.client.GameRequests.Send("addfriend", new Dictionary<string, string>() { { "", "" } }, new string[] { username }, OnSendGameRequestSuccess, OnSendGameRequestFail);
		}

		void OnSendGameRequestSuccess ()
		{
			FriendsMenu.friendsUsernames.Add(username);
			FriendInfoIndicator friendInfoIndicator = Instantiate(FriendsMenu.Instance.friendInfoIndicatorPrefab, FriendsMenu.instance.friendInfoIndicatorsParent);
			friendInfoIndicator.Init (username);
			GameManager.instance.DisplayNotification (username + " is now your friend");
			Destroy(gameObject);
		}

		void OnSendGameRequestFail (PlayerIOError error)
		{
			NetworkManager.DisplayError (error);
			acceptButton.interactable = true;
		}
		
		void OnLoadDBObjectSuccess_Friend (DatabaseObject dbObj)
		{
			DatabaseArray dbArray;
			if (dbObj.Contains("friends"))
				dbArray = dbObj.GetArray("friends");
			else
				dbArray = new DatabaseArray();
			dbArray.Add(LocalUserInfo.username);
			dbObj.Set("friends", dbArray);
			dbObj.Save(null, NetworkManager.DisplayError);
		}

		void OnLoadDBObjectSuccess_Local (DatabaseObject dbObj)
		{
			DatabaseArray dbArray;
			if (dbObj.Contains("friends"))
				dbArray = dbObj.GetArray("friends");
			else
				dbArray = new DatabaseArray();
			dbArray.Add(username);
			dbObj.Set("friends", dbArray);
			dbObj.Save(null, NetworkManager.DisplayError);
		}

		void OnDestroy ()
		{
			AddFriendRequestsMenu.usernamesOfPendingInvites.Remove(username);
		}
	}
}