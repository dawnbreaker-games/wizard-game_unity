namespace WizardGame
{
	public class FriendInfoIndicator : UserInfoIndicator
	{
		public void RemoveFriend ()
		{
			FriendsMenu.friendsUsernames.Remove(username);
			Destroy(gameObject);
		}
	}
}