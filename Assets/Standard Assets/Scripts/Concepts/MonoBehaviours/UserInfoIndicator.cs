using UnityEngine;

namespace WizardGame
{
	public class UserInfoIndicator : MonoBehaviour
	{
		public _Text usernameText;
		[HideInInspector]
		public string username;
		const string REPLACE_STRING = "_";

		public virtual void Init (string username)
		{
			this.username = username;
			usernameText.Text = usernameText.Text.Replace(REPLACE_STRING, username);
		}
	}
}