﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Device Display Settings", menuName = "Scriptable Objects/Device Display Settings", order = 1)]
public class DeviceDisplaySettings : ScriptableObject
{
    public string name;
    public bool hasContextIcons;
    public Sprite buttonNorthIcon;
    public Sprite buttonSouthIcon;
    public Sprite buttonWestIcon;
    public Sprite buttonEastIcon;
    public Sprite triggerRightFrontIcon;
    public Sprite triggerRightBackIcon;
    public Sprite triggerLeftFrontIcon;
    public Sprite triggerLeftBackIcon;
    public List<CustomInputContextEntry> customInputContextEntries = new List<CustomInputContextEntry>();
}

[Serializable]
public struct CustomInputContextEntry
{
    public string context;
    public Sprite sprite;
}