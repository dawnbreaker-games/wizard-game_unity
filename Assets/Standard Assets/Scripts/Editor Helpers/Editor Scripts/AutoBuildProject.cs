#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

namespace WizardGame
{
	public class AutoBuildProject : EditorScript
	{
		public uint buildAfterInactiveTime;
		float timeSinceBuilt;
		
		public override void Do ()
		{
			if (UnityEditorInternal.InternalEditorUtility.isApplicationActive)
				timeSinceBuilt = Time.realtimeSinceStartup;
			else if (Time.realtimeSinceStartup - timeSinceBuilt > buildAfterInactiveTime)
			{
				BuildManager.Instance._Build ();
				timeSinceBuilt = Time.realtimeSinceStartup;
			}
		}
	}
}
#else
namespace WizardGame
{
	public class AutoBuildProject : EditorScript
	{
	}
}
#endif
