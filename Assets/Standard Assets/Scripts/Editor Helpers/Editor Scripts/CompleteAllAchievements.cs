#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

namespace WizardGame
{
	public class CompleteAllAchievements : EditorScript
	{
		public override void Do ()
		{
			if (!Application.isPlaying)
				return;
			Achievement.instances = FindObjectsOfType<Achievement>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				achievement.Complete ();
				SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			}
		}
	}
}
#else
namespace WizardGame
{
	public class CompleteAllAchievements : EditorScript
	{
	}
}
#endif
