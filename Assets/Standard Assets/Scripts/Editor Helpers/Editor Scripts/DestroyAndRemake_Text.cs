#if UNITY_EDITOR
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	[ExecuteInEditMode]
	public class DestroyAndRemake_Text : EditorScript
	{
		public _Text text;

		public override void Do ()
		{
			if (text == null)
				text = GetComponent<_Text>();
			_Text newText = Instantiate(text, text.rectTrs.parent);
			DestroyImmediate(text.gameObject);
			text = newText;
		}
	}
}
#else
namespace WizardGame
{
	public class DestroyAndRemake_Text : EditorScript
	{
	}
}
#endif