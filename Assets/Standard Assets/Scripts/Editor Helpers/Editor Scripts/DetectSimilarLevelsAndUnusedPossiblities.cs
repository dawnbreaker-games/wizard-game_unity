#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace WizardGame
{
	public class DetectSimilarLevelsAndUnusedPossiblities : EditorScript
	{
		public override void Do ()
		{
			Level.instances = FindObjectsOfType<Level>();
			List<string> enemyTypes = new List<string>();
			string text = "";
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				if (level is BossLevel)
					continue;
				for (int i2 = i + 1; i2 < Level.instances.Length; i2 ++)
				{
					Level level2 = Level.instances[i2];
					if (level2 is BossLevel)
						continue;
					string[] enemyTypesStrings = GetEnemyTypes(level);
					for (int i3 = 0; i3 < enemyTypesStrings.Length; i3 ++)
					{
						string enemyType = enemyTypesStrings[i3];
						if (!enemyTypes.Contains(enemyType))
							enemyTypes.Add(enemyType);
					}
					string[] enemyTypesStrings2 = GetEnemyTypes(level2);
					for (int i3 = 0; i3 < enemyTypesStrings2.Length; i3 ++)
					{
						string enemyType = enemyTypesStrings2[i3];
						if (!enemyTypes.Contains(enemyType))
							enemyTypes.Add(enemyType);
					}
					if (new List<string>(enemyTypesStrings).ContainsAll(enemyTypesStrings2) || new List<string>(enemyTypesStrings2).ContainsAll(enemyTypesStrings))
						text += level.name + " and " + level2.name + " are too similar\n";
				}
			}
			text += "Possiblities:\n";
			List<List<string>> permutations = enemyTypes.ToArray().UniquePermutations(2, false);
			for (int i = 0; i < permutations.Count; i ++)
			{
				List<string> permutation = permutations[i];
				if (GetSimilarLevel(permutation.ToArray()) == null)
				{
					permutation.Sort(SortEnemyTypes);
					for (int i2 = 0; i2 < permutation.Count; i2 ++)
					{
						string enemyType = permutation[i2];
						text += enemyType + ", ";
					}
					text += "\n";
				}
			}
			permutations = enemyTypes.ToArray().UniquePermutations(3, false);
			for (int i = 0; i < permutations.Count; i ++)
			{
				List<string> permutation = permutations[i];
				if (GetSimilarLevel(permutation.ToArray()) == null)
				{
					permutation.Sort(SortEnemyTypes);
					for (int i2 = 0; i2 < permutation.Count; i2 ++)
					{
						string enemyType = permutation[i2];
						text += enemyType + ", ";
					}
					text += "\n";
				}
			}
			print(text);
		}

		Level GetSimilarLevel (string[] enemyTypes, int startIndex = 0)
		{
			for (int i = startIndex; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				if (level is BossLevel)
					continue;
				string[] enemyTypesStrings = GetEnemyTypes(level);
				if (new List<string>(enemyTypesStrings).ContainsAll(enemyTypes) || new List<string>(enemyTypes).ContainsAll(enemyTypesStrings))
					return level;
			}
			return null;
		}

		string[] GetEnemyTypes (Level level)
		{
			string enemyTypesString = level.name.StartAfter("(Enemy ");
			enemyTypesString = enemyTypesString.Substring(0, enemyTypesString.Length - 1);
			return enemyTypesString.Split(", ");
		}

		int SortEnemyTypes (string enemyType, string enemyType2)
		{
			uint enemyTypeIndex = uint.Parse(enemyType);
			uint enemyType2Index = uint.Parse(enemyType2);
			return MathfExtensions.Sign(enemyType2Index - enemyTypeIndex);
		}
	}
}
#else
namespace WizardGame
{
	public class DetectSimilarLevelsAndUnusedPossiblities : EditorScript
	{
	}
}
#endif
