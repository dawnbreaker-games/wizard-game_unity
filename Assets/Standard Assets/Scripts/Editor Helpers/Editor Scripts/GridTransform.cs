#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace WizardGame
{
	public class GridTransform : EditorScript
	{
		public Transform trs;

		public override void Do ()
		{
			if (trs == null)
				return;
			if (ShouldChangeComponent(trs.localScale.x))
				trs.localScale = trs.localScale.SetX(Mathf.Round(trs.localScale.x));
			if (ShouldChangeComponent(trs.localScale.y))
				trs.localScale = trs.localScale.SetY(Mathf.Round(trs.localScale.y));
			if (ShouldChangeComponent(trs.localScale.z))
				trs.localScale = trs.localScale.SetZ(Mathf.Round(trs.localScale.z));
			if (ShouldChangeComponent(trs.position.x))
				trs.position = trs.position.SetX(MathfExtensions.TruncateEnd(MathfExtensions.RoundToInterval(trs.position.x, .5f), 1));
			if (ShouldChangeComponent(trs.position.y))
				trs.position = trs.position.SetY(MathfExtensions.TruncateEnd(MathfExtensions.RoundToInterval(trs.position.y, .5f), 1));
			if (ShouldChangeComponent(trs.position.z))
				trs.position = trs.position.SetZ(MathfExtensions.TruncateEnd(MathfExtensions.RoundToInterval(trs.position.z, .5f), 1));
		}

		bool ShouldChangeComponent (float component)
		{
			string str = "" + component;
			return str.Contains(".") && str.StartAfter(".").Length > 1;
		}
	}
}
#else
namespace WizardGame
{
	public class GridTransform : EditorScript
	{
	}
}
#endif