#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace WizardGame
{
	public class RecalculateNormals : EditorScript
	{
		public MeshFilter meshFilter;
		public bool useSharedMesh;

		public override void Do ()
		{
			if (meshFilter == null)
				meshFilter = GetComponent<MeshFilter>();
			Mesh mesh;
			if (useSharedMesh)
				mesh = meshFilter.sharedMesh;
			else
				mesh = meshFilter.mesh;
			mesh.RecalculateNormals();
		}
	}
}
#else
namespace WizardGame
{
	public class RecalculateNormals : EditorScript
	{
	}
}
#endif