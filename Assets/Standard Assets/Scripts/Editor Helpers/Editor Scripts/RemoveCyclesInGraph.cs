#if UNITY_EDITOR
using UnityEngine;
using System.Collections.Generic;

namespace WizardGame
{
	public class RemoveCyclesInGraph : EditorScript
	{
		public bool randomizeGraph;
		public int connectionCount;
		public int nodeCount;
		public Graph<int> graph = new Graph<int>();
		List<int> nodesValues = new List<int>();
		Dictionary<Graph<int>.Node, List<Graph<int>.Node>> nodesDict = new Dictionary<Graph<int>.Node, List<Graph<int>.Node>>();
		List<Graph<int>.Node> nodes = new List<Graph<int>.Node>();

		public override void OnValidate ()
		{
			if (randomizeGraph)
			{
				nodesValues.Clear();
				nodesDict.Clear();
				nodes.Clear();
				randomizeGraph = false;
				for (int i = 0; i < nodeCount; i ++)
					nodesValues.Add(i);
				int connectionCountRemaining = connectionCount;
				for (int i = 0; i < nodeCount - nodes.Count; i ++)
				{
					int indexOfNodeValue = Random.Range(0, nodesValues.Count);
					int nodeValue = nodesValues[indexOfNodeValue];
					Graph<int>.Node node = new Graph<int>.Node(nodeValue);
					nodesValues.RemoveAt(indexOfNodeValue);
					nodesDict[node] = new List<Graph<int>.Node>();
					int connectionsToNodeCount = Random.Range(1, Mathf.Min(connectionCountRemaining, nodeCount - nodes.Count - 1));
					connectionCountRemaining -= connectionsToNodeCount;
					for (int i2 = i + 1; i2 < connectionsToNodeCount; i2 ++)
					{
						if (AddRandomNode(ref node))
						{
							nodes.Add(node);
							graph.nodes = nodes;
							base.OnValidate ();
							return;
						}
					}
					nodes.Add(node);
				}
				graph.nodes = nodes;
			}
			base.OnValidate ();
		}

		bool AddRandomNode (ref Graph<int>.Node node)
		{
			int indexOfNode2Value = Random.Range(0, nodesValues.Count);
			int node2Value = nodesValues[indexOfNode2Value];
			Graph<int>.Node node2 = new Graph<int>.Node(node2Value);
			nodesValues.RemoveAt(indexOfNode2Value);
			node.connectedTo.Add(node2);
			nodesDict[node].Add(node2);
			nodes.Add(node2);
			return nodes.Count >= nodeCount;
		}

		public override void Do ()
		{
		}
	}
}
#else
namespace WizardGame
{
	public class RemoveCyclesInGraph : EditorScript
	{
	}
}
#endif
