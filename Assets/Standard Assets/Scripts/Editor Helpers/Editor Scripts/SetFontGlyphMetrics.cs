#if UNITY_EDITOR
using TMPro;
using System;
using Extensions;
using UnityEditor;
using UnityEngine;
using System.Collections;
using UnityEngine.TextCore;
using System.Collections.Generic;

namespace WizardGame
{
	[ExecuteInEditMode]
	public class SetFontGlyphMetrics : EditorScript
	{
		public TMP_FontAsset fontAsset;
		public ChooseCharactersMode includeCharactersMode;
		public UnicodeCategories includeUnicodeCategories;
		public string includeCustomCharacters;
		public ChooseCharactersMode excludeCharactersMode;
		public UnicodeCategories excludeUnicodeCategories;
		public string excludeCustomCharacters;
		public OperationType operation;
		public GlyphMetricsValues glyphMetricsValues;
		public float value;

		public override void Do ()
		{
			for (int i = 0; i < fontAsset.characterTable.Count; i ++)
			{
				TMP_Character character = fontAsset.characterTable[i];
				bool operateOnGlyphMetrics = false;
				if (includeCharactersMode.HasFlag(ChooseCharactersMode.UnicodeCategories) && includeUnicodeCategories.HasFlag((UnicodeCategories) Mathf.Pow(2, 1 + Char.GetUnicodeCategory((char) character.unicode).GetHashCode())))
					operateOnGlyphMetrics = true;
				else if (includeCharactersMode.HasFlag(ChooseCharactersMode.CustomCharacters) && includeCustomCharacters.Contains(((char) character.unicode)))
					operateOnGlyphMetrics = true;
				if (operateOnGlyphMetrics)
				{
					if (excludeCharactersMode.HasFlag(ChooseCharactersMode.UnicodeCategories) && excludeUnicodeCategories.HasFlag((UnicodeCategories) Mathf.Pow(2, 1 + Char.GetUnicodeCategory((char) character.unicode).GetHashCode())))
						operateOnGlyphMetrics = false;
					else if (excludeCharactersMode.HasFlag(ChooseCharactersMode.CustomCharacters) && excludeCustomCharacters.Contains(((char) character.unicode)))
						operateOnGlyphMetrics = false;
				}
				else
					continue;
				Glyph glyph;
				if (operateOnGlyphMetrics && fontAsset.glyphLookupTable.TryGetValue(character.glyph.index, out glyph))
					glyph.metrics = OperateOnGlyphMetrics(glyph.metrics);
			}
		}

		GlyphMetrics OperateOnGlyphMetrics (GlyphMetrics metrics)
		{
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.Width))
				metrics = new GlyphMetrics(OperateOnGlyphMetricsValue(metrics.width), metrics.height, metrics.horizontalBearingX, metrics.horizontalBearingY, metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.Height))
				metrics = new GlyphMetrics(metrics.width, OperateOnGlyphMetricsValue(metrics.height), metrics.horizontalBearingX, metrics.horizontalBearingY, metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.HorizontalBearingX))
				metrics = new GlyphMetrics(metrics.width, metrics.height, OperateOnGlyphMetricsValue(metrics.horizontalBearingX), metrics.horizontalBearingY, metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.HorizontalBearingY))
				metrics = new GlyphMetrics(metrics.width, metrics.height, metrics.horizontalBearingX, OperateOnGlyphMetricsValue(metrics.horizontalBearingY), metrics.horizontalAdvance);
			if (glyphMetricsValues.HasFlag(GlyphMetricsValues.HorizontalAdvance))
				metrics = new GlyphMetrics(metrics.width, metrics.height, metrics.horizontalBearingX, metrics.horizontalBearingY, OperateOnGlyphMetricsValue(metrics.horizontalAdvance));
			return metrics;
		}

		float OperateOnGlyphMetricsValue (float value)
		{
			if (operation == OperationType.Set)
				return this.value;
			else if (operation == OperationType.Add)
				return value + this.value;
			else if (operation == OperationType.Subtract)
				return value - this.value;
			else if (operation == OperationType.SubtractSwapped)
				return this.value - value;
			else if (operation == OperationType.Multiply)
				return value * this.value;
			else if (operation == OperationType.Divide)
				return value / this.value;
			else if (operation == OperationType.DivideSwapped)
				return this.value / value;
			else if (operation == OperationType.Power)
				return Mathf.Pow(value, this.value);
			else if (operation == OperationType.PowerSwapped)
				return Mathf.Pow(this.value, value);
			else if (operation == OperationType.Modulo)
				return value % this.value;
			else // if (operation == OperationType.ModuloSwapped)
				return this.value % value;
		}

		[Flags]
		public enum ChooseCharactersMode
		{
			UnicodeCategories = 2,
			CustomCharacters = 4
		}

		[Flags]
		public enum UnicodeCategories
		{
			ClosePunctuation = 4194304, // Closing character of one of the paired punctuation marks, such as parentheses, square brackets, and braces. Signified by the Unicode designation "Pe" (punctuation, close). The value is 21.

			ConnectorPunctuation = 524288, // Connector punctuation character that connects two characters. Signified by the Unicode designation "Pc" (punctuation, connector). The value is 18.

			Control = 32768, // Control code character, with a Unicode value of U+007F or in the range U+0000 through U+001F or U+0080 through U+009F. Signified by the Unicode designation "Cc" (other, control). The value is 14.

			CurrencySymbol = 134217728, // Currency symbol character. Signified by the Unicode designation "Sc" (symbol, currency). The value is 26.

			DashPunctuation = 1048576, // Dash or hyphen character. Signified by the Unicode designation "Pd" (punctuation, dash). The value is 19.

			DecimalDigitNumber = 512, // Decimal digit character, that is, a character representing an integer in the range 0 through 9. Signified by the Unicode designation "Nd" (number, decimal digit). The value is 8.

			EnclosingMark = 256, // Enclosing mark character, which is a nonspacing combining character that surrounds all previous characters up to and including a base character. Signified by the Unicode designation "Me" (mark, enclosing). The value is 7.

			FinalQuotePunctuation = 16777216, // Closing or final quotation mark character. Signified by the Unicode designation "Pf" (punctuation, final quote). The value is 23.

			Format = 65536, // Format character that affects the layout of text or the operation of text processes, but is not normally rendered. Signified by the Unicode designation "Cf" (other, format). The value is 15.

			InitialQuotePunctuation = 8388608, // Opening or initial quotation mark character. Signified by the Unicode designation "Pi" (punctuation, initial quote). The value is 22.

			LetterNumber = 1024, // Number represented by a letter, instead of a decimal digit, for example, the Roman numeral for five, which is "V". The indicator is signified by the Unicode designation "Nl" (number, letter). The value is 9.

			LineSeparator = 8192, // Character that is used to separate lines of text. Signified by the Unicode designation "Zl" (separator, line). The value is 12.

			LowercaseLetter  = 4, // Lowercase letter. Signified by the Unicode designation "Ll" (letter, lowercase). The value is 1.

			MathSymbol = 67108864, // Mathematical symbol character, such as "+" or "= ". Signified by the Unicode designation "Sm" (symbol, math). The value is 25.

			ModifierLetter = 16, // Modifier letter character, which is free-standing spacing character that indicates modifications of a preceding letter. Signified by the Unicode designation "Lm" (letter, modifier). The value is 3.

			ModifierSymbol = 268435456, // Modifier symbol character, which indicates modifications of surrounding characters. For example, the fraction slash indicates that the number to the left is the numerator and the number to the right is the denominator. The indicator is signified by the Unicode designation "Sk" (symbol, modifier). The value is 27.

			NonSpacingMark = 64, // Nonspacing character that indicates modifications of a base character. Signified by the Unicode designation "Mn" (mark, nonspacing). The value is 5.

			OpenPunctuation = 2097152, // Opening character of one of the paired punctuation marks, such as parentheses, square brackets, and braces. Signified by the Unicode designation "Ps" (punctuation, open). The value is 20.

			OtherLetter = 32, // Letter that is not an uppercase letter, a lowercase letter, a titlecase letter, or a modifier letter. Signified by the Unicode designation "Lo" (letter, other). The value is 4.

			OtherNotAssigned = 1073741824, // Character that is not assigned to any Unicode category. Signified by the Unicode designation "Cn" (other, not assigned). The value is 29.

			OtherNumber = 2048, // Number that is neither a decimal digit nor a letter number, for example, the fraction 1/2. The indicator is signified by the Unicode designation "No" (number, other). The value is 10.

			OtherPunctuation = 33554432, // Punctuation character that is not a connector, a dash, open punctuation, close punctuation, an initial quote, or a final quote. Signified by the Unicode designation "Po" (punctuation, other). The value is 24.

			OtherSymbol = 536870912, // Symbol character that is not a mathematical symbol, a currency symbol or a modifier symbol. Signified by the Unicode designation "So" (symbol, other). The value is 28.

			ParagraphSeparator = 16384, // Character used to separate paragraphs. Signified by the Unicode designation "Zp" (separator, paragraph). The value is 13.

			PrivateUse = 262144, // Private-use character, with a Unicode value in the range U+E000 through U+F8FF. Signified by the Unicode designation "Co" (other, private use). The value is 17.

			SpaceSeparator = 4096, // Space character, which has no glyph but is not a control or format character. Signified by the Unicode designation "Zs" (separator, space). The value is 11.

			SpacingCombiningMark = 128, // Spacing character that indicates modifications of a base character and affects the width of the glyph for that base character. Signified by the Unicode designation "Mc" (mark, spacing combining). The value is 6.

			Surrogate = 131072, // High surrogate or a low surrogate character. Surrogate code values are in the range U+D800 through U+DFFF. Signified by the Unicode designation "Cs" (other, surrogate). The value is 16.

			TitlecaseLetter = 8, // Titlecase letter. Signified by the Unicode designation "Lt" (letter, titlecase). The value is 2.

			UppercaseLetter = 2 // Uppercase letter. Signified by the Unicode designation "Lu" (letter, uppercase). The value is 0.
		}

		public enum OperationType
		{
			Set,
			Add,
			Subtract,
			SubtractSwapped,
			Multiply,
			Divide,
			DivideSwapped,
			Power,
			PowerSwapped,
			Modulo,
			ModuloSwapped
		}

		[Flags]
		public enum GlyphMetricsValues
		{
			Width = 2,
			Height = 4,
			HorizontalBearingX = 8,
			HorizontalBearingY = 16,
			HorizontalAdvance = 32
		}
	}
}
#else
namespace WizardGame
{
	public class SetFontGlyphMetrics : EditorScript
	{
	}
}
#endif