#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace WizardGame
{
	public class SetLevelsTime : EditorScript
	{
        public Level[] levels = new Level[0];
		public float time;

		public override void Do ()
		{
			if (levels.Length == 0)
				levels = FindObjectsOfType<Level>();
			for (int i = 0; i < levels.Length; i ++)
			{
				Level level = levels[i];
				level.BestTimeReached = time;
			}
			SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
		}
	}
}
#else
namespace WizardGame
{
	public class SetLevelsTime : EditorScript
	{
	}
}
#endif