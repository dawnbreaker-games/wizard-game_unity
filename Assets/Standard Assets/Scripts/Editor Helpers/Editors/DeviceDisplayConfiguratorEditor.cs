#if UNITY_EDITOR
using WizardGame;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(DeviceDisplayConfigurator))]
public class DeviceDisplayConfiguratorEditor : Editor
{
	ReorderableList deviceSets;

	void OnEnable ()
	{
		DrawDeviceSets ();
	}

	public override void OnInspectorGUI ()
	{
		serializedObject.Update();
		EditorGUILayout.LabelField("Device Sets", EditorStyles.boldLabel);
		deviceSets.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
	}

	void DrawDeviceSets ()
	{
		deviceSets = new ReorderableList(serializedObject, serializedObject.FindProperty("deviceSets"), true, true, true, true);
		deviceSets.drawHeaderCallback = (Rect rect) => {
			EditorGUI.LabelField(EditorScript.CalculateColumn(rect, 1, 15, 0), "Raw Path Name");
			EditorGUI.LabelField(EditorScript.CalculateColumn(rect, 2, 15, 0), "Device Display Settings");
		};
		deviceSets.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
			var element = deviceSets.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.PropertyField(EditorScript.CalculateColumn(rect, 1, 0, 0), element.FindPropertyRelative("rawPath"), GUIContent.none);
			EditorGUI.PropertyField(EditorScript.CalculateColumn(rect, 2, 10, 10), element.FindPropertyRelative("displaySettings"), GUIContent.none);
		};   
	}
}
#endif