#if UNITY_EDITOR
using WizardGame;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(DeviceDisplaySettings))]
public class DeviceDisplaySettingsEditor : Editor
{
	public SerializedProperty deviceDisplayNameProperty;
	public SerializedProperty deviceDisplayColorProperty;
	public SerializedProperty deviceHasContextIconsProperty;
	public SerializedProperty buttonNorthIconProperty;
	public SerializedProperty buttonSouthIconProperty;
	public SerializedProperty buttonWestIconProperty;
	public SerializedProperty buttonEastIconProperty;
	public SerializedProperty triggerRightFrontIconProperty;
	public SerializedProperty triggerRightBackIconProperty;
	public SerializedProperty triggerLeftFrontIconProperty;
	public SerializedProperty triggerLeftBackIconProperty;
	ReorderableList customInputContextIconList;
	DeviceDisplaySettings deviceDisplaySettings;

	void OnEnable ()
	{
		deviceDisplayNameProperty = serializedObject.FindProperty("name");
		deviceHasContextIconsProperty = serializedObject.FindProperty("hasContextIcons");
		buttonNorthIconProperty = serializedObject.FindProperty("buttonNorthIcon");
		buttonSouthIconProperty = serializedObject.FindProperty("buttonSouthIcon");
		buttonWestIconProperty = serializedObject.FindProperty("buttonWestIcon");
		buttonEastIconProperty = serializedObject.FindProperty("buttonEastIcon");
		triggerRightFrontIconProperty = serializedObject.FindProperty("triggerRightFrontIcon");
		triggerRightBackIconProperty = serializedObject.FindProperty("triggerRightBackIcon");
		triggerLeftFrontIconProperty = serializedObject.FindProperty("triggerLeftFrontIcon");
		triggerLeftBackIconProperty = serializedObject.FindProperty("triggerLeftBackIcon");  
		DrawCustomContextList ();
	}

	void DrawCustomContextList ()
	{
		customInputContextIconList = new ReorderableList(serializedObject, serializedObject.FindProperty("customInputContextEntries"), true, true, true, true);
		customInputContextIconList.drawHeaderCallback = (Rect rect) => {
			EditorGUI.LabelField(EditorScript.CalculateColumn(rect, 1, 15, 0), "Input Binding String");
			EditorGUI.LabelField(EditorScript.CalculateColumn(rect, 2, 15, 0), "Display Icon");
		};
		customInputContextIconList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
			var element = customInputContextIconList.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.PropertyField(EditorScript.CalculateColumn(rect, 1, 0, 0), element.FindPropertyRelative("context"), GUIContent.none);
			EditorGUI.PropertyField(EditorScript.CalculateColumn(rect, 2, 10, 10), element.FindPropertyRelative("sprite"), GUIContent.none);
		};
	}

	public override void OnInspectorGUI ()
	{
		deviceDisplaySettings = (DeviceDisplaySettings)target;
		serializedObject.Update();
		EditorGUILayout.LabelField("Display Name", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(deviceDisplayNameProperty);
		DrawSpaceGUI (2);
		EditorGUILayout.LabelField("Icon Settings", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField(deviceHasContextIconsProperty);
		if (deviceDisplaySettings.hasContextIcons == true)
		{
			DrawSpaceGUI (3);
			EditorGUILayout.LabelField("Icons - Action Buttons", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(buttonNorthIconProperty);
			EditorGUILayout.PropertyField(buttonSouthIconProperty);
			EditorGUILayout.PropertyField(buttonWestIconProperty);
			EditorGUILayout.PropertyField(buttonEastIconProperty);
			DrawSpaceGUI (3);
			EditorGUILayout.LabelField("Icons - Triggers", EditorStyles.boldLabel);
			EditorGUILayout.PropertyField(triggerRightFrontIconProperty);
			EditorGUILayout.PropertyField(triggerRightBackIconProperty);
			EditorGUILayout.PropertyField(triggerLeftFrontIconProperty);
			EditorGUILayout.PropertyField(triggerLeftBackIconProperty);
			DrawSpaceGUI (3);
			EditorGUILayout.LabelField("Icons - Custom Contexts", EditorStyles.boldLabel);
			customInputContextIconList.DoLayoutList();
		}
		serializedObject.ApplyModifiedProperties();
	}

	void DrawSpaceGUI (int amountOfSpace)
	{
		for(int i = 0; i < amountOfSpace; i ++)
			EditorGUILayout.Space();
	}
}
#endif