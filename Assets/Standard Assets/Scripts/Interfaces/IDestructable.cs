﻿namespace WizardGame
{
	public interface IDestructable
	{
		float Hp { get; set; }
		int MaxHp { get; set; }
		
		void TakeDamage (float amount, Player player);
		void Death (Player player);
	}
}