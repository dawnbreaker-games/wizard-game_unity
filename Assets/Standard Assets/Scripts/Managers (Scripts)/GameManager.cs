﻿using TMPro;
using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace WizardGame
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, ISaveableAndLoadable
	{
		[SaveAndLoadValue]
		public List<Asset.Data> assetsData = new List<Asset.Data>();
		// public GameObject[] registeredGos = new GameObject[0];
		// [SaveAndLoadValue]
		// static string enabledGosString = "";
		// [SaveAndLoadValue]
		// static string disabledGosString = "";
		// [SaveAndLoadValue]
		// public GameModifier[] gameModifiers = new GameModifier[0];
		// public static Dictionary<string, GameModifier> gameModifierDict = new Dictionary<string, GameModifier>();
		public TemporaryActiveText temporaryActiveTextNotification;
		public SerializableDictionary<MenuType, Menu> menusDict = new SerializableDictionary<MenuType, Menu>();
		public float switchMenuRate;
		public Level firstLevel;
		public Transform acidMeterTrs;
		public float divideDifficulty;
		public Button buttonPrefab;
		public Button openLeaderboardButton;
		public static Menu currentMenu;
		public static bool paused;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLevelLoaded;
		public static bool isQuitting;
		public static float pausedTime;
		public static float TimeSinceLevelLoad
		{
			get
			{
				return Time.timeSinceLevelLoad - pausedTime;
			}
		}
		static MenuType currentMenuType;
		static float switchMenuTimer;
		static int switchMenuInput;
		static int previousSwitchMenuInput;
		static bool menuInput;
		static bool previousMenuInput;
		static bool restartInput;
		static bool previousRestartInput;
		static List<string> namesOfAlteredSpriteRenderers = new List<string>();
		static List<string> namesOfAlteredLineRenderers = new List<string>();

		public override void Awake ()
		{
			base.Awake ();
			if (BuildManager.Instance.clearDataOnFirstStartupOfThisVerison && BuildManager.VersionIndex < BuildManager.Instance.versionIndex)
			{
				bool isFirstStartup = BuildManager.IsFirstStartup;
				PlayerPrefs.DeleteAll();
				BuildManager.VersionIndex = BuildManager.instance.versionIndex;
				BuildManager.IsFirstStartup = isFirstStartup;
			}
			pausedTime = 0;
#if UNITY_EDITOR
			Achievement.completeCount = 0;
			AddToBulletDamageMultiplierItem.multiplier = 1;
#endif
#if !UNITY_WEBGL
			if (string.IsNullOrEmpty(SaveAndLoadManager.MostRecentSaveFileName))
			{
				SaveAndLoadManager.saveData.completeAchievementsNames = new string[0];
				SaveAndLoadManager.saveData.bestLevelTimesDict = new Dictionary<string, float>();
				SaveAndLoadManager.saveData.triedPlayers = new string[0];
				SaveAndLoadManager.saveData.boolDict = new Dictionary<string, bool>();
				SaveAndLoadManager.saveData.intDict = new Dictionary<string, int>();
				SaveAndLoadManager.saveData.floatDict = new Dictionary<string, float>();
				SaveAndLoadManager.saveData.stringDict = new Dictionary<string, string>();
				SaveAndLoadManager.saveData.vector2Dict = new Dictionary<string, Vector2>();
				SaveAndLoadManager.saveData.boolArrayDict = new Dictionary<string, bool[]>();
				SaveAndLoadManager.saveData.vector2IntArrayDict = new Dictionary<string, _Vector2Int[]>();
				Level.instance = firstLevel;
			}
			else
				SaveAndLoadManager.Instance.LoadMostRecent ();
#endif
			foreach (KeyValuePair<IDestructable, PoisonBullet.PoisonUpdater> keyValuePair in PoisonBullet.poisonUpdatersDict)
				updatables = updatables.Remove(keyValuePair.Value);
			PoisonBullet.poisonUpdatersDict.Clear();
			if (openLeaderboardButton != null)
				openLeaderboardButton.interactable = !string.IsNullOrEmpty(LocalUserInfo.username);
			menusDict.Init ();
			if (currentMenu != null)
				currentMenu.Open ();
			framesSinceLevelLoaded = 0;
			// gameModifierDict.Clear();
			// for (int i = 0; i < gameModifiers.Length; i ++)
			// {
			// 	GameModifier gameModifier = gameModifiers[i];
			// 	gameModifierDict.Add(gameModifier.name, gameModifier);
			// }
			BuildManager.IsFirstStartup = false;
			if (_SceneManager.CurrentScene.name.Contains("Menu"))
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}
			else
			{
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
			}
		}

		void Update ()
		{
			for (int i = 0; i < updatables.Length; i ++)
			{
				IUpdatable updatable = updatables[i];
				updatable.DoUpdate ();
			}
			if (!paused && Time.deltaTime > 0)
			{
				Physics.Simulate(Time.deltaTime);
				Physics.SyncTransforms();
				if (ObjectPool.Instance.enabled)
					ObjectPool.instance.DoUpdate ();
			}
			else
				pausedTime += Time.deltaTime;
			InputSystem.Update ();
			menuInput = InputManager.PauseInput;
			// switchMenuInput = InputManager.SwitchMenuInput;
			restartInput = InputManager.RestartInput;
			if (Level.instance != null)
				HandleMenus ();
			// HandleRestart ();
			framesSinceLevelLoaded ++;
			previousMenuInput = menuInput;
			// previousSwitchMenuInput = switchMenuInput;
			previousRestartInput = restartInput;
		}

		void HandleMenus ()
		{
			if (Level.instance.enabled && menuInput && !previousMenuInput)
			{
				if (currentMenu.gameObject.activeSelf)
					currentMenu.Close ();
				else
					currentMenu.Open ();
			}
			// if (switchMenuInput != 0 && currentMenu.gameObject.activeSelf)
			// {
			// 	switchMenuTimer -= Time.deltaTime;
			// 	if (Mathf.Sign(switchMenuInput) != MathfExtensions.Sign(previousSwitchMenuInput) || switchMenuTimer < 0)
			// 	{
			// 		switchMenuTimer = switchMenuRate;
			// 		int currentMenuType = GameManager.currentMenuType.GetHashCode() + switchMenuInput;
			// 		int menuTypeCount = Enum.GetNames(typeof(MenuType)).Length;
			// 		if (currentMenuType == menuTypeCount)
			// 			currentMenuType = 0;
			// 		else if (currentMenuType == -1)
			// 			currentMenuType = menuTypeCount - 1;
			// 		GameManager.currentMenuType = (MenuType) Enum.ToObject(typeof(MenuType), currentMenuType);
			// 		SetCurrentMenu (GameManager.currentMenuType);
			// 	}
			// }
			// else
			// 	switchMenuTimer = switchMenuRate;
		}

		void HandleRestart ()
		{
			if (restartInput && !previousRestartInput)
				_SceneManager.instance.RestartScene ();
				// Level.instance.End ();
		}

		public void SetCurrentMenu (MenuType menuType)
		{
			currentMenuType = menuType;
			currentMenu.Close ();
			currentMenu = menusDict[currentMenuType];
			currentMenu.Open ();
		}

		public static void HandleOverlappingLineRenderers ()
		{
			LineRenderer[] lineRenderers = FindObjectsOfType<LineRenderer>();
			for (int i = 0; i < lineRenderers.Length; i ++)
			{
				LineRenderer lineRenderer = lineRenderers[i];
				if (!lineRenderer.enabled)
					continue;
				LineSegment2D lineSegment = new LineSegment2D(lineRenderer.GetPosition(0), lineRenderer.GetPosition(1));
				if (!lineRenderer.useWorldSpace)
				{
					Transform lineRendererTrs = lineRenderer.GetComponent<Transform>();
					lineSegment = new LineSegment2D(lineRendererTrs.TransformPoint(lineSegment.start), lineRendererTrs.TransformPoint(lineSegment.end));
				}
				bool sharesPositionsWithOtherLineRenderer = false;
				int i2 = 0;
				if (i < lineRenderers.Length - 1)
					i2 = i + 1;
				for (i2 = i2; i2 < lineRenderers.Length; i2 ++)
				{
					if (i == i2)
						continue;
					LineRenderer lineRenderer2 = lineRenderers[i2];
					if (!lineRenderer2.enabled)
						continue;
					LineSegment2D lineSegment2 = new LineSegment2D(lineRenderer2.GetPosition(0), lineRenderer2.GetPosition(1));
					if (!lineRenderer2.useWorldSpace)
					{
						Transform lineRenderer2Trs = lineRenderer2.GetComponent<Transform>();
						lineSegment2 = new LineSegment2D(lineRenderer2Trs.TransformPoint(lineSegment2.start), lineRenderer2Trs.TransformPoint(lineSegment2.end));
					}
					if (lineSegment.Encapsulates(lineSegment2) || lineSegment2.Encapsulates(lineSegment))
					{
						Vector2 offset = lineSegment.GetPerpendicular().GetDirection();
						lineSegment = lineSegment.Move(offset * lineRenderer.startWidth / 2);
						sharesPositionsWithOtherLineRenderer = true;
						if (!namesOfAlteredLineRenderers.Contains(lineRenderer.name))
						{
							bool previousUsingWorldSpace = lineRenderer.useWorldSpace;
							lineRenderer.SetUseWorldSpace (true);
							lineRenderer.SetPositions(new Vector3[] { lineSegment.start, lineSegment.end });
							lineRenderer.SetUseWorldSpace (previousUsingWorldSpace);
							namesOfAlteredLineRenderers.Add(lineRenderer.name);
						}
						if (!namesOfAlteredLineRenderers.Contains(lineRenderer2.name))
						{
							lineSegment2 = lineSegment2.Move(-offset * lineRenderer2.startWidth / 2);
							bool previousUsingWorldSpace = lineRenderer2.useWorldSpace;
							lineRenderer2.SetUseWorldSpace (true);
							lineRenderer2.SetPositions(new Vector3[] { lineSegment2.start, lineSegment2.end });
							lineRenderer2.SetUseWorldSpace (previousUsingWorldSpace);
							namesOfAlteredLineRenderers.Add(lineRenderer2.name);
						}
					}
					break;
				}
				if (!sharesPositionsWithOtherLineRenderer && namesOfAlteredLineRenderers.Contains(lineRenderer.name))
				{
					Vector2 offset = lineSegment.GetPerpendicular().GetDirection();
					lineSegment = lineSegment.Move(-offset * lineRenderer.startWidth / 2);
					bool previousUsingWorldSpace = lineRenderer.useWorldSpace;
					lineRenderer.SetUseWorldSpace (true);
					lineRenderer.SetPositions(new Vector3[] { lineSegment.start, lineSegment.end });
					lineRenderer.SetUseWorldSpace (previousUsingWorldSpace);
					namesOfAlteredLineRenderers.Remove(lineRenderer.name);
				}
			}
		}
		
		public static void HandleOverlappingSpriteRenderers ()
		{
			SpriteRenderer[] spriteRenderers = FindObjectsOfType<SpriteRenderer>();
			for (int i = 0; i < spriteRenderers.Length; i ++)
			{
				SpriteRenderer spriteRenderer = spriteRenderers[i];
				Transform spriteRendererTrs = spriteRenderer.GetComponent<Transform>();
				bool sharesPositionWithOtherSpriteRenderer = false;
				int i2 = 0;
				if (i < spriteRenderers.Length - 1)
					i2 = i + 1;
				for (i2 = i2; i2 < spriteRenderers.Length; i2 ++)
				{
					if (i == i2)
						continue;
					SpriteRenderer spriteRenderer2 = spriteRenderers[i2];
					Transform spriteRenderer2Trs = spriteRenderer2.GetComponent<Transform>();
					if (spriteRendererTrs.position == spriteRenderer2Trs.position && (spriteRenderer.color != spriteRenderer2.color || spriteRenderer.sharedMaterial != spriteRenderer2.sharedMaterial))
					{
						sharesPositionWithOtherSpriteRenderer = true;
						if (!namesOfAlteredSpriteRenderers.Contains(spriteRenderer.name))
						{
							spriteRenderer.material.SetColor("_tint", spriteRenderer.material.GetColor("_tint").DivideAlpha(2));
							namesOfAlteredSpriteRenderers.Add(spriteRenderer.name);
						}
						if (!namesOfAlteredSpriteRenderers.Contains(spriteRenderer2.name))
						{
							spriteRenderer2.material.SetColor("_tint", spriteRenderer2.material.GetColor("_tint").DivideAlpha(2));
							namesOfAlteredSpriteRenderers.Add(spriteRenderer2.name);
						}
					}
					break;
				}
				if (!sharesPositionWithOtherSpriteRenderer && namesOfAlteredSpriteRenderers.Contains(spriteRenderer.name))
				{
					spriteRenderer.material.SetColor("_tint", spriteRenderer.material.GetColor("_tint").MultiplyAlpha(2));
					namesOfAlteredSpriteRenderers.Remove(spriteRenderer.name);
				}
			}
		}

		public void Quit ()
		{
#if UNITY_EDITOR
			EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
		}

		void OnApplicationQuit ()
		{
			isQuitting = true;
			// PlayerPrefs.DeleteAll();
			// SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
		}

		public void ToggleGameObject (GameObject go)
		{
			go.SetActive(!go.activeSelf);
		}

		public void DestroyChildren (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				Destroy(trs.GetChild(i).gameObject);
		}

		public void DestroyChildrenImmediate (Transform trs)
		{
			for (int i = 0; i < trs.childCount; i ++)
				DestroyImmediate(trs.GetChild(i).gameObject);
		}

		public void ClearData ()
		{
			PlayerPrefs.DeleteAll();
			BuildManager.VersionIndex = BuildManager.instance.versionIndex;
			BuildManager.IsFirstStartup = false;
			_SceneManager.Instance.LoadScene ("Tutorial");
		}

		public void DisplayNotification (string text)
		{
			temporaryActiveTextNotification.text.Text = text;
			temporaryActiveTextNotification.Do ();
		}

		public void DisplayChoices (string[] choices, Action[] results, Button buttonPrefab = null)
		{
			if (buttonPrefab == null)
				buttonPrefab = instance.buttonPrefab;
			// throw new NotImplementedException();
			for (int i = 0; i < choices.Length; i ++)
			{
				string choice = choices[i];
				Button button = Instantiate(buttonPrefab);
				button.GetComponentInChildren<_Text>().Text = choices[i];
				Action result = results[i];
				if (result != null)
					button.onClick.AddListener(() => { result(); } );
			}
		}

		public static void EndTutorial ()
		{
			_SceneManager.instance.LoadScene ("Menu");
		}

		public static void Log (object obj)
		{
			print(obj);
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
#endif
		
		// public static bool ModifierExistsAndIsActive (string name)
		// {
		// 	GameModifier gameModifier;
		// 	if (gameModifierDict.TryGetValue(name, out gameModifier))
		// 		return gameModifier.isActive;
		// 	else
		// 		return false;
		// }

		// public static bool ModifierIsActive (string name)
		// {
		// 	return gameModifierDict[name].isActive;
		// }

		// public static bool ModifierExists (string name)
		// {
		// 	return gameModifierDict.ContainsKey(name);
		// }

		public static void DoActionToObjectAndSpawnedInstances<T> (Action<T> action, T obj) where T : ISpawnable
		{
			action (obj);
			for (int i = 0; i < ObjectPool.instance.spawnedEntries.Count; i ++)
			{
				ObjectPool.SpawnedEntry spawnedEntry = ObjectPool.instance.spawnedEntries[i];
				T instance = spawnedEntry.go.GetComponent<T>();
				if (obj != null)
					action(obj);
			}
		}

		public enum MenuType
		{
			Map,
			Settings
		}

		// [Serializable]
		// public class GameModifier
		// {
		// 	public string name;
		// 	public bool isActive;
		// }
	}
}