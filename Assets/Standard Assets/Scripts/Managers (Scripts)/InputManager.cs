﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.Utilities;

namespace WizardGame
{
	public class InputManager : SingletonMonoBehaviour<InputManager>
	{
		public InputActionAsset inputActionAsset;
		public InputDevice inputDevice;
		public InputSettings settings;
		public static bool UsingGamepad
		{
			get
			{
				return Gamepad.current != null;
			}
		}
		public static bool UsingMouse
		{
			get
			{
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
				return Keyboard.current != null;
			}
		}
		public static bool LeftClickInput
		{
			get
			{
				return UsingMouse && Mouse.current.leftButton.isPressed;
			}
		}
		public static bool RightClickInput
		{
			get
			{
				return UsingMouse && Mouse.current.rightButton.isPressed;
			}
		}
		public static bool RestartInput
		{
			get
			{
				return UsingKeyboard && Keyboard.current.rKey.isPressed;
			}
		}
		public static Vector2? MousePosition
		{
			get
			{
				if (UsingMouse)
					return Mouse.current.position.ReadValue();
				else
					return null;
			}
		}
		public static Vector2 MoveInput
		{
			get
			{
				return Vector2.ClampMagnitude(moveInputAction.ReadValue<Vector2>(), 1);
			}
		}
		public static Vector2 AimInput
		{
			get
			{
				return aimInputAction.ReadValue<Vector2>();
			}
		}
		public static bool ShootInput
		{
			get
			{
				return shootInputAction.ReadValue<float>() > instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool AbilityInput
		{
			get
			{
				return abilityInputAction.ReadValue<float>() > instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool SelectInput
		{
			get
			{
				return selectInputAction.ReadValue<float>() > instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool PauseInput
		{
			get
			{
				return pauseInputAction.ReadValue<float>() > instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool JumpInput
		{
			get
			{
				return jumpInputAction.ReadValue<float>() > instance.settings.defaultDeadzoneMin;
			}
		}
		public static bool InteractInput
		{
			get
			{
				return interactInputAction.ReadValue<float>() > instance.settings.defaultDeadzoneMin;
			}
		}
		static InputAction moveInputAction;
		static InputAction aimInputAction;
		static InputAction shootInputAction;
		static InputAction abilityInputAction;
		static InputAction selectInputAction;
		static InputAction pauseInputAction;
		static InputAction jumpInputAction;
		static InputAction interactInputAction;

		public override void Awake ()
		{
			base.Awake ();
			moveInputAction = inputActionAsset.FindAction("Move");
			moveInputAction.Enable();
			aimInputAction = inputActionAsset.FindAction("Aim");
			aimInputAction.Enable();
			shootInputAction = inputActionAsset.FindAction("Shoot");
			shootInputAction.Enable();
			abilityInputAction = inputActionAsset.FindAction("Ability");
			abilityInputAction.Enable();
			selectInputAction = inputActionAsset.FindAction("Select");
			selectInputAction.Enable();
			pauseInputAction = inputActionAsset.FindAction("Pause");
			pauseInputAction.Enable();
			jumpInputAction = inputActionAsset.FindAction("Jump");
			jumpInputAction.Enable();
			interactInputAction = inputActionAsset.FindAction("Interact");
			interactInputAction.Enable();
		}

		public static float GetAxis (InputControl<float> positiveButton, InputControl<float> negativeButton)
		{
			return positiveButton.ReadValue() - negativeButton.ReadValue();
		}

		public static Vector2 GetAxis2D (InputControl<float> positiveXButton, InputControl<float> negativeXButton, InputControl<float> positiveYButton, InputControl<float> negativeYButton)
		{
			Vector2 output = new Vector2();
			output.x = positiveXButton.ReadValue() - negativeXButton.ReadValue();
			output.y = positiveYButton.ReadValue() - negativeYButton.ReadValue();
			output = Vector2.ClampMagnitude(output, 1);
			return output;
		}
		
		public enum InputDevice
		{
			KeyboardAndMouse,
			Phone,
			Gamepad
		}
	}
}