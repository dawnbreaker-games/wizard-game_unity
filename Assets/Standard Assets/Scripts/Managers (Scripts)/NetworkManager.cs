using Extensions;
using UnityEngine;
using PlayerIOClient;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

namespace WizardGame
{
	public class NetworkManager : SingletonUpdateWhileEnabled<NetworkManager>
	{
		public float updateGameRequestsInterval;
		public static Connection connection;
		public static Client client;
		public static bool IsOnline
		{
			get
			{
				return PlayerPrefsExtensions.GetBool("Is online", false);
			}
			set
			{
				PlayerPrefsExtensions.SetBool ("Is online", value);
			}
		}
		static float updateGameRequestsTimer;
		static string[] newPartyMembersUsernames = new string[0];
		static bool shouldQuit;
		static DatabaseObject dbObj;

		// public override void Awake ()
		// {
		// 	base.Awake ();
		// 	Application.wantsToQuit += OnWantsToQuit;
		// }

		// public override void OnDestroy ()
		// {
		// 	base.OnDestroy ();
		// 	Application.wantsToQuit -= OnWantsToQuit;
		// }

		public override void DoUpdate ()
		{
			if (client == null)
				return;
			updateGameRequestsTimer -= Time.deltaTime;
			if (updateGameRequestsTimer <= 0)
			{
				client.GameRequests.Refresh(OnRefreshGameRequestsSuccess);
				updateGameRequestsTimer += updateGameRequestsInterval;
			}
		}

		public static void Connect (Callback<Client> onSuccess, Callback<PlayerIOError> onFail)
		{
			PlayerIO.UseSecureApiRequests = true;
			if (string.IsNullOrEmpty(LocalUserInfo.username))
				LocalUserInfo.username = StringExtensions.Random(5, "qwertyuiopasdfghjklzxcvbnm");
			PlayerIO.Authenticate("wizard-game-vfjenolj8ekh1hzfsfulww",
				"public",
				new Dictionary<string, string> {
					{ "userId", LoginAndRegisterMenu.username },
				},
				null,
				onSuccess,
				onFail
			);
		}

		public static void Disconnect ()
		{
			Instance.enabled = false;
			if (connection != null)
				connection.Disconnect();
			if (client != null)
				client.Logout();
		}

		bool OnWantsToQuit ()
		{
			if (shouldQuit)
				Disconnect ();
			else
			{
				if (client != null)
					client.BigDB.LoadMyPlayerObject(OnLoadDBObjectSuccess, OnLoadDBObjectFail);
				else
					shouldQuit = true;
			}
			return shouldQuit;
		}

		public static void DisplayError (PlayerIOError error)
		{
			GameManager.instance.DisplayNotification (error.ToString());
		}

		public static void DisplayErrorAndRetry (PlayerIOError error)
		{
			DisplayError (error);
			new StackTrace().GetFrame(1).GetMethod().Invoke(null, null);
		}

		public static void PrintError (PlayerIOError error)
		{
			print(error.ToString());
		}

#region GameRequest Methods
		public static void OnRefreshGameRequestsSuccess ()
		{
			for (int i = 0; i < client.GameRequests.WaitingRequests.Length; i ++)
			{
				GameRequest gameRequest = client.GameRequests.WaitingRequests[i];
				if (gameRequest.Type == "addfriend")
				{
					if (gameRequest.Data.Count == 0)
					{
						if (!AddFriendRequestsMenu.usernamesOfPendingInvites.Contains(gameRequest.SenderUserId))
						{
							AddFriendRequestInfoIndicator addFriendRequestInfoIndicator = Instantiate(AddFriendRequestsMenu.Instance.addFriendRequestInfoIndicatorPrefab, AddFriendRequestsMenu.instance.addFriendRequestInfoIndicatorsParent);
							addFriendRequestInfoIndicator.Init (gameRequest.SenderUserId);
						}
						else if (FriendsMenu.friendsUsernames.Contains(gameRequest.SenderUserId) || AddFriendRequestsMenu.rejectedUsernames.Contains(gameRequest.SenderUserId))
							client.GameRequests.Delete(new GameRequest[] { gameRequest }, null, PrintError);
					}
					else
					{
						FriendsMenu.friendsUsernames.Add(gameRequest.SenderUserId);
						FriendInfoIndicator friendInfoIndicator = Instantiate(FriendsMenu.Instance.friendInfoIndicatorPrefab, FriendsMenu.instance.friendInfoIndicatorsParent);
						friendInfoIndicator.Init (gameRequest.SenderUserId);
						InviteFriendEntry inviteFriendEntry = Instantiate(InviteFriendsMenu.Instance.inviteFriendEntryPrefab, InviteFriendsMenu.instance.inviteFriendEntriesParent);
						inviteFriendEntry.Init (gameRequest.SenderUserId);
						GameManager.instance.DisplayNotification (gameRequest.SenderUserId + " accepted your friend request!");
						client.GameRequests.Delete(new GameRequest[] { gameRequest }, null, PrintError);
					}
				}
				else// if (gameRequest.Type == "invitefriend")
				{
					if (gameRequest.Data.Count == 0)
						client.BigDB.Load("PlayerObjects", gameRequest.SenderUserId, (DatabaseObject dbObj) => { OnLoadDBObjectSuccess (dbObj, gameRequest.SenderUserId); }, (PlayerIOError error) => { OnLoadDBObjectFail (error, gameRequest.SenderUserId); });
					else
					{
						for (int i2 = 0; i2 < newPartyMembersUsernames.Length; i2 ++)
						{
							string newPartyMemberUsername = newPartyMembersUsernames[i2];
							InviteRequest.AddPartyMember (newPartyMemberUsername);
							GameManager.instance.DisplayNotification (newPartyMemberUsername + " has joined the party!");
						}
					}
					client.GameRequests.Delete(new GameRequest[] { gameRequest }, null, PrintError);
				}
			}
		}
#endregion

#region Database Methods
		static void OnLoadDBObjectSuccess (DatabaseObject dbObj)
		{
			if (dbObj != null)
			{
				NetworkManager.dbObj = dbObj;
				dbObj.Set("otherPartyMembers", new DatabaseArray());
				dbObj.Save(OnSaveDBObjectSuccess, OnSaveDBObjectFail);
			}
			else
				OnSaveDBObjectSuccess ();
		}

		static void OnLoadDBObjectFail (PlayerIOError error)
		{
			PrintError (error);
			client.BigDB.LoadMyPlayerObject(OnLoadDBObjectSuccess, OnLoadDBObjectFail);
		}

		static void OnSaveDBObjectSuccess ()
		{
			shouldQuit = true;
			Application.Quit();
		}

		static void OnSaveDBObjectFail (PlayerIOError error)
		{
			PrintError (error);
			dbObj.Save(OnSaveDBObjectSuccess, OnSaveDBObjectFail);
		}

		static void OnLoadDBObjectSuccess (DatabaseObject dbObj, string inviterUsername)
		{
			DatabaseArray partyMembersDBArray;
			if (dbObj.Contains("otherPartyMembers"))
				partyMembersDBArray = dbObj.GetArray("otherPartyMembers");
			else
				partyMembersDBArray  = new DatabaseArray();
			newPartyMembersUsernames = new string[partyMembersDBArray.Count];
			for (int i = 0; i < partyMembersDBArray.Count; i ++)
				newPartyMembersUsernames[i] = partyMembersDBArray.GetString(i);
			partyMembersDBArray.Add(LocalUserInfo.username);
			Transform inviteRequestsParent;
			string gameModeName = "";
			if (Match.localPlayer == null)
			{
				inviteRequestsParent = InviteFriendsMenu.Instance.inviteRequestsParentIfNotInGame;
				uint partySize = (uint) (InviteFriendsMenu.otherPartyMemberCount + partyMembersDBArray.Count + 1);
				if (partySize > MainMenu.MaxPlayers)
					MainMenu.instance.maxPlayersInputField.text = "" + partySize;
			}
			else
			{
				inviteRequestsParent = InviteFriendsMenu.Instance.inviteRequestsParentIfInGame;
				gameModeName = "Battle";
			}
			InviteRequest inviteRequest = Instantiate(InviteFriendsMenu.instance.inviteRequestPrefab, inviteRequestsParent);
			inviteRequest.Init (gameModeName, inviterUsername, newPartyMembersUsernames);
			if (!dbObj.Contains("otherPartyMembers"))
				dbObj.Set("otherPartyMembers", partyMembersDBArray);
			dbObj.Save(null, PrintError);
		}

		static void OnLoadDBObjectFail (PlayerIOError error, string inviterUsername)
		{
			client.BigDB.Load("PlayerObjects", inviterUsername, (DatabaseObject dbObj) => { OnLoadDBObjectSuccess (dbObj, inviterUsername); }, (PlayerIOError error) => { OnLoadDBObjectFail (error, inviterUsername); });
		}
#endregion
	}
}