﻿using System;
using System.IO;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

namespace WizardGame
{
	public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
	{
		public static SaveData saveData = new SaveData();
		public static string MostRecentSaveFileName
		{
			get
			{
				return PlayerPrefs.GetString("Most recent save file name", null);
			}
			set
			{
				PlayerPrefs.SetString("Most recent save file name", value);
			}
		}
		public static string filePath;

		public static void Init ()
		{
			filePath = Application.persistentDataPath + Path.DirectorySeparatorChar + "Save Data";
			print(filePath);
#if !UNITY_WEBGL
			saveData.boolDict = new Dictionary<string, bool>();
			saveData.intDict = new Dictionary<string, int>();
			saveData.floatDict = new Dictionary<string, float>();
			saveData.stringDict = new Dictionary<string, string>();
			saveData.vector2Dict = new Dictionary<string, Vector2>();
			saveData.boolArrayDict = new Dictionary<string, bool[]>();
			saveData.vector2IntArrayDict = new Dictionary<string, _Vector2Int[]>();
			if (File.Exists(filePath))
				Load ();
#endif
		}
		
#if !UNITY_WEBGL
		public static void Save ()
		{
			FileStream fileStream = new FileStream(filePath, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, saveData);
			fileStream.Close();
		}

		public static void Load ()
		{
			FileStream fileStream = new FileStream(filePath, FileMode.Open);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
		}
#endif

		public static bool GetBool (string key, bool value = false)
		{
#if UNITY_WEBGL
			return PlayerPrefs.GetInt(key, value.GetHashCode()) == 1;
#else
			bool output = false;
			if (saveData.boolDict.TryGetValue(key, out output))
				return output;
			else
				return value;
#endif
		}

		public static void SetBool (string key, bool value)
		{
#if UNITY_WEBGL
			PlayerPrefs.SetInt(key, value.GetHashCode());
#else
			saveData.boolDict[key] = value;
#endif
		}

		public static int GetInt (string key, int value = 0)
		{
#if UNITY_WEBGL
			return PlayerPrefs.GetInt(key, value);
#else
			int output = 0;
			if (saveData.intDict.TryGetValue(key, out output))
				return output;
			else
				return value;
#endif
		}

		public static void SetInt (string key, int value)
		{
#if UNITY_WEBGL
			PlayerPrefs.SetInt(key, value);
#else
			saveData.intDict[key] = value;
#endif
		}

		public static float GetFloat (string key, float value = 0)
		{
#if UNITY_WEBGL
			return PlayerPrefs.GetFloat(key, value);
#else
			float output = 0;
			if (saveData.floatDict.TryGetValue(key, out output))
				return output;
			else
				return value;
#endif
		}

		public static void SetFloat (string key, float value)
		{
#if UNITY_WEBGL
			PlayerPrefs.SetFloat(key, value);
#else
			saveData.floatDict[key] = value;
#endif
		}

		public static Vector2 GetVector2 (string key, Vector2 value = new Vector2())
		{
#if UNITY_WEBGL
			return new Vector2(PlayerPrefs.GetFloat(key + ".x", value.x), PlayerPrefs.GetFloat(key + ".y", value.y));
#else
			Vector2 output = new Vector2();
			if (saveData.vector2Dict.TryGetValue(key, out output))
				return output;
			else
				return value;
#endif
		}

		public static void SetVector2 (string key, Vector2 value)
		{
#if UNITY_WEBGL
			PlayerPrefs.SetFloat(key + ".x", value.x);
			PlayerPrefs.SetFloat(key + ".y", value.y);
#else
			saveData.vector2Dict[key] = value;
#endif
		}

		public static _Vector2Int[] GetVector2IntArray (string key, _Vector2Int[] values = null)
		{
#if UNITY_WEBGL
			List<_Vector2Int> output = new List<_Vector2Int>();
			int index = 0;
			while (PlayerPrefs.HasKey(key + "[" + index + "].x"))
			{
				output.Add(new _Vector2Int(PlayerPrefs.GetInt(key + "[" + index + "].x"), PlayerPrefs.GetInt(key + "[" + index + "].y")));
				index ++;
			}
			if (output.Count == 0)
				return values;
			else
				return output.ToArray();
#else
			_Vector2Int[] output = new _Vector2Int[0];
			if (saveData.vector2IntArrayDict.TryGetValue(key, out output))
				return output;
			else
				return values;
#endif
		}

		public static void SetVector2IntArray (string key, _Vector2Int[] values)
		{
#if UNITY_WEBGL
			for (int i = 0; i < values.Length; i ++)
			{
				_Vector2Int value = values[i];
				PlayerPrefs.SetInt(key + "[" + i + "].x", value.x);
				PlayerPrefs.SetInt(key + "[" + i + "].y", value.y);
			}
			int index = values.Length;
			while (PlayerPrefs.HasKey(key + "[" + index + "].x"))
			{
				PlayerPrefs.DeleteKey(key + "[" + index + "].x");
				PlayerPrefs.DeleteKey(key + "[" + index + "].y");
				index ++;
			}
#else
			saveData.vector2IntArrayDict[key] = values;
#endif
		}

		public static bool[] GetBoolArray (string key, bool[] values = null)
		{
#if UNITY_WEBGL
			List<bool> output = new List<bool>();
			int index = 0;
			while (PlayerPrefs.HasKey(key + "[" + index + "]"))
			{
				output.Add(PlayerPrefsExtensions.GetBool(key + "[" + index + "]"));
				index ++;
			}
			if (output.Count == 0)
				return values;
			else
				return output.ToArray();
#else
			bool[] output = new bool[0];
			if (saveData.boolArrayDict.TryGetValue(key, out output))
				return output;
			else
				return values;
#endif
		}

		public static void SetBoolArray (string key, bool[] values)
		{
#if UNITY_WEBGL
			for (int i = 0; i < values.Length; i ++)
			{
				bool value = values[i];
				PlayerPrefsExtensions.SetBool (key + "[" + i + "]", value);
			}
			int index = values.Length;
			while (PlayerPrefs.HasKey(key + "[" + index + "]"))
			{
				PlayerPrefs.DeleteKey(key + "[" + index + "]");
				index ++;
			}
#else
			saveData.boolArrayDict[key] = values;
#endif
		}

		public static string GetString (string key, string value = "")
		{
#if UNITY_WEBGL
			return PlayerPrefs.GetString(key, value);
#else
			string output = null;
			if (saveData.stringDict.TryGetValue(key, out output))
				return output;
			else
				return value;
#endif
		}

		public static void SetString (string key, string value)
		{
#if UNITY_WEBGL
			PlayerPrefs.SetString(key, value);
#else
			saveData.stringDict[key] = value;
#endif
		}

		public static void DeleteKey (string key)
		{
#if UNITY_WEBGL
			PlayerPrefs.DeleteKey(key);
#else
			if (saveData.boolDict.Remove(key))
				return;
			else if (saveData.intDict.Remove(key))
				return;
			else if (saveData.floatDict.Remove(key))
				return;
			else if (saveData.vector2Dict.Remove(key))
				return;
			else if (saveData.vector2IntArrayDict.Remove(key))
				return;
			else
				saveData.stringDict.Remove(key);
#endif
		}

		public static void DeleteAll (string key)
		{
#if UNITY_WEBGL
			PlayerPrefs.DeleteAll();
#else
			saveData.boolDict.Clear();
			saveData.intDict.Clear();
			saveData.floatDict.Clear();
			saveData.floatDict.Clear();
			saveData.vector2IntArrayDict.Clear();
			saveData.boolArrayDict.Clear();
			saveData.stringDict.Clear();
#endif
		}

		static void OnAboutToSave ()
		{
			Achievement.instances = FindObjectsOfType<Achievement>();
			List<string> completeAchievementsNames = new List<string>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				if (achievement.complete)
					completeAchievementsNames.Add(achievement.name);
			}
			if (saveData.completeAchievementsNames == null)
				saveData.completeAchievementsNames = new string[0];
			if (saveData.bestLevelTimesDict == null)
				saveData.bestLevelTimesDict = new Dictionary<string, float>();
			if (saveData.triedPlayers == null)
				saveData.triedPlayers = new string[0];
			saveData.completeAchievementsNames = completeAchievementsNames.ToArray();
			if (Level.instance != null)
				saveData.levelName = Level.instance.name;
			if (Player.instance != null)
				saveData.playerName = Player.instance.name;
			saveData.inputRebindings = InputManager.instance.inputActionAsset.SaveBindingOverridesAsJson();
		}
		
		public static void Save (string fileName)
		{
			OnAboutToSave ();
			FileStream fileStream = new FileStream(fileName, FileMode.Create);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(fileStream, saveData);
			fileStream.Close();
			MostRecentSaveFileName = fileName;
		}
		
		public void Load (string fileName)
		{
			print(fileName);
			FileStream fileStream = new FileStream(fileName, FileMode.Open);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			saveData = (SaveData) binaryFormatter.Deserialize(fileStream);
			fileStream.Close();
			OnLoad (fileName);
		}

		void OnLoad (string fileName)
		{
			MostRecentSaveFileName = fileName;
			OnLoaded ();
		}

		void OnLoaded ()
		{
			Achievement.instances = FindObjectsOfType<Achievement>();
			for (int i = 0; i < Achievement.instances.Length; i ++)
			{
				Achievement achievement = Achievement.instances[i];
				achievement.Init ();
			}
			Level.instances = FindObjectsOfType<Level>();
			for (int i = 0; i < Level.instances.Length; i ++)
			{
				Level level = Level.instances[i];
				if (level.name == saveData.levelName)
				{
					Level.instance = level;
					break;
				}
			}
			InputManager.instance.inputActionAsset.LoadBindingOverridesFromJson(saveData.inputRebindings);
		}
		
		public void LoadMostRecent ()
		{
			Load (MostRecentSaveFileName);
		}
		
		[Serializable]
		public struct SaveData
		{
			public string[] completeAchievementsNames;
			public string levelName;
			public string playerName;
			public Dictionary<string, float> bestLevelTimesDict;
			public string[] triedPlayers;
			public string inputRebindings;
#if !UNITY_WEBGL
			public Dictionary<string, bool> boolDict;
			public Dictionary<string, int> intDict;
			public Dictionary<string, float> floatDict;
			public Dictionary<string, string> stringDict;
			public Dictionary<string, Vector2> vector2Dict;
			public Dictionary<string, bool[]> boolArrayDict;
			public Dictionary<string, _Vector2Int[]> vector2IntArrayDict;
#endif
		}
	}
}