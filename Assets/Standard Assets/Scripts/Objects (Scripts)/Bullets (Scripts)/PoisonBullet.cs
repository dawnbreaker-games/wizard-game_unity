using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WizardGame
{
	public class PoisonBullet : Bullet
	{
		public float addToPoisonRate;
		public float poisonToDamage;
		public float poisonDamage;
		public static Dictionary<IDestructable, PoisonUpdater> poisonUpdatersDict = new Dictionary<IDestructable, PoisonUpdater>();

		public override void OnTriggerEnter (Collider other)
		{
			base.OnTriggerEnter (other);
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null && !poisonUpdatersDict.ContainsKey(destructable))
			{
				PoisonUpdater poisonUpdater = new PoisonUpdater(other.GetComponentInParent<IDestructable>(), this);
				poisonUpdatersDict.Add(destructable, poisonUpdater);
				GameManager.updatables = GameManager.updatables.Add(poisonUpdater);
				if (destructable == Player.instance)
					GameManager.instance.acidMeterTrs.parent.gameObject.SetActive(true);
			}
		}

		void OnTriggerExit (Collider other)
		{
			IDestructable destructable = other.GetComponentInParent<IDestructable>();
			if (destructable != null && (Physics.GetIgnoreLayerCollision(other.gameObject.layer, gameObject.layer) || Physics.OverlapSphere(other.bounds.center, other.bounds.extents.x, LayerMask.GetMask(LayerMask.LayerToName(gameObject.layer))).Length == 0))
			{
				PoisonUpdater poisonUpdater;
				if (poisonUpdatersDict.TryGetValue(destructable, out poisonUpdater))
					GameManager.updatables = GameManager.updatables.Remove(poisonUpdater);
				poisonUpdatersDict.Remove(destructable);
				if (destructable == Player.instance)
					GameManager.instance.acidMeterTrs.parent.gameObject.SetActive(false);
			}
		}

		public class PoisonUpdater : IUpdatable
		{
			IDestructable destructable;
			PoisonBullet poisonBullet;
			float poisonAmount;

			public PoisonUpdater (IDestructable destructable, PoisonBullet poisonBullet)
			{
				this.destructable = destructable;
				this.poisonBullet = poisonBullet;
			}

			public void DoUpdate ()
			{
				if (destructable == null)
					return;
				poisonAmount += poisonBullet.addToPoisonRate * Time.deltaTime;
				while (poisonAmount > poisonBullet.poisonToDamage)
				{
					poisonAmount -= poisonBullet.poisonToDamage;
					destructable.TakeDamage (poisonBullet.poisonDamage, poisonBullet.shooter);
				}
				if (destructable == Player.instance)
					GameManager.instance.acidMeterTrs.localScale = new Vector2(poisonAmount / poisonBullet.poisonToDamage, 1);
			}
		}
	}
}