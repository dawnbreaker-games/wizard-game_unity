using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class PortalItem2 : UseableItem
	{
		public FloatRange distanceRange;
		public float chargeSpeed;
		public Transform targetPositionIndicator;
		public LayerMask whatICantTeleportTo;
		float targetDistance;

		public override void Use ()
		{
			base.Use ();
			targetDistance = distanceRange.Clamp(targetDistance + chargeSpeed * Time.deltaTime);
			Transform playerHeadTrs = Player.instance.headTrs;
			RaycastHit[] hits = Player.instance.rigid.SweepTestAll(playerHeadTrs.forward, targetDistance);
			for (int i = 0; i < hits.Length; i ++)
			{
				RaycastHit hit = hits[i];
				if (whatICantTeleportTo.ContainsLayer(hit.collider.gameObject.layer))
				{
					targetPositionIndicator.position = hit.point;
					return;
				}
			}
		}
	}
}