using Extensions;
using UnityEngine;

namespace WizardGame
{
	public class ShieldItem2 : UseableItem
	{
		public uint gainHp;
		public float hpDecaySpeed;
		public float hpGainSpeed;
		Updater updater;

		public override void Use ()
		{
			base.Use ();
			Player.instance.maxHp += (int) gainHp;
			updater = new Updater(this);
			GameManager.updatables = GameManager.updatables.Add(updater);
		}

		void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(updater);
		}

		class Updater : IUpdatable
		{
			public ShieldItem2 shieldItem;
			float gainedHp;

			public Updater (ShieldItem2 shieldItem)
			{
				this.shieldItem = shieldItem;
			}

			public void DoUpdate ()
			{
				if (gainedHp <= shieldItem.gainHp)
				{
					float gainHp = shieldItem.hpGainSpeed * Time.deltaTime;
					gainedHp += gainHp;
					float hpOvershoot = gainedHp - shieldItem.gainHp;
					Player.instance.hp += gainHp;
					if (hpOvershoot > 0)
						Player.instance.hp -= hpOvershoot;
				}
				else
				{
					float damageAmount = shieldItem.hpDecaySpeed * Time.deltaTime;
					gainedHp -= damageAmount;
					Player.instance.TakeDamage (damageAmount, null);
					if (gainedHp <= 0)
					{
						Player.instance.hp -= gainedHp;
						GameManager.updatables = GameManager.updatables.Remove(this);
					}
				}
			}
		}
	}
}