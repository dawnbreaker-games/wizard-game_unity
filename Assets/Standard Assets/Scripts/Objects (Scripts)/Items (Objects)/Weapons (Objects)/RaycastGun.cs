using UnityEngine;

namespace WizardGame
{
	public class RaycastGun : UseableWeapon
	{
		public float damage;
		public LayerMask whatIDaamge;

		public override void Use ()
		{
			base.Use ();
			RaycastHit hit;
			if (Physics.Raycast(trs.position, trs.forward, out hit, whatIDaamge))
				hit.collider.GetComponentInParent<IDestructable>().TakeDamage (damage, owner);
		}
	}
}