using WizardGame;
using Extensions;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class _Slider : _Selectable
{
	public Slider slider;
	public _Text displayValueText;
	public float[] snapValues = new float[0];
	string initDisplayValueTextString;
	
	void Awake ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		if (displayValueText != null)
		{
			initDisplayValueTextString = displayValueText.Text;
			SetDisplayValue ();
		}
		OnValueChanged ();
	}

	public void OnValueChanged ()
	{
		if (snapValues.Length > 0)
			slider.value = MathfExtensions.GetClosestNumber(slider.value, snapValues);
	}
	
	public void SetDisplayValue ()
	{
		if (displayValueText != null)
			displayValueText.Text = initDisplayValueTextString + slider.value;
	}
}
