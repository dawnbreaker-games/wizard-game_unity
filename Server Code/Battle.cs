using System;
using PlayerIO.GameLibrary;
using System.Collections.Generic;

[RoomType("Battle")]
public class Battle : Game<Player>
{
	Match match;

	public override void GameStarted ()
	{
		match = new Match(this);
	}

	public override void UserJoined (Player player)
	{
		match.UserJoined (player);
	}

	public override void UserLeft (Player player)
	{
		match.UserLeft (player);
	}

	public override void GotMessage (Player player, Message message)
	{
		match.GotMessage (player, message);
	}
}