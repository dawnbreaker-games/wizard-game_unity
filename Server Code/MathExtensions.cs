using System;

public static class MathExtensions
{
	public const float METERS_PER_MILE = 1609.344f;
	public const float DEGREES_PER_RADIAN = 0.01745329f;

	public static float Random (float min, float max, Random random)
	{
		return min + ((max - min) * (float) random.NextDouble());
	}
}