using PlayerIO.GameLibrary;

public class Player : BasePlayer
{
	public Vector3 position;
	public Vector3 rotation;
	public uint id;
	public bool invulnerable = true;
	public int score;
	public Vector3 color;
}